/*

 */

import Vue from 'vue';
import Router from 'vue-router';
import Dummy from './components/Dummy.vue';
import PrivateStreamer from './components/StreamersOverviewPrivate.vue';
import Home from './components/Home.vue';
import Login from './components/Login.vue';
import RegisterSuccessfull from './components/RegisterSuccessfull.vue';
import UserAccount from './components/UserAccount.vue';
import PasswordRetriever from './components/PasswordRetriever.vue';
import Concept from './components/Concept.vue';
import StreamerPage from './components/StreamerPage.vue';
import SharedStreamer from './components/StreamersOverviewShared.vue';
import StreamerLivePage from './components/StreamerLivePage.vue';
import StartLive from './components/StartLive.vue';
import FAQ from './components/FAQ.vue';

Vue.use(Router);

export default new Router({
    routes: [

        {
            path: '/',
            name: 'Home',
            component: Home
        },

        {   // usefull to redirect to home after login
            path: '/home',
            name: 'Home',
            component: Home
        },

        {
            path: "/concept",
            name: "Concept",
            component: Concept
        },

        {
            path: "/login/:redirection/:id",
            name: "Login",
            component: Login
        },

        {
            path: "/retrieve_password",
            name: "Retrieve Password",
            component: PasswordRetriever
        },

        {
            path: '/private',
            name: 'Private',
            component: PrivateStreamer,
        },

        {
            path: '/shared',
            name: 'Shared',
            component: SharedStreamer
        },

        {
            path: '/dummy',
            name: 'Dummy',
            component: Dummy
        },

        {
            path: '/register_successfull/:username',
            name: "Successfull Register",
            component: RegisterSuccessfull
        },
        {
            path: '/account',
            name: "User account",
            component: UserAccount
        },
        {
            path: '/streamer/:id',
            name: 'StreamerPage',
            component: StreamerPage
        },
        {
            path: '/streamer_live/:id',
            name: 'StreamerLivePage',
            component: StreamerLivePage
        },
        {
            path: '/startLive',
            name: 'StartLive',
            component: StartLive
        },
        {
            path: '/faq',
            name: 'FAQ',
            component: FAQ
        }
    ]
});


