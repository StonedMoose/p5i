import Vue from 'vue'
import VeeValidate, { Validator } from 'vee-validate';
import fr from 'vee-validate/dist/locale/fr';

var bigInt = require("big-integer");

// customize validator messages
const dictionary = {
    fr: {
        messages: {
            email: () => {
                return "Vous devez rentrer un e-mail valide"
            },
            required: (field) => {
                switch (field){
                    case "dispos":
                        return "Vous devez choisir au moins 1 disponibilité";
                    case "genres":
                        return "Vous devez choisir au moins 2 genres";
                    case "price":
                        return "Vous devez renseigné un prix pour vos animations"
                    default:
                        return " "
                }

            },
            min: (field, args) => {
                let msg;
                switch (field){
                    case "password":
                        msg="Le mot de passe doit contenir au minimum " + args + " caractères";
                        break;
                    case "bio":
                        msg="Votre description doit faire au minimum " + args + " caractères";
                        break;
                    default:
                        msg= field + " doit faire au minimum " + args + " caractères";
                        break;
                }

                return msg
            },
            decimal: () => {
                return "Montant invalide"
            },

            max_value: (field, args) => {
                return "L'image ne doit pas faire plus de " + args/(1024*1024) + " Mo"
            },
            bic: () => {
                return "Le code BIC saisi n'est pas valide"
            },
            iban: () => {
                return "Le code IBAN saisi n'est pas valide"
            },
            list_size: (field, args) => {
                let trad;
                switch (field){
                    case "genres":
                        trad="genres";
                        break;
                    case "dispos":
                        trad="disponibilité";
                        break;
                    default:
                        trad= field;
                        break;
                }
                return "Vous devez choisir au moins " + args + " " + trad;
            },
            tel: () => {
                return "Le numéro de téléphone saisi n'est pas valide"
            },
            username: () => {
                return "Le nom d'utilisateur ne doit pas contenir de caractères spéciaux ou d'espace"
            },
            price: () => {
                console.log("price validator")
                return "Montant invalide"
            }
        },
    }
};

Validator.extend('bic', {
    getMessage: () => 'You must enter a valid BIC',
    validate: value => /^[A-Za-z]{6}([A-Za-z0-9]{2}|[A-Za-z0-9]{5})$/.test(value.replace(/[-_ \t]/g, ""))
});

Validator.extend('iban', {
    getMessage: () => 'You must enter a valid IBAN',
    validate: value => {
        let v = value.replace(/[-_ \t]/g, "");
        v = v.substring(4) + v.substring(0,4);
        v = v.toUpperCase();

        var new_v = "";
        for (let i=0;i<v.length; i++){
            switch (v[i]){
                case "A":
                    new_v+="10";break;
                case "B":
                    new_v+="11";break;
                case "C":
                    new_v+="12";break;
                case "D":
                    new_v+="13";break;
                case "E":
                    new_v+="14";break;
                case "F":
                    new_v+="15";break;
                case "G":
                    new_v+="16";break;
                case "H":
                    new_v+="17";break;
                case "I":
                    new_v+="18";break;
                case "J":
                    new_v+="19";break;
                case "K":
                    new_v+="20";break;
                case "L":
                    new_v+="21";break;
                case "M":
                    new_v+="22";break;
                case "N":
                    new_v+="23";break;
                case "O":
                    new_v+="24";break;
                case "P":
                    new_v+="25";break;
                case "Q":
                    new_v+="26";break;
                case "R":
                    new_v+="27";break;
                case "S":
                    new_v+="28";break;
                case "T":
                    new_v+="29";break;
                case "U":
                    new_v+="30";break;
                case "V":
                    new_v+="31";break;
                case "W":
                    new_v+="32";break;
                case "X":
                    new_v+="33";break;
                case "Y":
                    new_v+="34";break;
                case "Z":
                    new_v+="35";break;
                default:
                    new_v+=v[i];
                    break;
            }
        }

        return (bigInt(new_v).mod(97)).value === 1;
    }
});

Validator.extend("list_size", {
    getMessage: (field, arg) => "You must chose at least " + arg + " " + field,
    validate: (value, arg) => (value.length !== 0)? value.split(',').length >= arg: false
});

Validator.extend("tel", {
    getMessage: () => "You must enter a valid phone number",
    validate: value => /^\+?[0-9]{8,18}$/.test(value.replace(/[-_ \t]/g, ""))
});

Validator.extend("username", {
    getMessage: () => "You must enter a username",
    validate: value => /^[a-zA-Z0-9_-]+$/.test(value)
});

Validator.extend("price", {
    getMessage: () => "You must enter a valid amount",
    validate: value => {
        if (value === 0)
            return false; 
        return /^[0-9]+\.?[0-9]{0,2}$/.test(value)
    }
});


// setting up validator
Validator.localize('fr', fr);
Vue.use(VeeValidate, {
    locale: 'fr', dictionary:        dictionary
});