import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);
import axios from 'axios';

// on store setup we want to retrieve user information from api to log him in if authorization token known
const initialSetup = store => {
    let session = window.sessionStorage.getItem("vue-session-key");
    try {
        let token = JSON.parse(session).authorization; // can raise error if session doesn't exist
        if (token){
            axios.get("/jwt/user_profil", {headers: {'Authorization': token}}).then(
                (response) => {
                    store.commit("log_in", response.data)
                }
            )
        }
    } catch(err) {}

};


export default new Vuex.Store({
    plugins: [initialSetup],
    strict: process.env.NODE_ENV !== 'production', // throw error for each mutation from outside mutation handlers, in dev only

    state: {
        user: {
            _id: undefined,
            username: undefined,
            mail: undefined,
            picture: undefined,
            streamerId: undefined
        },

        streamer_profil_fetched: false,
        streamer: {
            dispos: [],
            genres: [],
            picture: null,
            bio: undefined,
            iban: undefined,
            bic: undefined,
            doPrivate: undefined,
            telephone: undefined,
            price: undefined
        },

        genres: [ 'Blues', 'Classique', 'Electro', 'Jazz', 'Pop', 'Rap', 'Reggae', 'RnB', 'Rock', 'Salsa', 'Trans' ],
        dispos: [ 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche']

    },

    mutations: {
        /*
         *  Mutation trigerred on user login
         *  @param token an Object with _id, username, mail, picture and streamerId fields
         */
        log_in(state, token){
            state.user._id = token._id;
            state.user.username = token.username;
            state.user.mail = token.mail;
            state.user.picture = token.picture;
            state.user.streamerId = token.streamerId;
        },

        /*
         *  Mutation triggered when user's profil is updated
         *  @param data an Object with data that has been updated
         */
        user_profil_updated(state, data){
            for (let cle in data) {
                if (cle === "picture") {
                    // on picture we need to add "randomness" to bypass cache mecanism
                    state.user[cle] = data[cle] + '?' + new Date().getTime();
                } else {
                    state.user[cle] = data[cle];
                }
            }
        },

        /*
         *  Mutation triggered when user's profil is updated or created
         *  @param data an Object with data that has been updated
         */
        streamer_profil_received(state, data){
            state.streamer_profil_fetched = true;
            for (let cle in data) {
                state.streamer[cle] = data[cle];
            }
        },

        /*
         *  Mutation triggered when user's profil is updated or created
         *  @param data an Object with data that has been updated
         */
        streamer_profil_updated(state, data){
            for (let cle in data) {
                if (cle === "streamerId"){
                    state.user[cle] = data[cle]
                } else {
                    state.streamer[cle] = data[cle];
                }
            }
        }
    },


})