// module import
import Vue from 'vue';
import App from './App.vue';
import VueSession from 'vue-session';
import VueCookie from 'vue-cookie';
import axios from 'axios';
import VueProgressiveImage from 'vue-progressive-image'
// import 'videojs-contrib-hls/dist/videojs-contrib-hls.min'
// import 'videojs-contrib-quality-levels/dist/videojs-contrib-quality-levels.min'

// our import
import router from './router';
import store from './store'
import './validator';
import './themes';

// module set up
Vue.config.devtools = true;
Vue.use(VueCookie);
Vue.use(VueSession);
Vue.use(VueProgressiveImage);

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    store,
    template: '<App/>',
    components: { App },
    beforeCreate(){
        Vue.prototype.$http = axios.create();

        // Create a new composant which serve as a bus
        // to communicate through non parent components
        Vue.prototype.$bus = new Vue();

        // Snack bar accessible through all app
        // methods: show(msg)
        Vue.prototype.$snack = new Vue({
            el: '#snack',
            template: '<md-snackbar md-position="bottom center" ref="snackbar" :md-duration="3000" >' +
                      '<span style="color: white">{{ msg }}</span>' +
                      '</md-snackbar>',

            data: {
                msg: ""
            },

            methods:{
                // show snackbar displaying msg
                show(msg){
                    this.msg = msg;
                    this.$refs.snackbar.open()
                },
            }
        });

    }
});
