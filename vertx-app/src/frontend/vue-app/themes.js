import VueMaterial from 'vue-material';
import Vue from 'vue';
import 'vue-material/dist/vue-material.css';

Vue.use(VueMaterial);
// Registring themes
Vue.material.registerTheme({
    // 'default': {
    //     primary: {
    //         color: 'blue',
    //         hue: 900
    //     },
    //     background: {
    //         color: 'grey',
    //     },
    //     warn: {
    //         color: 'light-blue',
    //         hue: 700
    //     },
    //     accent: {
    //         color: 'blue',
    //         hue: 700
    //     }
    // },
    'default': {
        primary: {
            color: 'blue',
            hue: 900
        },
        secondary: {
            color: 'blue',
            hue: 'A200'
        },
        background: {
            color: 'grey',
            hue: 100
        },
        accent: {
            color: 'amber',
            hue: 700
        },
        warn: {
            color: 'light-blue',
            hue: 700
        },
    },
    'homePage': {
        primary: {
            color: 'white',
        }
    },
    'form': {
        primary: {
            color: 'blue',
            hue: 900
        },
        background: {
            color: 'grey',
            hue: 100
        },
        warn: {
            color: 'deep-orange',
            hue: 700
        },
        accent: {
            color: 'light-blue',
            hue: 700
        }

        // default theme of md, don't modify
    },
    'orange':{
        primary: {color: 'dark-oragne'}
    },
    'mdDefault': {
        primary: {color: 'indigo'},
        accent: {color: 'pink'},
        warn: {color: 'deep-orange'},
        background: {color: 'white'}
    }

});