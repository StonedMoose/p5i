package com.awa.webapp;

import io.vertx.core.logging.SLF4JLogDelegateFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.awa.webapp.database.AwaDatabaseVerticle;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;



public class MainVerticle extends AbstractVerticle {

    /*
    Main method of verticle, call on launch.
    */
    @Override
    public void start(Future<Void> startFuture) throws Exception {
        LoggerFactory.getLogger (MainVerticle.class); // Required for Logback to work in Vertx
        Future<String> dbVerticleDeployment = Future.future();

        DeploymentOptions options = new DeploymentOptions();
        options.setConfig(new JsonObject().put("mongo.port", Integer.parseInt(System.getProperty("mongo.port", "27017"))));

        vertx.deployVerticle(new AwaDatabaseVerticle(), options, dbVerticleDeployment.completer());
        // deploy Https Server
        dbVerticleDeployment.compose(id -> {
            Future<String> httpsVerticleDeployment = Future.future();

            JsonObject conf = new JsonObject().put("http.server.port", Integer.parseInt(System.getProperty("https.port", "8080")))
                                              .put("isTesting", System.getProperty("isTesting", "false").equals("true"));

            vertx.deployVerticle(
                    "com.awa.webapp.http.HttpsServerVerticle",
                    new DeploymentOptions().setInstances(Integer.parseInt(System.getProperty("https.nb", "1")))
                                           .setConfig(conf),
                    httpsVerticleDeployment.completer());

            return httpsVerticleDeployment;

        // deploy Http redirect server
        }).compose(id -> {
            Future<String> httpVerticleDeployment = Future.future();

            JsonObject conf = new JsonObject().put("http.server.port", Integer.parseInt(System.getProperty("httpRedirect.port", "8081")));

            vertx.deployVerticle(
                    "com.awa.webapp.http.HttpServerVerticle",
                    new DeploymentOptions().setInstances(Integer.parseInt(System.getProperty("httpRedirect.nb", "1")))
                                           .setConfig(conf),
                    httpVerticleDeployment.completer());

            return httpVerticleDeployment;

        // deploy Http Check stream publish server
        }).compose(id -> {
            Future<String> httpVerticleDeployment = Future.future();

            JsonObject conf = new JsonObject().put("http.server.port", Integer.parseInt(System.getProperty("httpPublish.port", "9000")));

            vertx.deployVerticle(
                    "com.awa.webapp.http.HttpCheckStreamPublish",
                    new DeploymentOptions().setInstances(Integer.parseInt(System.getProperty("httpPublish.nb", "1")))
                                           .setConfig(conf),
                    httpVerticleDeployment.completer());

            return httpVerticleDeployment;

        })
        .setHandler(ar -> {
            if (ar.succeeded()) {
                startFuture.complete();
            } else {
                startFuture.fail(ar.cause());
            }
        });
    }
}
