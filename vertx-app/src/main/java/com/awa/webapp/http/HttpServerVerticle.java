package com.awa.webapp.http;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.vertx.core.Future;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerRequest;


public class HttpServerVerticle extends AbstractVerticle {

    private static final Logger LOGGER = LoggerFactory.getLogger(HttpsServerVerticle.class);
    public static final String CONFIG_HTTP_SERVER_PORT = "http.server.port";

    @Override
    public void start(Future<Void> startFuture) throws Exception {
        int p = config().getInteger(CONFIG_HTTP_SERVER_PORT, 8081);
        HttpServer server = vertx.createHttpServer();

        server.requestHandler((HttpServerRequest req) -> {
            LOGGER.info("Http server redirect to https sibling");
            req.response()
                    .setStatusCode(301)
                    .putHeader("location", "https://www.awa.ovh")
                    .end();
        })
        .listen(p, ar -> {
            if (ar.succeeded()) {
                LOGGER.info("Redirect HTTP server running on port " + p);
                startFuture.complete();
            } else {
                LOGGER.error("Could not start a HTTP server", ar.cause());
                startFuture.fail(ar.cause());
            }

        });
    }


}





