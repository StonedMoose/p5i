package com.awa.webapp.http;

import com.awa.webapp.database.AwaDatabaseService;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.vertx.core.Future;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpServer;

//TODO: add control on play

public class HttpCheckStreamPublish extends AbstractVerticle {
    private static final Logger LOGGER = LoggerFactory.getLogger(HttpCheckStreamPublish.class);
    public static final String CONFIG_HTTP_SERVER_PORT = "http.server.port";

    private AwaDatabaseService dbService;

    @Override
    public void start(Future<Void> startFuture) throws Exception {

        String awaDbQueue = "awadb.queue";
        int portNumber = config().getInteger(CONFIG_HTTP_SERVER_PORT, 9000);;

        dbService = AwaDatabaseService.createProxy(vertx, awaDbQueue);

        HttpServer server = vertx.createHttpServer()
                                        .exceptionHandler(error -> LOGGER.error("Http server exception " + error.getLocalizedMessage()));

        // Install router
        Router router = Router.router(vertx);
        // Install Handler
        router.route().handler(BodyHandler.create());

        // Setup route handler
        router.post("/on_publish").handler(this::onPublishHandler);
        router.post("/on_publish_done").handler(this::onPublishDoneHandler);


        // launch serverlocalhost
        server.requestHandler(router::accept)
              .listen(portNumber, ar -> {
                  if (ar.succeeded()) {
                      LOGGER.info("HTTPCheckPublish server running on port " + portNumber);
                      startFuture.complete();
                  } else {
                      LOGGER.error("Could not start a HTTPCheckPublish server", ar.cause());
                      startFuture.fail(ar.cause());
                  }
              });
    }

    /*
        Handler adressed by nginx when a streamer wants to publish a stream
        @route POST localhost:9000/on_publish
        @return HTTP/200 or HTTP/400
     */
    private void onPublishHandler(RoutingContext context){
        String body = context.getBody().toString();
        String key = body.split("&name=")[1].split("&")[0];
        String id = key.split("_")[0];
        String stream_key = key.split("_")[1];

        dbService.setOnAir(id, stream_key, res -> {
            if (res.succeeded()){
                LOGGER.info("onPublishHandler -> Authorized stream publish for streamer_id:" + id);
                context.request().response().setStatusCode(200).end();

            } else {
                LOGGER.info("onPublishHandler -> Unauthorized stream publish for streamerId:" + id);
                context.response().setStatusCode(400).end();
            }
        });
    }

    /*
        Handler adressed by nginx when a streamer wants ends publish simply set user off air
        @route POST localhost:9000/on_publish_done
        @return HTTP/200 or HTTP/400
     */
    private void onPublishDoneHandler(RoutingContext context){
        // TODO: test it
        String key = context.getBody().toString().split("&name=")[1].split("&")[0];
        String id = key.split("_")[0];

        dbService.setOffAir(id, res -> {
            if (res.succeeded()){
                LOGGER.info("onPublishDoneHandler -> Not on air anymore, streamer_id:" + id);
                context.request().response().setStatusCode(200).end();
            } else {
                context.response().setStatusCode(400).end();
            }
        });


    }

}
