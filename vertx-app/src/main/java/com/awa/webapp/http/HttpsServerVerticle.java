package com.awa.webapp.http;

import com.awa.webapp.database.AwaDatabaseService;

import com.awa.webapp.utils.User;
import io.vertx.core.CompositeFuture;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.ext.auth.mongo.MongoAuth;
import io.vertx.ext.auth.mongo.impl.DefaultHashStrategy;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.web.handler.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import io.vertx.core.Future;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.json.JsonObject;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.net.JksOptions;

import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.sockjs.SockJSHandler;
import io.vertx.ext.web.handler.sockjs.BridgeOptions;
import io.vertx.ext.web.sstore.LocalSessionStore;
import io.vertx.ext.auth.jwt.JWTAuthOptions;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.bridge.PermittedOptions;
import io.vertx.ext.auth.KeyStoreOptions;
import io.vertx.ext.mail.MailClient;
import io.vertx.ext.mail.MailMessage;
import io.vertx.ext.mail.MailConfig;
import io.vertx.ext.mail.LoginOption;
import io.vertx.ext.mail.StartTLSOptions;

import javax.xml.bind.DatatypeConverter;
import java.security.KeyException;


public class HttpsServerVerticle extends AbstractVerticle {

    private static final Logger LOGGER = LoggerFactory.getLogger(HttpsServerVerticle.class);

    public static final String CONFIG_HTTP_SERVER_PORT = "http.server.port";
    public static final String CONFIG_AWADB_QUEUE = "awadb.queue";
    public static final String CONFIG_HTTP_JKS_KEYSTORE_FILE = "src/main/resources/server_keystore.jks";
    public static final String CONFIG_HTTP_JCEKS_KEYSTORE_FILE = "src/main/resources/keystore.jceks";

    private MailClient mailClient;
    private AwaDatabaseService dbService;
    private MongoAuth auth_username;
    private MongoAuth auth_mail;
    private JWTAuth jwtAuth;

    @Override
    public void start(Future<Void> startFuture) throws Exception {

        String awaDbQueue = config().getString(CONFIG_AWADB_QUEUE, "awadb.queue");
        int portNumber = config().getInteger(CONFIG_HTTP_SERVER_PORT, 8080);
        dbService = AwaDatabaseService.createProxy(vertx, awaDbQueue);

        /* get Http or Https instance of HttpServer depending on test running status */
        HttpServer server = HttpServerFactory(config().getBoolean("isTesting", false));

        /* Create an authentication provider using JDBCAuth Provider */
        auth_username = mongoAuthFactory("username");
        auth_mail = mongoAuthFactory("mail");

        /* Create jwt authentication provider */
        jwtAuth = jwtAuthFactory();

        /* Main Router Handler Definition */
        Router router = Router.router(vertx);

        // set up handler which add all security headers on every single routes
        router.route().handler(this::securityHandler);

        // setting static handler to serve ressources bundle by webpack
        router.route("/").handler(StaticHandler.create().setCachingEnabled(true).setWebRoot("src/frontend"));
        router.route("/dist/*").handler(StaticHandler.create().setCachingEnabled(false).setWebRoot("src/frontend/dist"));
        router.get("/").handler(context -> context.reroute("/app/index.html"));

        // We install a user session/Cookie handler for all routes

        router.route().handler(CookieHandler.create());
        router.route().handler(SessionHandler.create(LocalSessionStore.create(vertx))
                                             .setCookieHttpOnlyFlag(true)
                                             .setCookieSecureFlag(true));
        router.route().handler(UserSessionHandler.create(auth_username));

        // Register/logout
        router.post("/register").handler(this::registerHandler);
        router.post("/retrieve_password").handler(this::retrievePasswordHandler);
        router.post("/reset_password").handler(this::resetPasswordHandler);

        router.post("/login").handler(this::loginHandler);
        router.get("/activate_account").handler(this::activateAccountHandler);

        /* Auth Router Handler definition */
        Router authRouter = Router.router(vertx); // Router accessible through session
        Router jwtAuthRouter = Router.router(vertx); // through jwt

        // Install jwtAuthHandler for all routes where authentication is required i.e all routes of subrouter authRouter
        authRouter.route().handler(RedirectAuthHandler.create(auth_username, "/#/login"));
        jwtAuthRouter.route().handler(JWTAuthHandler.create(jwtAuth));

        // Define auth/routes
        jwtAuthRouter.get("/logout").handler(this::logoutHandler);

        // manage user profil
        jwtAuthRouter.get("/user_profil").handler(this::getUserProfilHandler);
        jwtAuthRouter.post("/user_profil").handler(this::updateUserProfilHandler);

        // manage streamer profil
        jwtAuthRouter.put("/streamer_profil").handler(this::registerStreamerProfilHandler);
        jwtAuthRouter.get("/streamer_profil").handler(this::getStreamerProfilHandler);
        jwtAuthRouter.post("/streamer_profil").handler(this::updateStreamerProfilHandler);

        jwtAuthRouter.get("/private_streamers").handler(this::privateStreamersHandler);
        jwtAuthRouter.get("/shared_streamers").handler(this::sharedStreamersHandler);
        jwtAuthRouter.get("/streamer_page").handler(this::getStreamerPageHandler);
        jwtAuthRouter.get("/streamer_live").handler(this::getStreamerLiveHandler);
        jwtAuthRouter.get("/stream_key").handler(this::getStreamKeyHandler);

        jwtAuthRouter.post("/toggle_favorite").handler(this::toggleFavoriteHandler);

        // create sockJs bridge for chat
        SockJSHandler sockJSHandler = SockJSHandler.create(vertx);
        BridgeOptions options = new BridgeOptions()
                                    .addInboundPermitted(new PermittedOptions().setAddressRegex("[0-9a-z]+_[a-zA-Z0-9_-]+"))
                                    .addOutboundPermitted(new PermittedOptions().setAddressRegex("[0-9a-z]+_[a-zA-Z0-9_-]+"));
        sockJSHandler.bridge(options);
        authRouter.route("/chat/*").handler(sockJSHandler);

        // Mount subrouter
        router.mountSubRouter("/auth", authRouter);
        router.mountSubRouter("/jwt", jwtAuthRouter);

        /* Server Logic */
        server
                .requestHandler(router::accept)
                .listen(portNumber, ar -> {
                    if (ar.succeeded()) {
                        LOGGER.info("HTTPS server running on port " + portNumber);
                        startFuture.complete();
                    } else {
                        LOGGER.error("Could not start a HTTPS server", ar.cause());
                        startFuture.fail(ar.cause());
                    }
                });

        /* Mail Service */
        MailConfig config = new MailConfig()
                                            .setKeepAlive(true)
                                            .setHostname("ssl0.ovh.net")
                                            .setPort(465)
                                            .setStarttls(StartTLSOptions.REQUIRED)
                                            .setLogin(LoginOption.REQUIRED)
                                            .setSsl(true)
                                            .setUsername("no-reply@awa.ovh")
                                            .setPassword("!j8^F*8JQnp?");

        mailClient = MailClient.createNonShared(vertx, config);

    }

    /*
        Create and return a Mongo Authentication handler in two flavor depending on
        methode parameter
        param method: String, method used to authenticate a user, "mail" or "username"
        return a MongoAuthProvider which use either username or mail to authenticate a user
    */
    private MongoAuth mongoAuthFactory(String method){
        MongoClient client = MongoClient.createShared(vertx, new JsonObject().put("connection_string", "mongodb://localhost:27017")
                                                                             .put("db_name", "awa"));

        MongoAuth authProvider = MongoAuth.create(client, new JsonObject());

        authProvider.setCollectionName("users");
        authProvider.setUsernameField(method.equals("username") ? "username" : "mail");

        return authProvider;
    }



    /*
        Create and return a JWTAuth
     */
    private JWTAuth jwtAuthFactory(){
        JWTAuthOptions config = new JWTAuthOptions()
                .setKeyStore(new KeyStoreOptions()
                        .setPath(CONFIG_HTTP_JCEKS_KEYSTORE_FILE)
                        .setType("jceks")
                        .setPassword("hjK!ln,12?jg"));

        return JWTAuth.create(vertx, config);
    }

    /*
        HttpServerFactory, create HttpServer with or without ssl (for tests)
    */
    private HttpServer HttpServerFactory(boolean isTesting){

        HttpServer server;
        if (isTesting){
            server = vertx.createHttpServer();
        } else {
            server = vertx.createHttpServer(new HttpServerOptions()
                                                        .setSsl(true)
                                                        .setKeyStoreOptions(new JksOptions()
                                                                .setPath(CONFIG_HTTP_JKS_KEYSTORE_FILE)
                                                                .setPassword("hTvejk%+X23")))
                          .exceptionHandler(error -> {
                              LOGGER.error("Http server exception " + error.getLocalizedMessage());});
        }
        return server;
    }

    /*
        Mail Factory, create and return an mail message
     */
    private MailMessage mailFactory(String type, String dest, String username, String key){
        MailMessage msg = new MailMessage()
                .setFrom("no-reply@awa.ovh (AwA Streaming Platform)")
                .setTo(dest);

        // string style
        String body_style = "background-color:grey; font-family: \"MS Sans Serif\", Geneva, sans-serif;margin:0; padding:0;";
        String card_style = "display=block; margin: auto; width: 500px;";
        String head_style = "display:block; background-color:#0d47a1; padding: 2%;";
        String content_style = "background-color: white;padding: 5%;";
        String p_style = "p text-align:justify;";
        String h1_style = "margin-top:0;";
        String button_style = "background-color:#0d47a1;color: rgba(255, 255, 255, .87);font-size:1.1em;" +
                              "display:inline-block;font-weight:bold;padding:10px;text-decoration:none;border:0;border-radius:10px;";
        String links_style = "font-size:0.9em;text-align:center;";
        String links_a_style = "margin: 0 5px";
        String bottom_style = "text-align:center; color: grey; font-size:0.8em;";

        // add html tag, headers
        String html = "<!doctype html>\n<html>\n<head>\n" +
                      "<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>\n" +
                      "<style>" +
                      "@media screen and (max-width: 320px) {.card {width: 100%!important;}}" +
                      "@media screen and (max-width: 320px) and (max-width: 480px) {.card {width: 90vw!important;}}" +
                      "@media screen and (min-width: 360px) and (max-width: 640px) {.card {width: 80vw!important;}}" +
                      "</style>" +
                      "</head>\n<body style='" + body_style + "'>\n" +
                      "<table style='" + card_style + "'>\n";

        // add blue header with awa logo
        html += "<tr >\n<td style='" + head_style + "'>\n" +
                "<img width='50px' src=\"https://www.awa.ovh/dist/logo-white.png\"/>\n</td>\n</tr>";
        // open content div
        html += "<tr>\n<td style='" + content_style + "'>\n";

        switch (type){
            case "activation":
                msg.setSubject("Bienvenue sur AwA");
                // add h1
                html += "<div>\n<h1 style='" + h1_style +"'>Bienvenue " + username + ".</h1>\n";
                // add message
                html += "<p style='" + p_style + "'>Pour finaliser votre inscription sur AwA, confirmez votre adresse " +
                        "mail. Rien de plus simple, il suffit de cliquer sur le boutton ci-dessous.</p>\n";
                // add button
                html += "<a href=\"https://www.awa.ovh/activate_account?username=" + username + "&activationKey=" + key +
                        "\" style='" + button_style + "'>Activer votre compte</a>\n</div>\n<br>\n<br>";
                break;
            case "reset":
                msg.setSubject("Réinitialisation du mot de passe");
                // add h1 content
                html += "<div><h1 style='" + h1_style +"'>Bonjour " + username + ".</h1>\n";
                // add message
                html += "<p style='" + p_style + "'>Vous avez fait une demande de réinitialisation de votre de passe." +
                        " Pour finir la procédure merci de cliquer sur le lien ci-dessous.</p>\n";
                // add button
                html += "<a href=\"https://www.awa.ovh/#/retrieve_password?username=" + username + "&activationKey=" + key +
                        "\" style='" + button_style + "'>Réinitialiser le mot de passe</a>\n</div>\n<br>\n<br>";

                break;
            default:
                break;
        }
        // links
        html += "<div style='" + links_style + "'>\n" +
                "<a href='link1' style='" + links_a_style + "'>A propos</a>|\n" +
                "<a href='link1' style='" + links_a_style + "'>Se désinscrire</a>|\n" +
                "<a href='link1' style='" + links_a_style + "'>Ce n'est pas mon compte</a>" +
                "\n</div>\n<br>";
        // bottom
        html += "<div style='" + bottom_style + "'>\nAwA Streaming App \n<br>" +
                "Adresse siege social\n<br>\n75003 Paris CEDEX\n<br>\n</div>\n</div>\n</td>\n</tr>\n</table>\n</body>\n</html>";

        msg.setHtml(html);

        return msg;
    }

    /*
        Security handler, add header to secure http traffic
        Pipelined on each routes
    */
    private void securityHandler(RoutingContext context){
        context.response()
        // do not allow proxies to cache the data
        // .putHeader("Cache-Control", "no-store, no-cache")
        // prevents Internet Explorer from MIME - sniffing a
        // response away from the declared content-type
        .putHeader("X-Content-Type-Options", "nosniff")
        // Strict HTTPS (for about ~6Months)
        .putHeader("Strict-Transport-Security", "max-age=" + 15768000 + ";includeSubDomains")
        // IE8+ do not allow opening of attachments in the context of this resource
        .putHeader("X-Download-Options", "noopen")
        // enable XSS for IE
        .putHeader("X-XSS-Protection", "1; mode=block")
        // deny frames
        .putHeader("X-FRAME-OPTIONS", "SAMEORIGIN")
        // set referrer policy
        .putHeader("Referrer-Policy", "no-referrer-when-downgrade")
        // set source authorize by the browser to load
        .putHeader("Content-Security-Policy", "style-src 'self' 'unsafe-inline' https://fonts.googleapis.com;");
        context.next();
    }


    /*
        Handler in charge of authentication management to log Users to AwA
        No authentication handler set up on this route
        On login success we retrieve user info on db to generate token and send it back to user
        @route POST /login
        @body A JsonObject with username, password and method (either username or mail)
        @responseOK a jwt token base64 encoded
        @responseBAD a msg about what goes wrong
     */
    private void loginHandler(RoutingContext context){
        context.request().bodyHandler(bodyHandler -> {
            JsonObject login_info = bodyHandler.toJsonObject();

            // using the authentication adapted to the params (username or mail)
            MongoAuth auth = (login_info.getString("method").equals("username") ? auth_username : auth_mail);

            auth.authenticate(login_info, authResult -> {

                if (authResult.succeeded()) {
                    LOGGER.info("LoginHandler -> Authentication successfull through loginHandler for user: " + login_info.getString("username"));

                    // prepare JsonObject to give to dbService.getUserInfo
                    JsonObject user_info = new JsonObject();
                    if (login_info.getString("method").equals("username")) {
                        user_info.put("username", login_info.getString("username"));
                    }

                    if (login_info.getString("method").equals("mail")){
                        user_info.put("mail", login_info.getString("username"));
                    }

                    user_info.put("activated", true);

                    dbService.getUserProfil(user_info, reply -> {
                        if (reply.succeeded()) {
                            LOGGER.info("LoginHandler -> Got user infos, generate token and send it back to user:" + login_info.getString("username"));

                            String token = jwtAuth.generateToken(reply.result());
                            context.response().putHeader("Content-Type", "application/json").end(token);
                        } else {
                            context.request().response()
                                    .putHeader("content-type", "application/json; charset=utf-8")
                                    .setStatusCode(400).end(reply.cause().getMessage());
                        }
                    });


                } else {
                    LOGGER.warn("LoginHandler -> Authentication fail through loginHandler :" + authResult.cause().getMessage());

                    String error_msg;
                    if (authResult.cause().getMessage().startsWith("No account")){
                        error_msg = login_info.getString("method").equals("mail") ? "Pas de compte trouvé avec cet e-mail"
                                                                                       : "Pas de compte trouvé avec ce nom d'utilisateur";
                    } else if (authResult.cause().getMessage().startsWith("Invalid")){
                        error_msg = login_info.getString("method").equals("mail") ? "E-mail/mot de passe invalide"
                                                                                       : "Nom d'utilisateur/mot de passe invalide";
                    } else {
                        error_msg = authResult.cause().getMessage();
                    }

                    context.request().response()
                            .putHeader("content-type", "application/json; charset=utf-8")
                            .setStatusCode(404)
                            .end(error_msg);  // Failed login
                }
            });
        });
    }

    /*
        Handler retrieving form information in body to register a new user.
        After inserting user in db try to send activation mail, if mail domain isn't valid rollback user in db
        @route POST /register
        @body A JsonObject with username, password, firstName, lastName, mail, birthdate
        @responseOK no msg, just status code
        @responseBAD a msg if the username or mail is already taken by another user
     */
    private void registerHandler(RoutingContext context){
        HttpServerRequest req = context.request();

        req.bodyHandler(bodyHandler -> {
            JsonObject registerInfo = bodyHandler.toJsonObject();

            User newUser;
            try{
                newUser = new User(registerInfo, auth_username);
            } catch (KeyException e){
                LOGGER.error("registerHandler -> Forged request received: " + e.getMessage());
                req.response().setStatusCode(400).end();
                return;
            }

            dbService.registerUser(JsonObject.mapFrom(newUser), reply -> {
                if (reply.succeeded()){
                    LOGGER.info("registerHandler -> Successfuly register " + newUser.getUsername());

                    // send confirmation email
                    MailMessage message = mailFactory("activation", registerInfo.getString("mail"),
                                                                          newUser.getUsername(),
                                                                          newUser.getActivationKey());

                    mailClient.sendMail(message, result -> {
                        if (result.succeeded()) {
                            LOGGER.info("registerHandler -> Mail sent to confirm registration");
                            req.response().setStatusCode(200)
                                    .putHeader("content-type", "application/json; charset=utf-8")
                                    .end();
                        } else {
                            LOGGER.error("registerHandler -> fail to send mail to user: " + newUser.getUsername());

                            // if sending mail failed, we delete user from DB
                            dbService.deleteUser(JsonObject.mapFrom(newUser), ans ->{
                                if (ans.succeeded()){
                                    LOGGER.info("registerHandler -> successfully rollBack user:" + newUser.getUsername());
                                } else {
                                    LOGGER.error("registerHandler -> fail to rollBack user:" + newUser.getUsername());
                                }
                                req.response().setStatusCode(400)
                                        .putHeader("content-type", "application/json; charset=utf-8")
                                        .end("Impossible d'envoyer un email à cette adresse. " +
                                                    "Vérifier la ou réessayer ultérieurement.");
                            });

                        }
                    });

                // if request failed, it's mean that the document User doesn't match the validation of jsonSchema
                // or uniqueness constraint on username or mail
                } else {
                    req.response().setStatusCode(400)
                            .putHeader("content-type", "application/json; charset=utf-8")
                            .end(reply.cause().getMessage());
                }
            });
        });
    }


    /*
        Activation account handler, check that the username and the activation key are in base.
        if they are, activate the account and change activation key (because we use the key in password reset)
        @route GET /activate_account
        @params username and activationKey
        @responseOK redirect 302 on home page
        @responseBAD redirect 302 on home page with params (we assume that the user has already been activated)
     */
    private void activateAccountHandler(RoutingContext context){

        String username = context.request().getParam("username");
        String activationKey = context.request().getParam("activationKey");

        if (username == null || activationKey == null || username.isEmpty() || activationKey.isEmpty()){
            LOGGER.error("activateAccountHandler -> forged request received, user: " + username);
            context.request().response().setStatusCode(400).end();
        } else {

            JsonObject params = new JsonObject().put("username", username)
                    .put("activationKey", activationKey)
                    .put("activated", false);

            dbService.activateAccount(params, reply -> {

                if (reply.succeeded()) {
                    LOGGER.info("activateAccountHandler -> Account activated successfully, username: " +
                                context.request().getParam("username"));
                    context.request().response().setStatusCode(302).putHeader("location", "/#/login/home/0").end();

                } else {
                    context.request().response().setStatusCode(302).putHeader("location", "/#/?activationAlreadyDone=true").end();
                }
            });
        }
    }

    /*
        Clear user to logout
        @route GET /jwt/logout
     */
    private void logoutHandler(RoutingContext context){
        LOGGER.info("Logout -> user_id:" + context.user().principal().getInteger("user_id"));
        context.clearUser();
        context.response()
                .putHeader("content-type", "application/json; charset=utf-8")
                .setStatusCode(200).end();
    }


    /*
        Retrieve password Handler, check it the user exist, then send him a mail with a link
        to reset his password
        @route POST /retrieve_password
        @body username or mail
        @response HTTP/200
     */
    private void retrievePasswordHandler(RoutingContext context){
        HttpServerRequest req = context.request();

        req.bodyHandler(bodyHandler -> {
            JsonObject user = bodyHandler.toJsonObject();

            // even if validation is done on client side, we protect us from forged request with wrong params
            if (user.fieldNames().size() != 1 || !(user.fieldNames().contains("username") || user.fieldNames().contains("mail"))){
                req.response().setStatusCode(400).end();
                LOGGER.error("retrievePasswordHandler -> Forged request with wrong params receive and dropped");
                return;
            }

            dbService.updateKey(user, reply -> {
                if (reply.succeeded()){

                    JsonObject result = reply.result();
                    LOGGER.info("retrievePasswordHandler -> Successfully update key, ready to send mail");

                    // send confirmation email
                    MailMessage message = mailFactory("reset", result.getString("mail"),
                            result.getString("username"),
                            result.getString("newKey"));

                    mailClient.sendMail(message, ans -> {
                        if (ans.succeeded()) {
                            LOGGER.info("retrievePasswordHandler -> Mail sent to reset password for:" + user.getString
                                    ("username"));

                            req.response().setStatusCode(200)
                                    .putHeader("content-type", "application/json; charset=utf-8")
                                    .end();
                        } else {
                            LOGGER.warn("retrievePasswordHandler -> fail to send mail");
                            req.response().setStatusCode(500)
                                    .putHeader("content-type", "application/json; charset=utf-8")
                                    .end(Json.encodePrettily("La procédure de récupération a echouée," +
                                                                  " veuillez essayer ultérieuement"));
                        }
                    });

                } else {
                    req.response().setStatusCode(400)
                            .putHeader("content-type", "application/json; charset=utf-8")
                            .end(reply.cause().getMessage());
                }
            });
        });
    }

    /*
        Handler to reset password. When retrieving a password we send a mail to user with a link to a page on awa
        In this link there is activationKey and username as URL params
        We get those in the view to send a proper json with all information to validate and reset user password
        @route POST /reset_password
        @body username, activationKey and password
        @responseOK HTTP/200
     */
    private void resetPasswordHandler(RoutingContext context) {
        HttpServerRequest req = context.request();

        req.bodyHandler(bodyHandler -> {
            JsonObject params = bodyHandler.toJsonObject();

            // even if validation is done on client side, we protect us from forged request with wrong params
            if (params.fieldNames().size() != 3 || !(params.fieldNames().containsAll(new ArrayList<String>() {
                { add("activationKey"); }
                { add("username"); }
                { add("password"); }
            }))) {
                req.response().setStatusCode(400).end();
                LOGGER.error("resetPasswordHandler -> Forged request with wrong params receive and dropped");
                return;
            }

            // even if validation is done on client side, we protect us from forged request with empty params
            for (String k : params.fieldNames()) {
                if (params.getString(k).equals("")) {
                    req.response().setStatusCode(400).end();
                    LOGGER.error("resetPasswordHandler -> Forged request with empty params receive and dropped, username: " +
                            params.getString("username"));
                    return;
                }
            }

            dbService.resetPassword(params, reply -> {
                if (reply.succeeded()) {
                    LOGGER.info("resetPasswordHandler -> password reseted successfully, username:" + params.getString
                            ("username"));
                    req.response().setStatusCode(200)
                            .putHeader("content-type", "application/json; charset=utf-8")
                            .end();

                } else {
                    req.response().setStatusCode(400)
                            .putHeader("content-type", "application/json; charset=utf-8")
                            .end("La réinitialisation a echouée !");
                }
            });
        });
    }

    /*
        Handler retrieving user's profil informations
        @route GET /jwt/user_profil
        @response a JSonObject with user profil i.e _id, username, mail, picture, streamerId
     */
    private void getUserProfilHandler(RoutingContext context){

        JsonObject query = new JsonObject().put("_id", context.user().principal().getString("_id"));

        // get user info
        dbService.getUserProfil(query, res -> {
           if (res.succeeded()){
               LOGGER.info("getUserProfilHandler -> Successfully got user informations");
               context.request().response().setStatusCode(200).end(Json.encodePrettily(res.result()));
           } else {
               context.request().response().setStatusCode(400).end(res.cause().getMessage());
           }
        });
    }


    /*
        Handler updating user profil parameters
        @route POST /jwt/user_profil
        @body one, several or all the following attributes: picture, mail
        @response A json array with all the value updated on db. For picture send back the picture path
     */
    private void updateUserProfilHandler(RoutingContext context){
        context.request().bodyHandler( bodyHandler -> {

            String id = context.user().principal().getString("_id");
            JsonObject update_query = bodyHandler.toJsonObject();

            // even if validation is done on client side, we protect us from forged request with wrong params
            for (String key: update_query.fieldNames()){
                if (!key.equals("mail") && !key.equals("picture")){
                    context.request().response().setStatusCode(400).end();
                    LOGGER.error("postUserProfilHandler -> Forged request with wrong params receive and dropped for user: " + id);
                    return;
                } else {
                    if (update_query.getString(key).equals("")){
                        context.request().response().setStatusCode(400).end();
                        LOGGER.error("postUserProfilHandler -> Forged request with wrong params receive and dropped for user: "
                                + id);
                        return;
                    }
                }
            }

            // prepare async actions with future initialization
            Future<Void> fsFuture = Future.future();
            Future<JsonObject> queryFuture = Future.future();

            // if update query contain picture parameter we extract it and replace it with picture path in update query
            // and then convert base64 image to write on FS the file
            String picture, picture_path;
            if (update_query.fieldNames().contains("picture")){
                // handle post params
                picture = (String)update_query.remove("picture");
                picture_path = "/dist/static/" + id + ".png";
                update_query.put("picture", picture_path);

                // check with rounded value that the image isn't too big
                if (picture.length() > 4.5*1024*1024) {
                    LOGGER.error("updateUserProfilHandler -> File too big, user:" + id);
                    context.request().response()
                            .putHeader("content-type", "application/json; charset=utf-8")
                            .setStatusCode(400)
                            .end("Erreur ! Fichier image trop volumineux.");
                    return;
                }
                // convert picture
                String base64Image = picture.split(",")[1];
                byte[] imageBytes = DatatypeConverter.parseBase64Binary(base64Image);
                // write picture on fs
                Buffer imgBuffer = Buffer.buffer(imageBytes);
                vertx.fileSystem().writeFile("src/frontend/" + picture_path, imgBuffer, fsFuture.completer());

            // else we simply complete fs future
            } else {
                fsFuture.complete();
            }
            // async mongo request
            dbService.updateUserProfil(id, update_query, queryFuture);

            // compose actions
            CompositeFuture.all(fsFuture, queryFuture).setHandler( res -> {
                if (res.succeeded()) {
                    LOGGER.info("updateUserProfilHandler -> update successfully user: " + id);
                    context.request().response()
                            .putHeader("content-type", "application/json; charset=utf-8")
                            .setStatusCode(200)
                            .end(Json.encodePrettily(update_query));
                } else {

                    String error;
                    if (queryFuture.isComplete() && !queryFuture.succeeded()){
                        error = queryFuture.cause().getMessage();
                        LOGGER.error("updateUserProfilHandler -> error on update query, user: " + id);
                    } else {
                        LOGGER.error("updateUserProfilHandler -> error on write new image on fs, user: " + id);
                        error = "Erreur ! Veuillez réessayer.";
                    }

                    context.request().response()
                            .putHeader("content-type", "application/json; charset=utf-8")
                            .setStatusCode(400)
                            .end(error);
                }
            });
        });
    }

    /*
        Handler updating streamer profil
        @route PUT /jwt/streamer_profil
        @body a json object with at least the following params bic, doPrivate, genres, iban, picture, telephoen
              and potentially bio, dispos and price
        @response a json object with all the fields
    */
    private void registerStreamerProfilHandler(RoutingContext context){
        context.request().bodyHandler( bodyHandler -> {

            String id = context.user().principal().getString("_id");
            JsonObject insert_query = bodyHandler.toJsonObject();

            // if user has already a streamer account, return 400
            if (context.user().principal().getString("streamerId") != null){
                context.request().response().setStatusCode(400).end();
                LOGGER.error("registerStreamerAccountHandler -> User has already a streamer account, user: " + id);
                return;
            }

            // even if validation is done on client side, we protect us from forged request with wrong params
            List<String> required_list = Arrays.asList("bic", "dispos", "doPrivate", "genres", "iban", "picture", "price",
                                                       "telephone");

            // on register if doPrivate == false price and dispos may not be present, we add them here
            // NB we prefer to add them here over sending empty array/value on network
            if (!insert_query.getBoolean("doPrivate")){
                if (insert_query.getString("price") == null) insert_query.put("price", "0");
                if (insert_query.getJsonArray("dispos") == null) insert_query.put("dispos", new JsonArray());
            }

            Set insert_key = insert_query.fieldNames();
            for (String key: required_list){
                if (!insert_key.contains(key)){
                    context.request().response().setStatusCode(400).end();
                    LOGGER.error("registerStreamerAccountHandler -> Forged request with wrong params receive and dropped for user: " + id);
                    return;
                }
            }

            // if we have all required params ensure that if we have one extra param it's streamer bio
            if (insert_key.size() == required_list.size() + 3 && !insert_key.contains("bio")) {
                context.request().response().setStatusCode(400).end();
                LOGGER.error("registerStreamerAccountHandler -> Forged request with wrong params receive and dropped for user: " + id);
                return;
            }

            // prepare async actions with future initialization
            Future<Void> fsFuture = Future.future();
            Future<JsonObject> queryFuture = Future.future();

            // if update query contain picture parameter we extract it and replace it with picture path in update query
            // and then convert base64 image to write on FS the file
            String picture, picture_path;
            picture = (String)insert_query.remove("picture");
            picture_path = "/dist/static/" + id + "_stream.png";
            insert_query.put("picture", picture_path);
            insert_query.put("onAir", false);
            insert_query.put("streamKey", DefaultHashStrategy.generateSalt());
            insert_query.put("streamKeyExpiration", 0);

            // we had username to streamer document, to be able to query only streamer collection when displaying streamers
            insert_query.put("username", context.user().principal().getString("username"));

            // check with rounded value that the image isn't too big
            if (picture.length() > 4.5*1024*1024) {
                LOGGER.error("registerStreamerAccountHandler -> File too big, user:" + id);
                context.request().response()
                        .putHeader("content-type", "application/json; charset=utf-8")
                        .setStatusCode(400)
                        .end("Erreur ! Fichier image trop volumineux.");
                return;
            }

            // check bio size
            if (insert_key.contains("bio") && insert_query.getString("bio").length() > 15000000) {
                LOGGER.error("registerStreamerAccountHandler -> Bio too big, user:" + id);
                context.request().response()
                        .putHeader("content-type", "application/json; charset=utf-8")
                        .setStatusCode(400)
                        .end("Erreur ! Description trop volumineuse !");
                return;
            }

            // convert picture
            String base64Image = picture.split(",")[1];
            byte[] imageBytes = DatatypeConverter.parseBase64Binary(base64Image);
            // write picture on fs
            Buffer imgBuffer = Buffer.buffer(imageBytes);
            vertx.fileSystem().writeFile("src/frontend/" + picture_path, imgBuffer, fsFuture.completer());

            // async mongo request
            dbService.registerStreamerProfil(id, insert_query, queryFuture);

            // compose actions
            CompositeFuture.all(fsFuture, queryFuture).setHandler( res -> {
                if (res.succeeded()) {
                    // add streamerId to user principal to use it on future update query on streamer profil
                    context.user().principal().remove("streamerId");
                    context.user().principal().put("streamerId", queryFuture.result().getString("streamerId"));

                    insert_query.put("streamerId", queryFuture.result().getString("streamerId"));

                    LOGGER.info("registerStreamerAccountHandler -> successfully register streamer account for user : " + id);
                    context.request().response()
                            .putHeader("content-type", "application/json; charset=utf-8")
                            .setStatusCode(200)
                            .end(Json.encodePrettily(insert_query));
                } else {

                    String error;
                    if (queryFuture.isComplete() && !queryFuture.succeeded()){
                        error = queryFuture.cause().getMessage();
                        LOGGER.error("registerStreamerAccountHandler -> error on query, user: " + id);
                    } else {
                        LOGGER.error("registerStreamerAccountHandler -> error on write new image on fs, user: " + id);
                        error = "Erreur ! Veuillez réessayer.";
                    }

                    context.request().response()
                            .putHeader("content-type", "application/json; charset=utf-8")
                            .setStatusCode(400)
                            .end(error);
                }
            });
        });
    }

    /*
        Retrieving streamer info for logged user
        @route GET /jwt/streamer_profil
        @response a jsonObject with the following fields: bic, doPrivate, genres, iban, picture, telephone, bio, dispos and price
     */
    private void getStreamerProfilHandler(RoutingContext context){

        String streamerId = context.user().principal().getString("streamerId");

        if (streamerId == null || streamerId.isEmpty()){
            LOGGER.error("getUserProfilHandler -> a use who isn't a streamer try to query streamer information, user_id: " +
            context.user().principal().getString("_id"));
            context.request().response().setStatusCode(400).end();
            return;
        }

       // get user info
        dbService.getStreamerProfil(streamerId, res -> {
            if (res.succeeded()){
                LOGGER.info("getStreamerProfilHandler -> Successfully got user informations");
                context.request().response().setStatusCode(200).end(Json.encodePrettily(res.result()));
            } else {
                context.request().response().setStatusCode(400).end(res.cause().getMessage());
            }
        });

    }

    /*
        Handler updating streamer profil
        @route POST /jwt/streamer_profil
        @body a json object with one, several ar all the following params bic, doPrivate, genres, iban, picture,
              telephone, bio, dispos and price
        @response a json object with all the fields that has been updated
    */
    private void updateStreamerProfilHandler(RoutingContext context){
        context.request().bodyHandler( bodyHandler -> {

            String id = context.user().principal().getString("_id");
            String streamerId = context.user().principal().getString("streamerId");

            if (streamerId == null){
                LOGGER.error("updateStreamerProfilHandler -> a non streamer user try to update his streamer account, user_id: " +
                        id);
                context.request().response().setStatusCode(400).end();
                return;
            }

            JsonObject update_query = bodyHandler.toJsonObject();
            // even if validation is done on client side, we protect us from forged request with wrong params
            List valid_keys_list = Arrays.asList("bic", "bio", "dispos", "doPrivate", "genres", "iban", "picture", "price",
                    "telephone");

            for (String key: update_query.fieldNames()){
                if (!valid_keys_list.contains(key)){
                    context.request().response().setStatusCode(400).end();
                    LOGGER.error("updateStreamerProfilHandler -> Forged request with wrong params receive and dropped for user: " + id);
                    return;
                }
            }

            // check bio size
            if (update_query.fieldNames().contains("bio") && update_query.getString("bio").length() > 15000000) {
                LOGGER.error("registerStreamerAccountHandler -> Bio too big, user:" + id);
                context.request().response()
                        .putHeader("content-type", "application/json; charset=utf-8")
                        .setStatusCode(400)
                        .end("Erreur ! Description trop volumineuse !");
                return;
            }

            // prepare async actions with future initialization
            Future<Void> fsFuture = Future.future();
            Future<JsonObject> queryFuture = Future.future();

            // if update query contain picture parameter we extract it and replace it with picture path in update query
            // and then convert base64 image to write on FS the file
            String picture, picture_path;
            if (update_query.fieldNames().contains("picture")) {
                picture = (String) update_query.remove("picture");
                picture_path = "/dist/static/" + id + "_stream.png";
                update_query.put("picture", picture_path);

                // check with rounded value that the image isn't too big
                if (picture.length() > 4.5 * 1024 * 1024) {
                    LOGGER.error("updateStreamerProfilHandler -> File too big, user:" + id);
                    context.request().response()
                            .putHeader("content-type", "application/json; charset=utf-8")
                            .setStatusCode(400)
                            .end("Erreur ! Fichier image trop volumineux.");
                    return;
                }
                // convert picture
                String base64Image = picture.split(",")[1];
                byte[] imageBytes = DatatypeConverter.parseBase64Binary(base64Image);
                // write picture on fs
                Buffer imgBuffer = Buffer.buffer(imageBytes);
                vertx.fileSystem().writeFile("src/frontend/" + picture_path, imgBuffer, fsFuture.completer());
            } else {
                fsFuture.complete();
            }

            // async mongo request
            dbService.updateStreamerProfil(streamerId, update_query, queryFuture);

            // compose actions
            CompositeFuture.all(fsFuture, queryFuture).setHandler( res -> {
                if (res.succeeded()) {

                    LOGGER.info("updateStreamerProfilHandler -> successfully update streamer account for user : " + id);
                    context.request().response()
                            .putHeader("content-type", "application/json; charset=utf-8")
                            .setStatusCode(200)
                            .end(Json.encodePrettily(update_query));
                } else {

                    String error;
                    if (queryFuture.isComplete() && !queryFuture.succeeded()){
                        error = queryFuture.cause().getMessage();
                        LOGGER.error("updateStreamerProfilHandler -> error on update query, user: " + id);
                    } else {
                        LOGGER.error("updateStreamerProfilHandler -> error on write new image on fs, user: " + id);
                        error = "Erreur ! Veuillez réessayer.";
                    }

                    context.request().response()
                            .putHeader("content-type", "application/json; charset=utf-8")
                            .setStatusCode(400)
                            .end(error);
                }
            });
        });
    }


    /*
        Handler retrieving list of all streamer doing private sessions
        @route GET /jwt/private_streamers
        @response a JsonArray with all the private streamers. Each streamer is described with a jsonObject, with the following
                  fields: _id, dispos, genres, price, picture, username, followed, followers and potentially bio
     */
    private void privateStreamersHandler(RoutingContext context){
        String id = context.user().principal().getString("_id");

        dbService.fetchPrivateStreamers(id, res -> {

            if (res.succeeded()){
                context.request().response().putHeader("content-type", "application/json; charset=utf-8")
                        .end(Json.encodePrettily(res.result()));
            } else {
                context.request().response()
                        .putHeader("content-type", "application/json; charset=utf-8")
                        .setStatusCode(400)
                        .end(res.cause().getMessage());
            }
        });
    }

    /*
        Fetch streamer info from streamer with id streamer_id in params
        @route GET /jwt/streamer_page
        @response a JsonObject describing a streamer with the following fields: _id, price, dispos, genres, picture, username,
                  followed, followers and potentially bio
     */
    private void getStreamerPageHandler(RoutingContext context){
        String id = context.user().principal().getString("_id");
        String streamerId = context.request().getHeader("streamerId");

        dbService.getStreamerPage(id, streamerId, res -> {
            if (res.succeeded()){
                context.request().response().putHeader("content-type", "application/json; charset=utf-8")
                        .end(Json.encodePrettily(res.result()));
            } else {
                context.request().response()
                        .putHeader("content-type", "application/json; charset=utf-8")
                        .setStatusCode(400)
                        .end(res.cause().getMessage());
            }
        });
    }

    /*
        Handler retrieving list of all streamer on air
        @route GET /jwt/shared_streamers
        @response a JsonArray with all the shared streamers. Each streamer is described with a jsonObject, with the following
                  fields: _id, dispos, genres, price, picture, username, followed, followers and potentially bio
     */
    private void sharedStreamersHandler(RoutingContext context){
        String id = context.user().principal().getString("_id");

        dbService.fetchSharedStreamers(id, res -> {

            if (res.succeeded()){
                context.request().response().putHeader("content-type", "application/json; charset=utf-8")
                        .end(Json.encodePrettily(res.result()));
            } else {
                context.request().response()
                        .putHeader("content-type", "application/json; charset=utf-8")
                        .setStatusCode(400)
                        .end(res.cause().getMessage());
            }
        });
    }

    /*
        Fetch streamer info from streamer with id streamerId in params for live streamer only
        @route GET /jwt/streamer_live
        @params streamerId
        @response a json object with picture, bio, username, genres, followers_nb, followed, src, onAir
     */
    private void getStreamerLiveHandler(RoutingContext context){
        String id = context.user().principal().getString("_id");
        String streamerId = context.request().getHeader("streamerId");

        dbService.getStreamerLiveInfos(id, streamerId, res -> {
            if (res.succeeded()) {
                LOGGER.info("getStreamerHandler -> handler got access to DB successfully, request from user_id: " + id);
                context.request().response().putHeader("content-type", "application/json; charset=utf-8")
                        .end(Json.encodePrettily(res.result()));
            } else {
                context.fail(res.cause());
            }
        });
    }


    /*
        Generate a new streamKey and an expiration date, put them in db and send back the key to user
        @route GET /jwt/stream_key
        @response A json object with url and key to stream
     */
    private void getStreamKeyHandler(RoutingContext context){
        String id = context.user().principal().getString("_id");
        String streamerId = context.user().principal().getString("streamerId");

        if (streamerId == null){
            LOGGER.error("getStreamKeyHandler -> A user try to start a stream without streamerAccount, user_id:" + id);
            context.response().setStatusCode(404).putHeader("content-type", "application/json; charset=utf-8")
                    .end("Vous devez avoir un compte streamer pour pouvoir commencer un live.");
            return;
        }

        String key = DefaultHashStrategy.generateSalt().substring(0, 24);
        System.out.println(key.length());

        dbService.updateStreamKey(streamerId, key, reply -> {
            // on reply
            if (reply.succeeded()) {
                LOGGER.info("getStreamKeyHandler -> StreamKey added to DB, send it back to user_id:" + id);

                context.response().putHeader("content-type", "application/json; charset=utf-8")
                        .setStatusCode(200)
                        .end(Json.encodePrettily(
                                new JsonObject().put("url", "rtmp://awa.ovh/live/")
                                                .put("key", streamerId + "_" + key)
                        ));

            } else {
                context.fail(reply.cause());
            }
        });

    }

    /*
        Handler adding/removing follower to streamer
        @route POST /jwt/toggle_favorite
     */
    private void toggleFavoriteHandler(RoutingContext context){
        HttpServerRequest req = context.request();
        req.bodyHandler(bodyHandler -> {

            JsonObject body = bodyHandler.toJsonObject();
            String id = context.user().principal().getString("_id");

            Set<String> keys = body.fieldNames();
            // even if validation is done on client side, we protect us from forged request with wrong params
            if (keys.size() != 2 || !(keys.containsAll(new ArrayList<String>()
            {{add("streamerId"); add("isAdded");}}))){
                req.response()
                        .putHeader("content-type", "application/json; charset=utf-8")
                        .setStatusCode(400).end();
                LOGGER.error("toggleFavoriteHandler -> Forged request with wrong params receive and dropped, from user_id: " + id);
                return;
            }

            dbService.toggleFavorite(id, body.getString("streamerId"), body.getBoolean("isAdded"), reply -> {
                if (reply.succeeded()) {
                    LOGGER.info("toggleFavoriteHandler -> Favorite toggle successfully, for user_id: " + id);
                    req.response()
                            .putHeader("content-type", "application/json; charset=utf-8")
                            .setStatusCode(200).end();
                } else {
                    req.response().setStatusCode(400).end(reply.cause().getMessage());
                }
            });

        });
    }
}

