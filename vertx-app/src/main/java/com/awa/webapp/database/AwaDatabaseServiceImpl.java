package com.awa.webapp.database;

import com.mongodb.MongoCommandException;
import com.mongodb.MongoWriteException;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.mongo.MongoAuth;
import io.vertx.ext.auth.mongo.impl.DefaultHashStrategy;
import io.vertx.ext.auth.mongo.impl.MongoUser;
import io.vertx.ext.mongo.FindOptions;
import io.vertx.ext.mongo.MongoClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import java.util.List;


// tag::implementation[]
class AwaDatabaseServiceImpl implements AwaDatabaseService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AwaDatabaseServiceImpl.class);
    private static final String USERS_COLLECTION = "users";
    private static final String STREAMERS_COLLECTION = "streamers";
    private static final String SERVER_ERROR = "Internal server error";

    private final MongoClient dbClient;
    private final MongoAuth auth;

    public AwaDatabaseServiceImpl(MongoClient dbClient, MongoAuth auth, Handler<AsyncResult<AwaDatabaseService>> readyHandler) {
        this.auth = auth;
        this.dbClient = dbClient;

        // to check that mongoDB is up we try a simple query on it
        // it allows us to determine if the readyHandler should be succeeded or failed
        dbClient.count("users", new JsonObject(), result -> {
            if (result.succeeded()){
                readyHandler.handle(Future.succeededFuture(this));
            } else {
                readyHandler.handle(Future.failedFuture(result.cause()));
            }
        });

    }


    /*
        Insert user given as a JsonObject in parameter, used in register handler
     */
    @Override
    public AwaDatabaseService registerUser(JsonObject user, Handler<AsyncResult<JsonObject>> resultHandler){
        dbClient.insert(USERS_COLLECTION, user, res -> {
            if (res.succeeded()) {
                // on success, return user id
                resultHandler.handle(Future.succeededFuture());
            } else {
                if (((MongoWriteException) res.cause()).getCode() == 11000) {
                    String error_msg;
                    switch (res.cause().toString().split("index: ")[1].split("_")[0]){
                        case "username":
                            error_msg = "Nom d'utilisateur déjà utilisé";
                            break;
                        case "mail":
                            error_msg = "Adresse mail déjà utilisée";
                            break;
                        default:
                            error_msg = "Erreur, veuillez rééssayer";
                            break;
                    }
                    LOGGER.warn("registerUser -> Database query error for registerUser: " + error_msg);
                    resultHandler.handle(Future.failedFuture(error_msg));

                } else {
                    LOGGER.error("registerUser -> Database query error for registerUser. " + res.cause().getMessage() +
                            ". Data which failed:" + user.toString());
                    resultHandler.handle(Future.failedFuture("Erreur, veuillez rééssayer"));
                }

            }
        });
        return this;
    }

    /*
        Delete user matching the JsonObject given, used in register handler when mail unreachable (rollback user)
        @params user
     */
    @Override
    public AwaDatabaseService deleteUser(JsonObject user, Handler<AsyncResult<JsonObject>> resultHandler) {
        dbClient.removeDocument(USERS_COLLECTION, user, res -> {
            if (res.succeeded()){
                resultHandler.handle(Future.succeededFuture());
            } else {
                LOGGER.error("deleteUser -> Database query error", res.cause().getMessage());
                resultHandler.handle(Future.failedFuture(res.cause()));
            }
        });
        return this;
    }

    /*
        find a user matching activation key and username provided and then update profil (activated and activation_key)
        @param user
     */
    @Override
    public AwaDatabaseService activateAccount(JsonObject user, Handler<AsyncResult<JsonObject>> resultHandler) {

        JsonObject update = new JsonObject().put("$set", new JsonObject().put("activated", true)
                .put("activationKey", DefaultHashStrategy.generateSalt()));

        dbClient.findOneAndUpdate(USERS_COLLECTION, user, update, res -> {
            if (res.succeeded()){
                // if result is, null no matching document
                if (res.result() == null){
                    LOGGER.warn("activateAccount -> Incorrect activation link (forged or already activated), username: " + user.getString("username"));
                    resultHandler.handle(Future.failedFuture("activated"));
                } else {
                    resultHandler.handle(Future.succeededFuture());
                }

            } else {
                LOGGER.error("activateAccount -> query failed: " + res.cause().getMessage());
                resultHandler.handle(Future.failedFuture(res.cause()));
            }
        });
        return this;
    }

    /*
        Generate a new key (salt) and insert it on db for user with username on user jsonObject
        Used in retrieve password handler (reset password first step)
        Send back the key as a result to generate a mail from it
     */
    @Override
    public AwaDatabaseService updateKey(JsonObject user, Handler<AsyncResult<JsonObject>> resultHandler) {

        String newKey = DefaultHashStrategy.generateSalt();
        JsonObject update_query = new JsonObject().put("$set", new JsonObject().put("activationKey", newKey));

        dbClient.findOneAndUpdate(USERS_COLLECTION, user, update_query, res -> {
            if (res.succeeded()) {

                if (res.result() != null){
                    res.result().put("newKey", newKey);
                    resultHandler.handle(Future.succeededFuture(res.result()));
                } else {
                    LOGGER.warn("updateKey -> No username matching:" + user.toString());
                    resultHandler.handle(Future.failedFuture("Pas d'utilisateur connu avec cet identifiant"));
                }

            } else {
                LOGGER.error("updateKey -> Database query error " + res.cause().getMessage());
                resultHandler.handle(Future.failedFuture("La procédure de récupération a echouée, veuillez essayer " +
                        "ultérieuement"));
            }

        });
        return this;
    }

    /*
        Reset the password of the user mathcing the username and activationKey in params
        @params params json object with username, password (not hashed) and activationKey
     */
    @Override
    public AwaDatabaseService resetPassword(JsonObject params, Handler<AsyncResult<JsonObject>> resultHandler) {
        // we don't forget to renew activation key to make the link valid only once
        String newKey = DefaultHashStrategy.generateSalt();

        // generate a new salt and hash password
        DefaultHashStrategy hashStrategy = new DefaultHashStrategy();
        String salt = DefaultHashStrategy.generateSalt();
        MongoUser mongoUser = new MongoUser(params.getString("username"), auth);
        mongoUser.principal().put("salt", salt);
        String password = hashStrategy.computeHash((String)params.remove("password"), mongoUser);

        // create update query
        JsonObject update_query = new JsonObject().put("$set", new JsonObject().put("password", password)
                .put("salt", salt)
                .put("activationKey", newKey));

        dbClient.findOneAndUpdate(USERS_COLLECTION, params, update_query, res -> {
            if (res.succeeded()){

                if (res.result() != null){
                    resultHandler.handle(Future.succeededFuture());
                } else {
                    LOGGER.warn("resetPassword -> No user matching", res.cause());
                    resultHandler.handle(Future.failedFuture(res.cause()));
                }

            } else {
                LOGGER.error("resetPassword -> Database query error" + res.cause().getMessage());
                resultHandler.handle(Future.failedFuture(res.cause()));
            }
        });

        return this;
    }


    /*
        Get user infos. These infos are used on login
        param: user_info a JsonObject with username or else
     */
    @Override
    public AwaDatabaseService getUserProfil(JsonObject user_info, Handler<AsyncResult<JsonObject>> resultHandler) {

        dbClient.findOne(USERS_COLLECTION, user_info, new JsonObject().put("_id", 1)
                                                                      .put("username", 1)
                                                                      .put("mail", 1)
                                                                      .put("picture", 1)
                                                                      .put("streamerId", 1), res -> {
            if (res.succeeded()) {
                if (res.result() != null){
                    resultHandler.handle(Future.succeededFuture(res.result()));
                } else {
                    LOGGER.warn("getUserInfo -> No user matching: " + user_info.toString());
                    // if activated field in user_info the request is called from login hancler hence the msg
                    String msg = user_info.containsKey("activated") ? "Pas d'utilisateur trouvé avec ce compte. Pensez à " +
                            "l'activer." : SERVER_ERROR;

                    resultHandler.handle(Future.failedFuture(msg));
                }
            } else {
                LOGGER.error("getUserInfo -> query error " + res.cause().getMessage());
                resultHandler.handle(Future.failedFuture(SERVER_ERROR));
            }
        });
        return this;
    }



    @Override
    public AwaDatabaseService updateUserProfil(String id, JsonObject update_query, Handler<AsyncResult<JsonObject>> resultHandler) {
        dbClient.findOneAndUpdate(USERS_COLLECTION, new JsonObject().put("_id", id),
                                                    new JsonObject().put("$set", update_query), res ->{
            if (res.succeeded()){

                if (res.result() != null){
                    resultHandler.handle(Future.succeededFuture());
                } else {
                    LOGGER.warn("updateUserProfil -> No user matching id: " + id);
                    resultHandler.handle(Future.failedFuture(SERVER_ERROR));
                }

            } else {
                if (((MongoCommandException) res.cause()).getCode() == 11000) {
                    String error_msg;
                    switch (res.cause().toString().split("index: ")[1].split("_")[0]){
                        case "mail":
                            error_msg = "Adresse mail déjà utilisée";
                            break;
                        default:
                            error_msg = "Erreur, veuillez rééssayer";
                            break;
                    }
                    LOGGER.warn("registerUser -> Database query error for registerUser: " + error_msg);
                    resultHandler.handle(Future.failedFuture(error_msg));

                } else {
                    LOGGER.error("updateUserProfil -> query error ",res.cause());
                    resultHandler.handle(Future.failedFuture(SERVER_ERROR));
                }
            }
        });
        return this;
    }

    @Override
    public AwaDatabaseService registerStreamerProfil(String id, JsonObject insert_query, Handler<AsyncResult<JsonObject>>
            resultHandler){

        dbClient.insert(STREAMERS_COLLECTION, insert_query, res -> {

            if (res.succeeded()){
                // on insert success we complete user profil with streamerId key
                dbClient.findOneAndUpdate(USERS_COLLECTION, new JsonObject().put("_id", id),
                                                            new JsonObject().put("$set",
                                                                    new JsonObject().put("streamerId", res.result())), ans -> {
                    if (ans.succeeded()){
                        resultHandler.handle(Future.succeededFuture(new JsonObject().put("streamerId", res.result())));
                    } else {
                        // don't forget to remove invalid streamer document
                        dbClient.findOneAndDelete(USERS_COLLECTION, new JsonObject().put("_id", res.result()), async -> {});
                        LOGGER.error("registerStreamerProfil -> query error when updating user ", ans.cause());
                        resultHandler.handle(Future.failedFuture(SERVER_ERROR));
                    }
                });
            } else {

                if (((MongoWriteException) res.cause()).getCode() == 11000) {
                    String error_msg;
                    switch (res.cause().toString().split("index: ")[1].split("_")[0]){
                        case "telephone":
                            error_msg = "Téléphone déjà utilisé";
                            break;
                        case "iban":
                            error_msg = "IBAN déjà utilisé";
                            break;
                        case "bic":
                            error_msg = "BIC déjà utilisé";
                            break;
                        default:
                            error_msg = SERVER_ERROR;
                            break;
                    }
                    LOGGER.warn("registerStreamerProfil -> query error when inserting streamer:" + error_msg);
                    resultHandler.handle(Future.failedFuture(error_msg));

                } else {
                    LOGGER.error("registerStreamerProfil -> Database query error for registerUser. " + res.cause().getMessage() +
                            ". Data which failed:" + insert_query.toString());
                    resultHandler.handle(Future.failedFuture(SERVER_ERROR));
                }
            }
        });
        return this;
    }


    @Override
    public AwaDatabaseService getStreamerProfil(String id, Handler<AsyncResult<JsonObject>> resultHandler){

        dbClient.findOne(STREAMERS_COLLECTION, new JsonObject().put("_id", id), new JsonObject(), res -> {

            if (res.succeeded()) {
                if (res.result() != null){
                    resultHandler.handle(Future.succeededFuture(res.result()));
                } else {
                    LOGGER.warn("getStreamerProfil -> No streamer matching _id: " + id);
                    resultHandler.handle(Future.failedFuture(SERVER_ERROR));
                }
            } else {
                LOGGER.error("getStreamerProfil -> query error " + res.cause().getMessage());
                resultHandler.handle(Future.failedFuture(SERVER_ERROR));

            }
        });

        return  this;
    }

    @Override
    public AwaDatabaseService updateStreamerProfil(String streamerId, JsonObject update_query, Handler<AsyncResult<JsonObject>> resultHandler){
        dbClient.findOneAndUpdate(STREAMERS_COLLECTION, new JsonObject().put("_id", streamerId),
                new JsonObject().put("$set", update_query), res ->{
                    if (res.succeeded()){

                        if (res.result() != null){
                            resultHandler.handle(Future.succeededFuture());
                        } else {
                            LOGGER.warn("updateStreamerProfil -> No streamer matching id: " + streamerId);
                            resultHandler.handle(Future.failedFuture(SERVER_ERROR));
                        }

                    } else {
                        if (((MongoCommandException) res.cause()).getCode() == 11000) {
                            String error_msg;
                            switch (res.cause().toString().split("index: ")[1].split("_")[0]){
                                case "telephone":
                                    error_msg = "Téléphone déjà utilisé";
                                    break;
                                case "iban":
                                    error_msg = "IBAN déjà utilisé";
                                    break;
                                case "bic":
                                    error_msg = "BIC déjà utilisé";
                                    break;
                                default:
                                    error_msg = SERVER_ERROR;
                                    break;
                            }
                            LOGGER.warn("updateStreamerProfil -> query error when inserting streamer:" + error_msg);
                            resultHandler.handle(Future.failedFuture(error_msg));

                        } else {
                            LOGGER.error("updateStreamerProfil -> Database query error for registerUser. " + res.cause().getMessage() +
                                    ". Data which failed:" + update_query.toString());
                            resultHandler.handle(Future.failedFuture(SERVER_ERROR));
                        }
                    }
                });
        return this;
    }

    @Override
    public AwaDatabaseService fetchPrivateStreamers(String userId, Handler<AsyncResult<List<JsonObject>>> resultHandler){

        dbClient.findWithOptions(STREAMERS_COLLECTION, new JsonObject().put("doPrivate", true),
               new FindOptions().setFields(new JsonObject()
                                               .put("username", 1)
                                               .put("price", 1)
                                               .put("followers", 1)
                                               .put("picture", 1)
                                               .put("genres", 1)
                                               .put("dispos", 1))
        , res -> {
            if (res.succeeded()){
                for (JsonObject json: res.result()){
                    // determine if user follow streamers
                    JsonArray followers = (JsonArray)json.remove("followers");

                    if (followers != null && followers.contains(userId)){
                        json.put("followed", true);
                    } else {
                        json.put("followed", false);
                    }
                    // add followers number
                    json.put("followers_nb", followers != null ? followers.size(): 0);
                }

                resultHandler.handle(Future.succeededFuture(res.result()));

            } else {
               LOGGER.error("fetchPrivateStreamers -> Database query error", res.cause());
               resultHandler.handle(Future.failedFuture(res.cause()));
            }

        });
        return this;
    }

    @Override
    public AwaDatabaseService toggleFavorite(String userId, String streamerId, Boolean isAdded, Handler<AsyncResult<JsonArray>>
            resultHandler){

        JsonObject update = new JsonObject().put(isAdded ? "$addToSet":"$pull", new JsonObject().put("followers", userId));

        dbClient.findOneAndUpdate(STREAMERS_COLLECTION, new JsonObject().put("_id", streamerId), update, res -> {
            if (res.succeeded()){
                resultHandler.handle(Future.succeededFuture());
            } else {
                LOGGER.error("toggleFavorite -> Database query error", res.cause());
                resultHandler.handle(Future.failedFuture(res.cause()));
            }
        });
        return this;
    }


    @Override
    public AwaDatabaseService getStreamerPage(String userId, String streamerId, Handler<AsyncResult<JsonObject>> resultHandler) {
            dbClient.findOne(STREAMERS_COLLECTION, new JsonObject().put("_id", streamerId), new JsonObject()
                                                                                                .put("username", 1)
                                                                                                .put("price", 1)
                                                                                                .put("followers", 1)
                                                                                                .put("picture", 1)
                                                                                                .put("genres", 1)
                                                                                                .put("dispos", 1)
                                                                                                .put("bio", 1), res -> {
                if (res.succeeded()){
                    // determine if user follow streamers
                    JsonArray followers = (JsonArray)res.result().remove("followers");

                    if (followers != null && followers.contains(userId)){
                        res.result().put("followed", true);
                    } else {
                        res.result().put("followed", false);
                    }
                    // add followers number
                    res.result().put("followers_nb", followers != null ? followers.size(): 0);

                    resultHandler.handle(Future.succeededFuture(res.result()));

                } else {
                    LOGGER.error("fetchPrivateStreamer -> Database query error", res.cause());
                    resultHandler.handle(Future.failedFuture(res.cause()));
                }

                });
        return this;
    }

    @Override
    public AwaDatabaseService fetchSharedStreamers(String userId, Handler<AsyncResult<List<JsonObject>>> resultHandler){

        dbClient.findWithOptions(STREAMERS_COLLECTION, new JsonObject().put("onAir", true),
            new FindOptions().setFields(new JsonObject()
                    .put("username", 1)
                    .put("followers", 1)
                    .put("picture", 1)
                    .put("genres", 1))
            , res -> {
                if (res.succeeded()){
                    for (JsonObject json: res.result()){
                        // determine if user follow streamers
                        JsonArray followers = (JsonArray)json.remove("followers");

                        if (followers != null && followers.contains(userId)){
                            json.put("followed", true);
                        } else {
                            json.put("followed", false);
                        }
                        // add followers number
                        json.put("followers_nb", followers != null ? followers.size(): 0);
                    }

                    resultHandler.handle(Future.succeededFuture(res.result()));

                } else {
                    LOGGER.error("fetchSharedStreamers -> Database query error", res.cause());
                    resultHandler.handle(Future.failedFuture(res.cause()));
                }

            });
        return this;
    }

    @Override
    public AwaDatabaseService updateStreamKey(String id, String key, Handler<AsyncResult<JsonObject>> resultHandler) {

        // the key is available only 30 minutes (ie. 1000*60*30 ms)
        JsonObject update = new JsonObject().put("$set",
                new JsonObject().put("streamKey", key).put("streamKeyExpiration", System.currentTimeMillis() + 1000*60*30));

        dbClient.findOneAndUpdate(STREAMERS_COLLECTION, new JsonObject().put("_id", id), update, res -> {
            if (res.succeeded()){
                resultHandler.handle(Future.succeededFuture());
            } else {
                LOGGER.error("updateStreamKey -> Database query error", res.cause());
                resultHandler.handle(Future.failedFuture(SERVER_ERROR));
            }
        });
        return this;
    }
//            .put("src", "/hls/" + context.request().getHeader("streamer_id") + "_"  + results.getString(6) + ".m3u8")

    @Override
    public AwaDatabaseService getStreamerLiveInfos(String id, String streamerId, Handler<AsyncResult<JsonObject>> resultHandler) {
        System.out.println(streamerId);
        JsonObject filters = new JsonObject().put("bio", 1)
                                            .put("username", 1)
                                            .put("picture", 1)
                                            .put("genres", 1)
                                            .put("followers", 1)
                                            .put("streamKey", 1)
                                            .put("onAir", 1);

        dbClient.findOne(STREAMERS_COLLECTION, new JsonObject().put("_id", streamerId), filters, res -> {
            if (res.succeeded()){
                System.out.print(res.result());
                JsonArray followers = (JsonArray)res.result().remove("followers");

                if (followers != null && followers.contains(id)){
                    res.result().put("followed", true);
                } else {
                    res.result().put("followed", false);
                }
                // add followers number
                res.result().put("followers_nb", followers != null ? followers.size(): 0);

                // add src url
                String key = (String)res.result().remove("streamKey");
                res.result().put("src", streamerId + "_"  + key.substring(0, 10) + ".m3u8");

                resultHandler.handle(Future.succeededFuture(res.result()));
            } else {
                LOGGER.error("getStreamerLiveInfos -> Database query error", res.cause());
                resultHandler.handle(Future.failedFuture(SERVER_ERROR));
            }
        });
        return this;
    }

    @Override
    public AwaDatabaseService setOnAir(String id, String key, Handler<AsyncResult<JsonArray>> resultHandler) {

        JsonObject filter = new JsonObject().put("_id", id)
                .put("onAir", false)
                .put("streamKey", key)
                .put("streamKeyExpiration", new JsonObject().put("$gt", System.currentTimeMillis()));

        JsonObject update = new JsonObject().put("$set", new JsonObject().put("onAir", true));

        dbClient.findOneAndUpdate(STREAMERS_COLLECTION, filter, update, res -> {
            if (res.succeeded()){

                if (res.result() != null){
                    resultHandler.handle(Future.succeededFuture());
                } else {
                    resultHandler.handle(Future.failedFuture("Unauthorized stream"));
                }
            } else {
                LOGGER.error("setOnAir -> query error:", res.cause());
                resultHandler.handle(Future.failedFuture(res.cause()));
            }

        });
        return this;
    }

    @Override
    public AwaDatabaseService setOffAir(String id, Handler<AsyncResult<JsonArray>> resultHandler) {
        JsonObject filter = new JsonObject().put("_id", id);
        JsonObject update = new JsonObject().put("$set", new JsonObject().put("onAir", false));

        dbClient.findOneAndUpdate(STREAMERS_COLLECTION, filter, update, res -> {
            if (res.succeeded()){

                if (res.result() != null){
                    resultHandler.handle(Future.succeededFuture());
                } else {
                    resultHandler.handle(Future.failedFuture("Unauthorized stream"));
                }
            } else {
                LOGGER.error("setOffAir -> query error:", res.cause());
                resultHandler.handle(Future.failedFuture(res.cause()));
            }

        });
        return this;
    }

}
// end::implementation[]
