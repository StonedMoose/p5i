package com.awa.webapp.database;

import io.vertx.ext.auth.mongo.MongoAuth;
import io.vertx.ext.mongo.MongoClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.vertx.core.Future;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.JsonObject;
import io.vertx.serviceproxy.ProxyHelper;


public class AwaDatabaseVerticle extends AbstractVerticle {

    public static final String CONFIG_AWADB_QUEUE = "awadb.queue";
    private static final Logger LOGGER = LoggerFactory.getLogger(AwaDatabaseVerticle.class);

    @Override
    public void start(Future<Void> startFuture) throws Exception {

        JsonObject conf = new JsonObject().put("connection_string",
                                                "mongodb://localhost:" + config().getInteger("mongo.port", 27017))
                                                .put("db_name", "awa");
        MongoClient dbClient = MongoClient.createShared(vertx, conf);

        MongoAuth auth = MongoAuth.create(dbClient, new JsonObject());
        

        AwaDatabaseService.create(dbClient, auth, ready -> {
            if (ready.succeeded()) {
                ProxyHelper.registerService(AwaDatabaseService.class, vertx, ready.result(), CONFIG_AWADB_QUEUE);
                startFuture.complete();
            } else {
                startFuture.fail(ready.cause());
            }
        });
    }
}