/*
 * Author: Julien Lhermite
 * Packqage to handle database interaction in AWA webapp
 */
@ModuleGen(groupPackage = "com.awa.webapp.database", name = "awa-database")

package com.awa.webapp.database;
import io.vertx.codegen.annotations.ModuleGen;
