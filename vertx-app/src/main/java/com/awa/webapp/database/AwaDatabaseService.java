package com.awa.webapp.database;

import io.vertx.codegen.annotations.Fluent;
import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.mongo.MongoAuth;
import io.vertx.ext.mongo.MongoClient;

import java.util.List;


// tag::interface[]
@ProxyGen  // The ProxyGen annotation is used to trigger the code generation of a proxy for clients of that service.
public interface AwaDatabaseService {

    @Fluent// The Fluent annotation is optional, but allows fluent interfaces where operations can be chained by returning the service instance.
    AwaDatabaseService registerUser(JsonObject user, Handler<AsyncResult<JsonObject>> resultHandler);

    @Fluent
    AwaDatabaseService deleteUser(JsonObject user, Handler<AsyncResult<JsonObject>> resultHandler);

    @Fluent
    AwaDatabaseService activateAccount(JsonObject user, Handler<AsyncResult<JsonObject>> resultHandler);

    @Fluent
    AwaDatabaseService updateKey(JsonObject user, Handler<AsyncResult<JsonObject>> resultHandler);

    @Fluent
    AwaDatabaseService resetPassword(JsonObject params, Handler<AsyncResult<JsonObject>> resultHandler);

    @Fluent
    AwaDatabaseService getUserProfil(JsonObject user_info, Handler<AsyncResult<JsonObject>> resultHandler);

    @Fluent
    AwaDatabaseService updateUserProfil(String id, JsonObject update_query, Handler<AsyncResult<JsonObject>> resultHandler);

    @Fluent
    AwaDatabaseService registerStreamerProfil(String id, JsonObject insert_query, Handler<AsyncResult<JsonObject>> resultHandler);

    @Fluent
    AwaDatabaseService getStreamerProfil(String id, Handler<AsyncResult<JsonObject>> resultHandler);

    @Fluent
    AwaDatabaseService updateStreamerProfil(String streamerId, JsonObject update_query, Handler<AsyncResult<JsonObject>> resultHandler);

    @Fluent
    AwaDatabaseService fetchPrivateStreamers(String userId, Handler<AsyncResult<List<JsonObject>>> resultHandler);

    @Fluent
    AwaDatabaseService toggleFavorite(String userId, String streamerId, Boolean isAdded, Handler<AsyncResult<JsonArray>>resultHandler);

    @Fluent
    AwaDatabaseService getStreamerPage(String userId, String streamerId, Handler<AsyncResult<JsonObject>> resultHandler);

    @Fluent
    AwaDatabaseService fetchSharedStreamers(String userId, Handler<AsyncResult<List<JsonObject>>> resultHandler);

    @Fluent
    AwaDatabaseService updateStreamKey(String id, String key, Handler<AsyncResult<JsonObject>> resultHandler);

    @Fluent
    AwaDatabaseService getStreamerLiveInfos(String id, String streamerId, Handler<AsyncResult<JsonObject>> resultHandler);

    @Fluent
    AwaDatabaseService setOnAir(String id, String streamKey, Handler<AsyncResult<JsonArray>> resultHandler);

    @Fluent
    AwaDatabaseService setOffAir(String id, Handler<AsyncResult<JsonArray>> resultHandler);

    // NB: It is a good practice that service interfaces provide static methods to create instances of both the actual
    // service implementation and proxy for client code over the event bus.
    static AwaDatabaseService create(MongoClient dbClient, MongoAuth auth, Handler<AsyncResult<AwaDatabaseService>> readyHandler) {
        return new AwaDatabaseServiceImpl(dbClient, auth, readyHandler);
    }

    static AwaDatabaseService createProxy(Vertx vertx, String address) {
        return new AwaDatabaseServiceVertxEBProxy(vertx, address);
    }
}
