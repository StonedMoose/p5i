package com.awa.webapp.utils;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.mongo.MongoAuth;
import io.vertx.ext.auth.mongo.impl.DefaultHashStrategy;
import io.vertx.ext.auth.mongo.impl.MongoUser;

import java.security.KeyException;
import java.util.ArrayList;
import java.util.Set;

/*
    This class, is used to create a user and encode it as a Json with jackson support
*/
public class User{

    private String username;
    private String firstName;
    private String lastName;
    private String mail;
    private String password;
    private String salt;
    private String birthdate;
    private String picture; // we store image as base64 string
    private String streamerId;
    private String activationKey;
    private boolean activated;

    /*
        Constructor of user, generate salt and hashed password
        \param userInfo a JsonObject with all info on user
        \param auth a MongoAuth used for hashing pwd
    */
    public User(JsonObject userInfo, MongoAuth auth) throws KeyException{

        Set<String> keys = userInfo.fieldNames();

        // even if validation is done on client side, we protect us from forged request with wrong params
        if (keys.size() != 6 || !(keys.containsAll(new ArrayList<String>() {
            {add("username"); add("firstName"); add("lastName");
                add("mail"); add("birthdate");add("password"); }}))){
            throw new KeyException("wrong keys");
        }

        for (String item: keys){
            String param = userInfo.getString(item);
            // even if validation is done on client side, we protect us from forged request with empty params
            if (param.isEmpty()){
                throw new KeyException("empty params");
            }
        }

        this.username = userInfo.getString("username");
        this.firstName = userInfo.getString("firstName");
        this.lastName = userInfo.getString("lastName");
        this.mail = userInfo.getString("mail");
        this.birthdate = userInfo.getString("birthdate");
        this.activated = false;

        // add activation key
        DefaultHashStrategy hashStrategy = new DefaultHashStrategy();
        this.activationKey = hashStrategy.generateSalt();

        // generate salt and hash password
        String salt = DefaultHashStrategy.generateSalt();
        MongoUser mongoUser = new MongoUser(userInfo.getString("username"), auth);
        mongoUser.principal().put("salt", salt);
        this.salt = salt;
        this.password = hashStrategy.computeHash(userInfo.getString("password"), mongoUser);

    }

    /*
        Constructor of user, generate hash password. Salt and activation key are given. Usefull for testing
        \param userInfo a JsonObject with all info on user
        \param auth a MongoAuth used for hashing pwd
        \param salt a string used for hashing pwd
        \param activationKey a string used for as a activation key
    */
    public User(JsonObject userInfo, MongoAuth auth, String salt, String activationKey){
        this.username = userInfo.getString("username");
        this.firstName = userInfo.getString("firstName");
        this.lastName = userInfo.getString("lastName");
        this.mail = userInfo.getString("mail");
        this.birthdate = userInfo.getString("birthdate");
        this.activationKey = activationKey;
        this.salt = salt;
        this.activated = false;

        // add activation key
        DefaultHashStrategy hashStrategy = new DefaultHashStrategy();

        // generate salt and hash password
        MongoUser mongoUser = new MongoUser(userInfo.getString("username"), auth);
        mongoUser.principal().put("salt", salt);
        this.password = hashStrategy.computeHash(userInfo.getString("password"), mongoUser);

    }

    /*
        Constructor of user, salt and hash given in userInfo
        \param userInfo a JsonObject with all info on user
    */
    public User(JsonObject userInfo){
        this.username = userInfo.getString("username");
        this.firstName = userInfo.getString("firstName");
        this.lastName = userInfo.getString("lastName");
        this.mail = userInfo.getString("mail");
        this.birthdate = userInfo.getString("birthdate");
        this.salt = userInfo.getString("salt");
        this.password = userInfo.getString("password");
        this.activationKey = userInfo.getString("activationKey");
        this.activated = userInfo.getBoolean("activated");
    }

    // define getters and setters for jackson implementation
    public String getFirstName() { return firstName; }
    public void setFirstName(String firstName) { this.firstName = firstName; }

    public String getLastName() { return lastName; }
    public void setLastName(String lastName) { this.lastName = lastName; }

    public String getMail() { return mail; }
    public void setMail(String mail) { this.mail = mail; }

    public String getPassword() { return password; }
    public void setPassword(String password) { this.password = password; }

    public String getSalt() { return salt; }
    public void setSalt(String salt) { this.salt = salt; }

    public String getBirthdate() { return birthdate; }
    public void setBirthdate( String birthdate) { this.birthdate = birthdate; }

    public String getPicture() { return picture; }
    public void setPicture(String picture) {this.picture = picture; }

    public String getStreamerId() { return streamerId; }
    public void setStreamerId(String streamerId) { this.streamerId = streamerId; }

    public String getActivationKey() { return activationKey; }
    public void setActivationKey(String activationKey) { this.activationKey = activationKey; }

    public String getUsername(){ return username; }
    public void setUsername(String username){ this.username = username; }

    public boolean isActivated() { return activated; }
    public void setActivated(boolean activated) { this.activated = activated; }
}