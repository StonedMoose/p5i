package com.awa.webapp;

import io.vertx.core.Vertx;
import io.vertx.core.Future;
import io.vertx.core.MultiMap;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;
import io.vertx.core.buffer.Buffer;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.junit.*;
import org.junit.runner.RunWith;

@RunWith(VertxUnitRunner.class)
public class MainVerticleTest {

    private static Vertx vertx;
    private static WebClient client;

    @BeforeClass
    public static void setUp(TestContext context) {
        vertx = Vertx.vertx();
        client = WebClient.create(vertx, new WebClientOptions().setFollowRedirects(false));
        vertx.deployVerticle(MainVerticle.class.getName(), context.asyncAssertSuccess());
    }

    @AfterClass
    public static void tearDown(TestContext context) {
        vertx.close(context.asyncAssertSuccess());
    }

    /*
        Test that the main awa server is started and answer on 8080 
    */
    @Test
    public void testHttpsServerStarted(TestContext context) {
        Async async = context.async();
        client.get(3000, "localhost", "/").send( ar -> {
            if (ar.succeeded()) {
                HttpResponse<Buffer> response = ar.result();
                context.assertTrue(response.headers().contains("Content-Type"));
                context.assertEquals("text/html;charset=UTF-8", response.getHeader("Content-Type"));
                async.complete();
            } else {
                async.resolve(Future.failedFuture(ar.cause()));
            }
        });
    }

    /*
        Test that the server started on 3001 redirect to awa.ovh
    */
    @Test 
    public void testHttpRedirectServerStarted(TestContext context){
        Async async = context.async();
        client.get(3001, "localhost", "/").send( ar -> {
            if (ar.succeeded()) {
                HttpResponse<Buffer> response = ar.result();
                context.assertEquals(response.statusCode(), 301);
                context.assertTrue(response.headers().contains("location"));
                context.assertEquals("https://www.awa.ovh", response.getHeader("location"));
                async.complete();
            } else {
                async.resolve(Future.failedFuture(ar.cause()));
            }
        });
    }

    /*
        Test that the server started on 9000 answer
    */
    @Test 
    public void testHttpCheckPublishServerStarted(TestContext context){
        Async async = context.async();

        MultiMap form = MultiMap.caseInsensitiveMultiMap();
        form.set("someParams", "someValue");
        form.set("name", "someId_someNameLongerThan10digits");

        client.post(3002, "localhost", "/on_publish").sendForm(form, ar -> {
            if (ar.succeeded()) {
                HttpResponse<Buffer> response = ar.result();
                context.assertEquals(response.statusCode(), 400);
                async.complete();
            } else {
                async.resolve(Future.failedFuture(ar.cause()));
            }
        });
    }

}