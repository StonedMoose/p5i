package com.awa.webapp.http;

import com.awa.webapp.MainVerticle;
import com.awa.webapp.utils.User;
import io.vertx.core.Future;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.mongo.MongoAuth;
import io.vertx.ext.mongo.BulkOperation;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.security.KeyException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(VertxUnitRunner.class)
public class GetStreamerProfilTest {

    private static Vertx vertx;
    private static WebClient client;
    private static MongoClient mongoClient;
    private static MongoAuth authUsername;
    private static MongoAuth authMail;

    private static JsonObject streamer_data1;
    private static JsonObject streamer_data2;

    @BeforeClass
    public static void setUp(TestContext context) throws InterruptedException, KeyException {
        // create vertx Instance
        vertx = Vertx.vertx();

        // deploy database verticle
        vertx.deployVerticle(MainVerticle.class.getName(), context.asyncAssertSuccess());

        // create mongo client and mong auth provider
        mongoClient = MongoClient.createShared(vertx, new JsonObject().put("connection_string", "mongodb://localhost:27017")
                .put("db_name", "awa"));

        authUsername = MongoAuth.create(mongoClient, new JsonObject());
        authMail = MongoAuth.create(mongoClient, new JsonObject());

        authUsername.setCollectionName("users");
        authUsername.setUsernameField("username");

        authMail.setCollectionName("users");
        authMail.setUsernameField("mail");

        // create WebClient
        client = WebClient.create(vertx, new WebClientOptions().setFollowRedirects(false));

        // insert two users in DB
        JsonObject user_data1 = new JsonObject().put("username", "getStreamerProfil1")
                .put("firstName", "getSteamer")
                .put("lastName", "Profil1")
                .put("mail", "getstreamer@profil1.com")
                .put("birthdate", "1990-07-29")
                .put("password", "azerty");

        JsonObject user_data2 = new JsonObject().put("username", "getStreamerProfil2")
                .put("firstName", "getStreamer")
                .put("lastName", "Profil2")
                .put("mail", "getstreamer@profil2.com")
                .put("birthdate", "1990-07-29")
                .put("password", "azerty");

        User user1 = new User(user_data1, authMail);
        User user2 = new User(user_data2, authMail);

        user1.setActivated(true);
        user1.setStreamerId("getstreamerprofil1");

        user2.setActivated(true);
        user2.setStreamerId("getstreamerprofil2");


        // create latch object to make setUp method to wait for asynchronous insertion before processing tests
        CountDownLatch latch = new CountDownLatch(2);

        List<BulkOperation> operations = Arrays.asList(
                BulkOperation.createInsert(JsonObject.mapFrom(user1)),
                BulkOperation.createInsert(JsonObject.mapFrom(user2))
        );

        mongoClient.bulkWrite("users", operations, res -> {
            if (res.failed()) {
                context.async().resolve(Future.failedFuture(res.cause()));
                return;
            } else {
                latch.countDown();
            }
        });

        // create associated streamer
        streamer_data1 = new JsonObject()
                .put("_id", user1.getStreamerId())
                .put("username",user1.getUsername())
                .put("dispos", new JsonArray().add("Lundi").add("Mardi"))
                .put("genres", new JsonArray().add("Rock").add("Reggae"))
                .put("iban", "FR3030002006630000692730X25")
                .put("bic", "AZEIHKOH")
                .put("doPrivate", true)
                .put("telephone", "0683925519")
                .put("price", "50")
                .put("picture", "some_path");

        streamer_data2 = new JsonObject()
                .put("_id", user2.getStreamerId())
                .put("username", user2.getUsername())
                .put("genres", new JsonArray().add("Rock").add("Reggae"))
                .put("iban", "FR3030002006630000692730X24")
                .put("bic", "AZEIHKOD")
                .put("doPrivate", false)
                .put("telephone", "0618189716")
                .put("dispos", new JsonArray())
                .put("price", "0")
                .put("picture", "some_path");

        List<BulkOperation> streamer_operations = Arrays.asList(
                BulkOperation.createInsert(streamer_data1),
                BulkOperation.createInsert(streamer_data2)
        );

        mongoClient.bulkWrite("streamers", streamer_operations, res -> {
            if (res.failed()) {
                context.async().resolve(Future.failedFuture(res.cause()));
                return;
            } else {
                latch.countDown();
            }
        });

        latch.await(5, TimeUnit.SECONDS);

    }

    @AfterClass
    public static void tearDown(TestContext context) throws InterruptedException{
        CountDownLatch latch = new CountDownLatch(2);

        mongoClient.removeDocuments("users", new JsonObject(), res -> {
            if (res.succeeded()) latch.countDown();
        });
        mongoClient.removeDocuments("streamers", new JsonObject(), res -> {
            if (res.succeeded()) latch.countDown();
        });

        latch.await();
        vertx.close(context.asyncAssertSuccess());
    }


    /*
        Test that user1 get his own streamer informations when requesting a get on /streamer_profil
        1. log as user1
        2. get jwt token
        3. send get request
        4. check that parameters returned are right
     */
    @Test
    public void getStreamerProfilHandler_getStreamer1Infos(TestContext context) {
        Async async = context.async();

        JsonObject post_data = new JsonObject().put("username", "getStreamerProfil1")
                .put("password", "azerty")
                .put("method", "username");

        // 1. log as user1
        client.post(3000, "localhost", "/login").sendJson(post_data, ans -> {

            if (ans.succeeded()){
                HttpResponse<Buffer> response = ans.result();

                if (response.statusCode() != 200){
                    async.resolve(Future.failedFuture("Can't log in"));
                }

                // 2. get jwt token
                String token = response.bodyAsString();

                // 3. send get request
                client.get(3000, "localhost", "/jwt/streamer_profil")
                        .putHeader("Authorization", "Bearer " + token).send(res -> {

                        if (res.succeeded()){
                            // 4. check that parameters returned are right
                            HttpResponse<Buffer> answer = res.result();
                            JsonObject json_answer = answer.bodyAsJsonObject();

                            for (String s: Arrays.asList("_id", "username", "genres", "iban", "bic", "telephone", "dispos",
                                    "price", "doPrivate", "picture")){
                                context.assertEquals(streamer_data1.getValue(s), json_answer.getValue(s));
                            }

                            async.complete();

                        } else {
                            async.resolve(Future.failedFuture(res.cause()));
                        }
                });


            } else {
                async.resolve(Future.failedFuture(ans.cause()));
            }
        });
    }

    /*
        Test that user2 get his own information when requesting a get on /streamer_profil
        1. log as user2
        2. get jwt token
        3. send get request
        4. check that parameters returned are right
     */
    @Test
    public void getStreamerProfilHandler_getStreamer2Infos(TestContext context) {
        Async async = context.async();

        JsonObject post_data = new JsonObject().put("username", "getStreamerProfil2")
                .put("password", "azerty")
                .put("method", "username");

        // 1. log as user1
        client.post(3000, "localhost", "/login").sendJson(post_data, ans -> {

            if (ans.succeeded()){
                HttpResponse<Buffer> response = ans.result();

                if (response.statusCode() != 200){
                    async.resolve(Future.failedFuture("Can't log in"));
                }

                // 2. get jwt token
                String token = response.bodyAsString();

                // 3. send get request
                client.get(3000, "localhost", "/jwt/streamer_profil")
                        .putHeader("Authorization", "Bearer " + token).send(res -> {

                    if (res.succeeded()){
                        // 4. check that parameters returned are right
                        HttpResponse<Buffer> answer = res.result();
                        JsonObject json_answer = answer.bodyAsJsonObject();

                        for (String s: Arrays.asList("_id", "username", "genres", "iban", "bic", "telephone", "doPrivate",
                                "picture")){
                            context.assertEquals(streamer_data2.getValue(s), json_answer.getValue(s));
                        }
                        context.assertEquals("0", json_answer.getString("price"));
                        context.assertEquals(new JsonArray(), json_answer.getJsonArray("dispos"));

                        async.complete();

                    } else {
                        async.resolve(Future.failedFuture(res.cause()));
                    }
                });


            } else {
                async.resolve(Future.failedFuture(ans.cause()));
            }
        });
    }

    @Test
    public void onPublishDone_setStreamerOffAirTest(TestContext context) {

        System.out.println("## before async ##");
        Async async = context.async();

        MultiMap form = MultiMap.caseInsensitiveMultiMap();
        form.set("aParam", "someValue");
        form.set("name", streamer_data1.getString("_id") + "_" + streamer_data1.getString("streamKey"));
        // 1. send a post request emulating nginx post on publish
        client.post(3002, "localhost", "/on_publish_done").sendForm(form, ar -> {
            if (ar.succeeded()) {
                HttpResponse<Buffer> response = ar.result();
                // 2. check status code
                context.assertEquals(200, response.statusCode());
                // 3. check that user is onAir in DB
                JsonObject query = new JsonObject().put("_id", streamer_data1.getString("_id"));
                mongoClient.findOne("streamers", query, new JsonObject(), res -> {

                    if (res.succeeded()){
                        context.assertFalse(res.result().getBoolean("onAir"));
                        async.complete();
                    } else {
                        async.resolve(Future.failedFuture(ar.cause()));
                    }
                });

            } else {
                async.resolve(Future.failedFuture(ar.cause()));
            }
        });
    }


}