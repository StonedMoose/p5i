package com.awa.webapp.http;

import com.awa.webapp.MainVerticle;
import com.awa.webapp.utils.User;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.mongo.MongoAuth;
import io.vertx.ext.auth.mongo.impl.DefaultHashStrategy;
import io.vertx.ext.mongo.BulkOperation;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.ext.web.client.HttpRequest;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.security.KeyException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(VertxUnitRunner.class)
public class GetUserProfilTest {

    private static Vertx vertx;
    private static WebClient client;
    private static MongoClient mongoClient;
    private static MongoAuth authUsername;
    private static MongoAuth authMail;

    private static JsonObject user_data1;
    private static JsonObject user_data2;
    private static User user1;
    private static User user2;

    @BeforeClass
    public static void setUp(TestContext context) throws InterruptedException, KeyException {
        // create vertx Instance
        vertx = Vertx.vertx();

        // deploy database verticle
        vertx.deployVerticle(MainVerticle.class.getName(), context.asyncAssertSuccess());

        // create mongo client and mong auth provider
        mongoClient = MongoClient.createShared(vertx, new JsonObject().put("connection_string", "mongodb://localhost:27017")
                .put("db_name", "awa"));

        authUsername = MongoAuth.create(mongoClient, new JsonObject());
        authMail = MongoAuth.create(mongoClient, new JsonObject());

        authUsername.setCollectionName("users");
        authUsername.setUsernameField("username");

        authMail.setCollectionName("users");
        authMail.setUsernameField("mail");

        // create WebClient
        client = WebClient.create(vertx, new WebClientOptions().setFollowRedirects(false));

        // insert two users in DB
        user_data1 = new JsonObject().put("username", "getUserProfil1")
                .put("firstName", "getUser")
                .put("lastName", "Profil1")
                .put("mail", "getuser@profil1.com")
                .put("birthdate", "1990-07-29")
                .put("password", "azerty");

        user_data2 = new JsonObject().put("username", "getUserProfil2")
                .put("firstName", "getUser")
                .put("lastName", "Profil2")
                .put("mail", "getuser@profil2.com")
                .put("birthdate", "1990-07-29")
                .put("password", "azerty");

        user1 = new User(user_data1, authMail);
        user2 = new User(user_data2, authMail);

        user1.setActivated(true);
        user2.setActivated(true);

        user1.setPicture("/dist/static/some_picture_path.png");

        // create latch object to make setUp method to wait for asynchronous insertion before processing tests
        CountDownLatch latch = new CountDownLatch(1);

        List<BulkOperation> operations = Arrays.asList(BulkOperation.createInsert(JsonObject.mapFrom(user1)),
                BulkOperation.createInsert(JsonObject.mapFrom(user2)));

        mongoClient.bulkWrite("users", operations, res -> {
            if (res.failed()) {
                context.async().resolve(Future.failedFuture(res.cause()));
                return;
            } else {
                latch.countDown();
            }
        });

        latch.await(5, TimeUnit.SECONDS);
    }

    @AfterClass
    public static void tearDown(TestContext context) throws InterruptedException{
        CountDownLatch latch = new CountDownLatch(2);

        mongoClient.removeDocuments("users", new JsonObject(), res -> {
            if (res.succeeded()) latch.countDown();
        });
        mongoClient.removeDocuments("streamers", new JsonObject(), res -> {
            if (res.succeeded()) latch.countDown();
        });

        latch.await();
        vertx.close(context.asyncAssertSuccess());
    }

    /*
        Test that user1 get his own information when requesting a get on /user_profil
        1. log as user1
        2. get jwt token
        3. send get request
        4. check that parameters returned are right
     */
    @Test
    public void getUserProfilHandler_getUser1Infos(TestContext context) {
        Async async = context.async();

        JsonObject post_data = new JsonObject().put("username", "getUserProfil1")
                .put("password", "azerty")
                .put("method", "username");

        // 1. log as user1
        client.post(3000, "localhost", "/login").sendJson(post_data, ans -> {

            if (ans.succeeded()){
                HttpResponse<Buffer> response = ans.result();

                if (response.statusCode() != 200){
                    async.resolve(Future.failedFuture("Can't log in"));
                }

                // 2. get jwt token
                String token = response.bodyAsString();

                // 3. send get request
                client.get(3000, "localhost", "/jwt/user_profil")
                        .putHeader("Authorization", "Bearer " + token).send(res -> {

                        if (res.succeeded()){
                            // 4. check that parameters returned are right
                            HttpResponse<Buffer> answer = res.result();
                            JsonObject json_answer = answer.bodyAsJsonObject();

                            context.assertEquals(user1.getUsername(), json_answer.getString("username"));
                            context.assertEquals(user1.getMail(), json_answer.getString("mail"));
                            context.assertEquals(user1.getPicture(), json_answer.getString("picture"));

                            async.complete();

                        } else {
                            async.resolve(Future.failedFuture(res.cause()));
                        }
                });


            } else {
                async.resolve(Future.failedFuture(ans.cause()));
            }
        });
    }

    /*
        Test that user2 get his own information when requesting a get on /user_profil
        1. log as user2
        2. get jwt token
        3. send get request
        4. check that parameters returned are right
     */
    @Test
    public void getUserProfilHandler_getUser2Infos(TestContext context) {
        Async async = context.async();

        JsonObject post_data = new JsonObject().put("username", "getUserProfil2")
                .put("password", "azerty")
                .put("method", "username");

        // 1. log as user1
        client.post(3000, "localhost", "/login").sendJson(post_data, ans -> {

            if (ans.succeeded()){
                HttpResponse<Buffer> response = ans.result();

                if (response.statusCode() != 200){
                    async.resolve(Future.failedFuture("Can't log in"));
                }

                // 2. get jwt token
                String token = response.bodyAsString();

                // 3. send get request
                client.get(3000, "localhost", "/jwt/user_profil")
                        .putHeader("Authorization", "Bearer " + token).send(res -> {

                    if (res.succeeded()){
                        // 4. check that parameters returned are right
                        HttpResponse<Buffer> answer = res.result();
                        JsonObject json_answer = answer.bodyAsJsonObject();

                        context.assertEquals(user2.getUsername(), json_answer.getString("username"));
                        context.assertEquals(user2.getMail(), json_answer.getString("mail"));
                        context.assertEquals(user2.getPicture(), json_answer.getString("picture"));

                        async.complete();

                    } else {
                        async.resolve(Future.failedFuture(res.cause()));
                    }
                });


            } else {
                async.resolve(Future.failedFuture(ans.cause()));
            }
        });
    }

}