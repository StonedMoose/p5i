package com.awa.webapp.http;

import com.awa.webapp.MainVerticle;
import com.awa.webapp.utils.User;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.mongo.MongoAuth;
import io.vertx.ext.mongo.BulkOperation;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.security.KeyException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(VertxUnitRunner.class)
public class GetStreamKeyTest {

    private static Vertx vertx;
    private static WebClient client;
    private static MongoClient mongoClient;
    private static MongoAuth authUsername;
    private static MongoAuth authMail;

    private static JsonObject streamer_data1, streamer_data2;

    @BeforeClass
    public static void setUp(TestContext context) throws InterruptedException, KeyException {
        // create vertx Instance
        vertx = Vertx.vertx();

        // deploy database verticle
        vertx.deployVerticle(MainVerticle.class.getName(), context.asyncAssertSuccess());

        // create mongo client and mong auth provider
        mongoClient = MongoClient.createShared(vertx, new JsonObject().put("connection_string", "mongodb://localhost:27017")
                .put("db_name", "awa"));

        authUsername = MongoAuth.create(mongoClient, new JsonObject());
        authMail = MongoAuth.create(mongoClient, new JsonObject());

        authUsername.setCollectionName("users");
        authUsername.setUsernameField("username");

        authMail.setCollectionName("users");
        authMail.setUsernameField("mail");

        // create WebClient
        client = WebClient.create(vertx, new WebClientOptions().setFollowRedirects(false));

        // create latch object to make setUp method to wait for asynchronous insertion before processing tests
        CountDownLatch latch = new CountDownLatch(2);

        // insert a user to log with
        JsonObject user_data1 = new JsonObject().put("username", "user1")
                .put("firstName", "toto")
                .put("lastName", "tata")
                .put("mail", "user1@awa.com")
                .put("birthdate", "1990-07-29")
                .put("password", "azerty");

        JsonObject user_data2 = new JsonObject().put("username", "user2")
                .put("firstName", "toto")
                .put("lastName", "tata")
                .put("mail", "user2@awa.com")
                .put("birthdate", "1990-07-29")
                .put("password", "azerty");

        User user1 = new User(user_data1, authMail);
        user1.setActivated(true);
        user1.setStreamerId("streamer1id");

        User user2 = new User(user_data2, authMail);
        user2.setActivated(true);

        List<BulkOperation> user_operations = Arrays.asList(
                BulkOperation.createInsert(JsonObject.mapFrom(user1)),
                BulkOperation.createInsert(JsonObject.mapFrom(user2))
        );

        mongoClient.bulkWrite("users", user_operations, res -> {
            if (res.failed()) {
                context.async().resolve(Future.failedFuture(res.cause()));
                return;
            } else {
                latch.countDown();
            }
        });
        // insert streamers
        streamer_data1 = new JsonObject()
                .put("_id", "streamer1id")
                .put("username", "user1")
                .put("dispos", new JsonArray().add("Lundi").add("Mardi"))
                .put("genres", new JsonArray().add("Rock").add("Reggae"))
                .put("iban", "FR3030002006633200692730X25")
                .put("bic", "AZEIHKHH")
                .put("doPrivate", true)
                .put("telephone", "0683925521")
                .put("price", "50")
                .put("picture", "/dist/static/streamer1id.png")
                .put("followers", new JsonArray().add("userXid").add("user1id").add("userYid"))
                .put("bio", "une bio")
                .put("streamKey", "TLZSHLVDJJIHSZQZRZQMRUHOLHKSQHUOIPESAYEWFQHIJKTAUEDBUYYYHJZMPHZE")
                .put("onAir", false);

        List<BulkOperation> streamer_operations = Arrays.asList(
                BulkOperation.createInsert(streamer_data1)
        );

        mongoClient.bulkWrite("streamers", streamer_operations, res -> {
            if (res.failed()) {
                context.async().resolve(Future.failedFuture(res.cause()));
                return;
            } else {
                latch.countDown();
            }
        });

        latch.await(5, TimeUnit.SECONDS);

    }

    @AfterClass
    public static void tearDown(TestContext context) throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(2);

        mongoClient.removeDocuments("users", new JsonObject(), res -> {
            if (res.succeeded()) latch.countDown();
        });
        mongoClient.removeDocuments("streamers", new JsonObject(), res -> {
            if (res.succeeded()) latch.countDown();
        });

        latch.await();
        vertx.close(context.asyncAssertSuccess());
    }


    /*
        Test that a get on /stream_key renew key and expiration date in db and return a json with url and streamKey
        1. log as user1
        2. get jwt token
        3. send get request
        4. check status code
        5. check that changed has been made in db (new streamKey and streamKeyExpiration)
     */
    @Test
    public void getStreamKey_authorizedUser(TestContext context) {
        Async async = context.async();

        JsonObject post_data = new JsonObject().put("username", "user1")
                .put("password", "azerty")
                .put("method", "username");

        //  1. log as user1
        client.post(3000, "localhost", "/login").sendJson(post_data, ans -> {

            if (ans.succeeded()) {
                HttpResponse<Buffer> response = ans.result();
                if (response.statusCode() != 200) {
                    async.resolve(Future.failedFuture("Can't log in"));
                }

                // 2. get jwt token
                String token = response.bodyAsString();

                // 3. send get request
                client.get(3000, "localhost", "/jwt/stream_key")
                        .putHeader("Authorization", "Bearer " + token).send(res -> {

                    if (res.succeeded()) {
                        // 4. check value from the json returned and status code
                        HttpResponse<Buffer> answer = res.result();

                        context.assertEquals(200, answer.statusCode());
                        JsonObject json_answer = answer.bodyAsJsonObject();

                        context.assertEquals("rtmp://awa.ovh/live/", json_answer.getString("url"));
                        context.assertNotEquals(streamer_data1.getString("_id") + "_" + streamer_data1.getString("key"),
                                                json_answer.getString("key"));

                        mongoClient.findOne("streamers",
                                new JsonObject().put("_id", streamer_data1.getString("_id")),
                                new JsonObject(), reply -> {

                            if (res.succeeded()){
                                context.assertEquals(json_answer.getString("key"),
                                  reply.result().getString("_id") + "_" + reply.result().getString("streamKey"));
                                async.complete();
                            } else {
                                async.resolve(Future.failedFuture(res.cause()));
                            }
                        });
                    } else {
                        async.resolve(Future.failedFuture(res.cause()));
                    }
                });
            } else {
                async.resolve(Future.failedFuture(ans.cause()));
            }
        });
    }

    /*
    Test that a get on /stream_key for a non streamer user cause a HTTP/404
    1. log as user1
    2. get jwt token
    3. send get request
    4. check status code
 */
    @Test
    public void getStreamKey_unauthorizedUser(TestContext context) {
        Async async = context.async();

        JsonObject post_data = new JsonObject().put("username", "user2")
                .put("password", "azerty")
                .put("method", "username");

        //  1. log as user1
        client.post(3000, "localhost", "/login").sendJson(post_data, ans -> {

            if (ans.succeeded()) {
                HttpResponse<Buffer> response = ans.result();
                if (response.statusCode() != 200) {
                    async.resolve(Future.failedFuture("Can't log in"));
                }

                // 2. get jwt token
                String token = response.bodyAsString();

                // 3. send get request
                client.get(3000, "localhost", "/jwt/stream_key")
                        .putHeader("Authorization", "Bearer " + token).send(res -> {

                    if (res.succeeded()) {
                        // 4. check value from the json returned and status code
                        HttpResponse<Buffer> answer = res.result();
                        context.assertEquals(404, answer.statusCode());
                        async.complete();
                    } else {
                        async.resolve(Future.failedFuture(res.cause()));
                    }
                });
            } else {
                async.resolve(Future.failedFuture(ans.cause()));
            }
        });
    }


}