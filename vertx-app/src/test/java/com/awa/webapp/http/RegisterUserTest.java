package com.awa.webapp.http;
import com.awa.webapp.MainVerticle;

import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.mongo.MongoAuth;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import com.awa.webapp.utils.User;

import java.util.concurrent.CountDownLatch;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(VertxUnitRunner.class)
public class RegisterUserTest {

    private static Vertx vertx;
    private static WebClient client;
    private static MongoClient mongoClient;
    private static MongoAuth authUsername;
    private static MongoAuth authMail;

    @BeforeClass
    public static void setUp(TestContext context) {
        // create vertx Instance
        vertx = Vertx.vertx();

        // deploy database verticle
        vertx.deployVerticle(MainVerticle.class.getName(), context.asyncAssertSuccess());

        // create mongo client and mong auth provider
        mongoClient = MongoClient.createShared(vertx, new JsonObject().put("connection_string", "mongodb://localhost:27017")
                .put("db_name", "awa"));

        authUsername = MongoAuth.create(mongoClient, new JsonObject());
        authMail = MongoAuth.create(mongoClient, new JsonObject());

        authUsername.setCollectionName("users");
        authUsername.setUsernameField("username");

        authMail.setCollectionName("users");
        authMail.setUsernameField("mail");

        // create WebClient
        client = WebClient.create(vertx, new WebClientOptions().setFollowRedirects(false));
    }

    @AfterClass
    public static void tearDown(TestContext context) throws InterruptedException{
        CountDownLatch latch = new CountDownLatch(2);

        mongoClient.removeDocuments("users", new JsonObject(), res -> {
            if (res.succeeded()) latch.countDown();
        });
        mongoClient.removeDocuments("streamers", new JsonObject(), res -> {
            if (res.succeeded()) latch.countDown();
        });

        latch.await();
        vertx.close(context.asyncAssertSuccess());
    }

    /*
        Test that a user can register
        1. post user info to register
        2. check status code
        3. check that user has been inserted in db
     */
    @Test
    public void registerHandlerTest_GoodRequest(TestContext context) {
        Async async = context.async();

        JsonObject user_data = new JsonObject().put("username", "testUser")
                .put("firstName", "test")
                .put("lastName", "user")
                .put("mail", "test@user.fr")
                .put("birthdate", "1990-07-29")
                .put("password", "azerty");


        client.post(3000, "localhost", "/register").sendJson(user_data, ar -> {
            if (ar.succeeded()) {
                HttpResponse<Buffer> response = ar.result();
                context.assertEquals(200, response.statusCode());

                mongoClient.findOne("users", new JsonObject().put("username", user_data.getString("username")),
                        new JsonObject(), res -> {
                            if (res.succeeded()) {
                                // given the same salt and activationKey as the one used in register handler and stored in db, create a user
                                User user = new User(user_data, authMail, res.result().getString("salt"), res.result().getString("activationKey"));

                                context.assertEquals(JsonObject.mapFrom(user), JsonObject.mapFrom(new User(res.result())));
                                async.complete();
                            } else {
                                async.resolve(Future.failedFuture(ar.cause()));
                            }
                        });

            } else {
                async.resolve(Future.failedFuture(ar.cause()));
            }
        });
    }

    /*
        Test that user can't register with existing username
        1. post userInfo to register
        2. check response status and msg
     */
    @Test
    public void registerHandlerTest_GoodRequestExistingUsername(TestContext context) {
        Async async = context.async();

        JsonObject data = new JsonObject().put("username", "testUser")
                .put("firstName", "test")
                .put("lastName", "user")
                .put("mail", "test2@user.fr")
                .put("birthdate", "1990-07-29")
                .put("password", "azerty");


        client.post(3000, "localhost", "/register").sendJson(data, ar -> {
            if (ar.succeeded()) {
                HttpResponse<Buffer> response = ar.result();
                context.assertEquals(400, response.statusCode());
                context.assertEquals("Nom d'utilisateur déjà utilisé", response.bodyAsString());

                async.complete();
            } else {
                async.resolve(Future.failedFuture(ar.cause()));
            }
        });
    }

    /*
        Test that user can't register with existing e-mail
        1. post userInfo to register
        2. check response status and msg
     */
    @Test
    public void registerHandlerTest_GoodRequestExistingMail(TestContext context) {
        Async async = context.async();

        JsonObject data = new JsonObject().put("username", "testUser2")
                .put("firstName", "test")
                .put("lastName", "user")
                .put("mail", "test@user.fr")
                .put("birthdate", "1990-07-29")
                .put("password", "azerty");


        client.post(3000, "localhost", "/register").sendJson(data, ar -> {
            if (ar.succeeded()) {
                HttpResponse<Buffer> response = ar.result();
                context.assertEquals(400, response.statusCode());
                context.assertEquals("Adresse mail déjà utilisée", response.bodyAsString());

                async.complete();
            } else {
                async.resolve(Future.failedFuture(ar.cause()));
            }
        });
    }

    /*
        Test that user can't register with request which as missing parameters
        1. post userInfo to register
        2. check response status
     */
    @Test
    public void registerHandlerTest_WrongRequestMissingParameter(TestContext context) {
        Async async = context.async();

        JsonObject data = new JsonObject().put("username", "testUser2")
                .put("firstName", "test")
                .put("mail", "test@user2.fr")
                .put("birthdate", "1990-12-12")
                .put("password", "azerty");


        client.post(3000, "localhost", "/register").sendJson(data, ar -> {
            if (ar.succeeded()) {
                HttpResponse<Buffer> response = ar.result();
                context.assertEquals(400, response.statusCode());

                async.complete();
            } else {
                async.resolve(Future.failedFuture(ar.cause()));
            }
        });
    }

    /*
        Test that user can't register with request which as empty params
        1. post userInfo to register
        2. check response status
     */
    @Test
    public void registerHandlerTest_WrongRequestEmptyParameter(TestContext context) {
        Async async = context.async();

        JsonObject data = new JsonObject().put("username", "testUser2")
                .put("firstName", "test")
                .put("lastName", "")
                .put("mail", "test@user2.fr")
                .put("birthdate", "805759200000")
                .put("password", "azerty");


        client.post(3000, "localhost", "/register").sendJson(data, ar -> {
            if (ar.succeeded()) {
                HttpResponse<Buffer> response = ar.result();
                context.assertEquals(400, response.statusCode());

                async.complete();
            } else {
                async.resolve(Future.failedFuture(ar.cause()));
            }
        });
    }

    /*
        Test that a user can't register with unreachable mail address
        1. post user info to register
        2. check status code
        3. check that user hasn't been inserted in db
     */
    @Test
    public void registerHandlerTest_WrongRequestMailUnreachable(TestContext context) {
        Async async = context.async();

        JsonObject user_data = new JsonObject().put("username", "wrongMail")
                .put("firstName", "test")
                .put("lastName", "user")
                .put("mail", "test@tamerela***.ouzbek")
                .put("birthdate", "1990-07-29")
                .put("password", "azerty");

        client.post(3000, "localhost", "/register").sendJson(user_data, ar -> {
            if (ar.succeeded()) {
                HttpResponse<Buffer> response = ar.result();
                context.assertEquals(400, response.statusCode());

                mongoClient.find("users", new JsonObject().put("username", user_data.getString("username")), res -> {
                    if (res.succeeded()) {
                        context.assertTrue(res.result().isEmpty());
                        async.complete();
                    } else {
                        async.resolve(Future.failedFuture(ar.cause()));
                    }
                });

            } else {
                async.resolve(Future.failedFuture(ar.cause()));
            }
        });
    }

    /*
        Test that a user can't register with username which doesn't respect username regex /[a-zA-Z0-9_-]+/
        1. post user info to register
        2. check status code
        3. check that user hasn't been inserted in db
     */
    @Test
    public void registerHandlerTest_WrongUsername(TestContext context) {
        Async async = context.async();

        JsonObject user_data = new JsonObject().put("username", "wrong username")
                .put("firstName", "test")
                .put("lastName", "user")
                .put("mail", "test@user.com")
                .put("birthdate", "1990-07-29")
                .put("password", "azerty");


        client.post(3000, "localhost", "/register").sendJson(user_data, ar -> {
            if (ar.succeeded()) {
                HttpResponse<Buffer> response = ar.result();
                context.assertEquals(400, response.statusCode());

                mongoClient.find("users", new JsonObject().put("username", user_data.getString("username")), res -> {
                    if (res.succeeded()) {
                        context.assertTrue(res.result().isEmpty());
                        async.complete();
                    } else {
                        async.resolve(Future.failedFuture(ar.cause()));
                    }
                });

            } else {
                async.resolve(Future.failedFuture(ar.cause()));
            }
        });
    }

    /*
        Test that a user can't register with birthdate not matching regex /[0-9]{4}-[0-9]{2}-[0-9]{2}/
        1. post user info to register
        2. check status code
        3. check that user hasn't been inserted in db
     */
    @Test
    public void registerHandlerTest_WrongBirthdate(TestContext context) {
        Async async = context.async();

        JsonObject user_data = new JsonObject().put("username", "wrongBirthdate")
                .put("firstName", "test")
                .put("lastName", "user")
                .put("mail", "test@user.com")
                .put("birthdate", "199aa-07-29")
                .put("password", "azerty");

        client.post(3000, "localhost", "/register").sendJson(user_data, ar -> {
            if (ar.succeeded()) {
                HttpResponse<Buffer> response = ar.result();
                context.assertEquals(400, response.statusCode());

                mongoClient.find("users", new JsonObject().put("username", user_data.getString("username")), res -> {
                    if (res.succeeded()) {
                        context.assertTrue(res.result().isEmpty());
                        async.complete();
                    } else {
                        async.resolve(Future.failedFuture(ar.cause()));
                    }
                });

            } else {
                async.resolve(Future.failedFuture(ar.cause()));
            }
        });
    }
}
