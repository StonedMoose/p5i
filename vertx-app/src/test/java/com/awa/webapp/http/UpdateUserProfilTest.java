package com.awa.webapp.http;

import com.awa.webapp.MainVerticle;
import com.awa.webapp.utils.User;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.file.FileSystem;
import io.vertx.core.impl.FailedFuture;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.mongo.MongoAuth;
import io.vertx.ext.mongo.BulkOperation;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.security.KeyException;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(VertxUnitRunner.class)
public class UpdateUserProfilTest {

    private static Vertx vertx;
    private static FileSystem fs;
    private static WebClient client;
    private static MongoClient mongoClient;
    private static MongoAuth authUsername;
    private static MongoAuth authMail;

    private static User user1;
    private static User user2;
    private static User user3;
    private static User user4;

    private static String img_base64 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAQAAAAECAIAAAAmkwkpAAAACXBIWXMAAAsTAA" +
            "ALEwEAmpwYAAAAB3RJTUUH4gMEDhgQyqhYmQAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUHAAAALUlEQVQI103JoREAM" +
            "AwDsW8vBgYG2SX7zxZQUlGdmQFsJ7l8yvYbSZUEkNTdCzJaAZgMY+zMAAAAAElFTkSuQmCC";

    @BeforeClass
    public static void setUp(TestContext context) throws InterruptedException, KeyException {
        // create vertx Instance
        vertx = Vertx.vertx();
        fs = vertx.fileSystem();

        // deploy database verticle
        vertx.deployVerticle(MainVerticle.class.getName(), context.asyncAssertSuccess());
        // create mongo client and mong auth provider
        mongoClient = MongoClient.createShared(vertx, new JsonObject().put("connection_string", "mongodb://localhost:27017")
                .put("db_name", "awa"));

        authUsername = MongoAuth.create(mongoClient, new JsonObject());
        authMail = MongoAuth.create(mongoClient, new JsonObject());

        authUsername.setCollectionName("users");
        authUsername.setUsernameField("username");

        authMail.setCollectionName("users");
        authMail.setUsernameField("mail");

        // create WebClient
        client = WebClient.create(vertx, new WebClientOptions().setFollowRedirects(false));

        // insert two users in DB
        JsonObject user_data1 = new JsonObject().put("username", "updateUserProfil1")
                .put("firstName", "User")
                .put("lastName", "Profil1")
                .put("mail", "user@profil1.com")
                .put("birthdate", "1990-07-29")
                .put("password", "azerty");

        JsonObject user_data2 = new JsonObject().put("username", "updateUserProfil2")
                .put("firstName", "User")
                .put("lastName", "Profil2")
                .put("mail", "user@profil2.com")
                .put("birthdate", "1990-07-29")
                .put("password", "azerty");

        JsonObject user_data3 = new JsonObject().put("username", "updateUserProfil3")
                .put("firstName", "User")
                .put("lastName", "Profil3")
                .put("mail", "user@profil3.com")
                .put("birthdate", "1990-07-29")
                .put("password", "azerty");

        JsonObject user_data4 = new JsonObject().put("username", "updateUserProfil4")
                .put("firstName", "User")
                .put("lastName", "Profil3")
                .put("mail", "user@profil4.com")
                .put("birthdate", "1990-07-29")
                .put("password", "azerty");

        user1 = new User(user_data1, authMail);
        user2 = new User(user_data2, authMail);
        user3 = new User(user_data3, authMail);
        user4 = new User(user_data4, authMail);

        user1.setPicture("/dist/static/some_picture_path.png");
        for (User u: Arrays.asList(user1, user2, user3, user4)){
            u.setActivated(true);
        }

        // create latch object to make setUp method to wait for asynchronous insertion before processing tests
        CountDownLatch latch = new CountDownLatch(1);

        List<BulkOperation> operations = Arrays.asList(BulkOperation.createInsert(JsonObject.mapFrom(user1)),
                BulkOperation.createInsert(JsonObject.mapFrom(user2)),
                BulkOperation.createInsert(JsonObject.mapFrom(user3)),
                BulkOperation.createInsert(JsonObject.mapFrom(user4)));

        mongoClient.bulkWrite("users", operations, res -> {
            if (res.failed()) {
                context.asyncAssertFailure();
            } else {
                latch.countDown();
            }
        });

        latch.await(5, TimeUnit.SECONDS);
    }

    @AfterClass
    public static void tearDown(TestContext context) throws InterruptedException{
        CountDownLatch latch = new CountDownLatch(2);

        mongoClient.removeDocuments("users", new JsonObject(), res -> {
            if (res.succeeded()) latch.countDown();
        });
        mongoClient.removeDocuments("streamers", new JsonObject(), res -> {
            if (res.succeeded()) latch.countDown();
        });

        latch.await();
        vertx.close(context.asyncAssertSuccess());
    }

    /*
        Test that user1 change his own information when requesting a post on /user_profil with three params username, mail,
        picture
        1. log as user1
        2. get jwt token
        3. send post request
        4. check request response
        5. check that user informations have changed in db
        6. check that a picture has been written on fs
     */
    @Test
    public void updateUserProfilHandler_fullUpdate(TestContext context) {
        Async async = context.async();

        JsonObject post_data = new JsonObject().put("username", "updateUserProfil1")
                .put("password", "azerty")
                .put("method", "username");

        // 1. log as user1
        client.post(3000, "localhost", "/login").sendJson(post_data, ans -> {

            if (ans.succeeded()){

                HttpResponse<Buffer> response = ans.result();

                if (response.statusCode() != 200){
                    async.resolve(Future.failedFuture("Can't log in"));
                }

                // 2. get jwt token
                String token = response.bodyAsString();
                JsonObject user_infos = new JsonObject(new String(Base64.getDecoder().decode(token.split("\\.")[1])));

                // update query
                JsonObject update_query = new JsonObject().put("mail", "newEmail@test.com")
                                                          .put("picture", img_base64);

                // 3. send post request
                client.post(3000, "localhost", "/jwt/user_profil")
                        .putHeader("Authorization", "Bearer " + token).sendJson(update_query, res -> {

                    if (res.succeeded()){
                        // 4. check request response
                        HttpResponse<Buffer> answer = res.result();
                        JsonObject json_answer = answer.bodyAsJsonObject();
                        String picture_path = "/dist/static/" + user_infos.getString("_id") + ".png";

                        context.assertEquals(200, answer.statusCode());
                        context.assertEquals(update_query.getString("mail"), json_answer.getString("mail"));
                        context.assertEquals(picture_path, json_answer.getString("picture"));

                        mongoClient.findOne("users", new JsonObject().put("_id", user_infos.getString("_id")), new JsonObject(),
                                reply -> {
                            if (reply.succeeded()){
                                // 5. check that user informations have changed in db
                                context.assertEquals(update_query.getString("mail"), reply.result().getString("mail"));
                                context.assertEquals(picture_path, reply.result().getString("picture"));
                                // 6. check that a picture has been written on fs
                                context.assertTrue(fs.existsBlocking("src/frontend/" + picture_path));

                                // delete file to clean up
                                fs.deleteBlocking("src/frontend/" + picture_path);
                                async.complete();

                            } else {
                                async.resolve(Future.failedFuture(reply.cause()));
                            }
                        });

                    } else {
                        async.resolve(Future.failedFuture(res.cause()));
                    }
                });

            } else {
                async.resolve(Future.failedFuture(ans.cause()));
            }
        });
    }

    /*
        Test that user3 change his own informations when requesting a post on /user_profil with one params mail
        1. log as user3
        2. get jwt token
        3. send post request
        4. check request response
        5. check that only mail has changed in db
     */
    @Test
    public void updateUserProfilHandler_mailUpdate(TestContext context) {
        Async async = context.async();

        JsonObject post_data = new JsonObject().put("username", "updateUserProfil3")
                .put("password", "azerty")
                .put("method", "username");

        // 1. log as user2
        client.post(3000, "localhost", "/login").sendJson(post_data, ans -> {

            if (ans.succeeded()){
                HttpResponse<Buffer> response = ans.result();

                if (response.statusCode() != 200){
                    async.resolve(Future.failedFuture("Can't log in"));
                }

                // 2. get jwt token
                String token = response.bodyAsString();
                JsonObject user_infos = new JsonObject(new String(Base64.getDecoder().decode(token.split("\\.")[1])));

                // update query
                JsonObject update_query = new JsonObject().put("mail", "updatedMail@test.com");
                // 3. send post request
                client.post(3000, "localhost", "/jwt/user_profil")
                        .putHeader("Authorization", "Bearer " + token).sendJson(update_query, res -> {

                    if (res.succeeded()){
                        // 4. check request response
                        HttpResponse<Buffer> answer = res.result();
                        JsonObject json_answer = answer.bodyAsJsonObject();

                        context.assertEquals(200, answer.statusCode());
                        context.assertEquals(update_query.getString("mail"), json_answer.getString("mail"));

                        mongoClient.findOne("users", new JsonObject().put("_id", user_infos.getString("_id")), new JsonObject(), reply -> {
                            if (reply.succeeded()){
                                // 5. check that only mail has changed in db
                                context.assertEquals(user3.getUsername(), reply.result().getString("username"));
                                context.assertEquals(update_query.getString("mail"), reply.result().getString("mail"));
                                context.assertEquals(user3.getPicture(), reply.result().getString("picture"));

                                async.complete();

                            } else {
                                async.resolve(Future.failedFuture(reply.cause()));
                            }
                        });

                    } else {
                        async.resolve(Future.failedFuture(res.cause()));
                    }
                });

            } else {
                async.resolve(Future.failedFuture(ans.cause()));
            }
        });
    }

    /*
        Test that user4 change his own informations when requesting a post on /user_profil with one params picture
        1. log as user4
        2. get jwt token
        3. send post request
        4. check request response
        5. check that only picture has changed in db
        6. check that a picture have been written on fs
     */
    @Test
    public void updateUserProfilHandler_pictureUpdate(TestContext context) {
        Async async = context.async();

        JsonObject post_data = new JsonObject().put("username", "updateUserProfil4")
                .put("password", "azerty")
                .put("method", "username");

        // 1. log as user2
        client.post(3000, "localhost", "/login").sendJson(post_data, ans -> {

            if (ans.succeeded()){
                HttpResponse<Buffer> response = ans.result();

                if (response.statusCode() != 200){
                    async.resolve(Future.failedFuture("Can't log in"));
                }

                // 2. get jwt token
                String token = response.bodyAsString();
                JsonObject user_infos = new JsonObject(new String(Base64.getDecoder().decode(token.split("\\.")[1])));

                // update query
                JsonObject update_query = new JsonObject().put("picture", img_base64);
                // 3. send post request
                client.post(3000, "localhost", "/jwt/user_profil")
                        .putHeader("Authorization", "Bearer " + token).sendJson(update_query, res -> {

                    if (res.succeeded()){
                        // 4. check request response
                        HttpResponse<Buffer> answer = res.result();
                        JsonObject json_answer = answer.bodyAsJsonObject();
                        String picture_path = "/dist/static/" + user_infos.getString("_id") + ".png";

                        context.assertEquals(200, answer.statusCode());
                        context.assertEquals(picture_path, json_answer.getString("picture"));

                        mongoClient.findOne("users", new JsonObject().put("_id", user_infos.getString("_id")), new JsonObject(), reply -> {
                            if (reply.succeeded()){
                                // 5. check that only picture has changed in db
                                context.assertEquals(user4.getUsername(), reply.result().getString("username"));
                                context.assertEquals(user4.getMail(), reply.result().getString("mail"));
                                context.assertEquals(picture_path, reply.result().getString("picture"));

                                // 6. check that a picture have been written on fs
                                context.assertTrue(fs.existsBlocking("src/frontend/" + picture_path));

                                // delete file to clean up
                                fs.deleteBlocking("src/frontend/" + picture_path);
                                async.complete();

                            } else {
                                async.resolve(Future.failedFuture(reply.cause()));
                            }
                        });

                    } else {
                        async.resolve(Future.failedFuture(res.cause()));
                    }
                });

            } else {
                async.resolve(Future.failedFuture(ans.cause()));
            }
        });
    }


    /*
        Test that user4 can't change his profil picture when posting image bigger than 4.5*1024*1024 characters (base64 encoded)
        1. log as user4
        2. get jwt token
        3. send post request
        4. check request response (msg and status code)
     */
    @Test
    public void updateUserProfilHandler_wrongPictureUpdate(TestContext context) {
        Async async = context.async();

        JsonObject post_data = new JsonObject().put("username", "updateUserProfil4")
                .put("password", "azerty")
                .put("method", "username");

        // 1. log as user2
        client.post(3000, "localhost", "/login").sendJson(post_data, ans -> {

            if (ans.succeeded()){
                HttpResponse<Buffer> response = ans.result();

                if (response.statusCode() != 200){
                    async.resolve(Future.failedFuture("Can't log in"));
                }

                // 2. get jwt token
                String token = response.bodyAsString();

                // genereta picture bigger than 4.5*1024*1024 characters (base64 encoded)
                StringBuilder img = new StringBuilder("data:image/png;base64,");
                for (int i = 0; i < 4.5*1024*1024; i++){
                    img.append("A");
                }

                // update query
                JsonObject update_query = new JsonObject().put("picture", img.toString());
                // 3. send post request
                client.post(3000, "localhost", "/jwt/user_profil")
                        .putHeader("Authorization", "Bearer " + token).sendJson(update_query, res -> {

                    if (res.succeeded()){
                        // 4. check request response
                        HttpResponse<Buffer> answer = res.result();

                        context.assertEquals(400, answer.statusCode());
                        context.assertEquals("Erreur ! Fichier image trop volumineux.", answer.bodyAsString());

                        async.complete();

                    } else {
                        async.resolve(Future.failedFuture(res.cause()));
                    }
                });

            } else {
                async.resolve(Future.failedFuture(ans.cause()));
            }
        });
    }

    /*
    Test that user4 can't change his profil with existing mail
    1. log as user4
    2. get jwt token
    3. send post request
    4. check request response (msg and status code)
 */
    @Test
    public void updateUserProfilHandler_existingMail(TestContext context) {
        Async async = context.async();

        JsonObject post_data = new JsonObject().put("username", "updateUserProfil4")
                .put("password", "azerty")
                .put("method", "username");

        // 1. log as user2
        client.post(3000, "localhost", "/login").sendJson(post_data, ans -> {

            if (ans.succeeded()){
                HttpResponse<Buffer> response = ans.result();

                if (response.statusCode() != 200){
                    async.resolve(Future.failedFuture("Can't log in"));
                }

                // 2. get jwt token
                String token = response.bodyAsString();

                // update query
                JsonObject update_query = new JsonObject().put("mail", user2.getMail());
                // 3. send post request
                client.post(3000, "localhost", "/jwt/user_profil")
                        .putHeader("Authorization", "Bearer " + token).sendJson(update_query, res -> {

                    if (res.succeeded()){
                        // 4. check request response
                        HttpResponse<Buffer> answer = res.result();

                        context.assertEquals(400, answer.statusCode());
                        context.assertEquals("Adresse mail déjà utilisée", answer.bodyAsString());

                        async.complete();

                    } else {
                        async.resolve(Future.failedFuture(res.cause()));
                    }
                });

            } else {
                async.resolve(Future.failedFuture(ans.cause()));
            }
        });
    }

    /*
        Test that user4  can't change his own informations when requesting a post with wrong params
        1. log as user4
        2. get jwt token
        3. send post request
        4. check request response
     */
    @Test
    public void updateUserProfilHandler_wrongParams(TestContext context) {
        Async async = context.async();

        JsonObject post_data = new JsonObject().put("username", "updateUserProfil4")
                .put("password", "azerty")
                .put("method", "username");

        // 1. log as user2
        client.post(3000, "localhost", "/login").sendJson(post_data, ans -> {

            if (ans.succeeded()){
                HttpResponse<Buffer> response = ans.result();

                if (response.statusCode() != 200){
                    async.resolve(Future.failedFuture("Can't log in"));
                }

                // 2. get jwt token
                String token = response.bodyAsString();

                // update query
                JsonObject update_query = new JsonObject().put("username", "yolo");
                // 3. send post request
                client.post(3000, "localhost", "/jwt/user_profil")
                        .putHeader("Authorization", "Bearer " + token).sendJson(update_query, res -> {

                    if (res.succeeded()){
                        // 4. check request response
                        HttpResponse<Buffer> answer = res.result();
                        context.assertEquals(400, answer.statusCode());

                        async.complete();

                    } else {
                        async.resolve(Future.failedFuture(res.cause()));
                    }
                });

            } else {
                async.resolve(Future.failedFuture(ans.cause()));
            }
        });
    }


    /*
        Test that user3  can't change his own informations when requesting a post with empty params
        1. log as user4
        2. get jwt token
        3. send post request
        4. check request response
     */
    @Test
    public void updateUserProfilHandler_emptyParams(TestContext context) {
        Async async = context.async();

        JsonObject post_data = new JsonObject().put("username", "updateUserProfil4")
                .put("password", "azerty")
                .put("method", "username");

        // 1. log as user2
        client.post(3000, "localhost", "/login").sendJson(post_data, ans -> {

            if (ans.succeeded()){
                HttpResponse<Buffer> response = ans.result();

                if (response.statusCode() != 200){
                    async.resolve(Future.failedFuture("Can't log in"));
                }

                // 2. get jwt token
                String token = response.bodyAsString();
                // update query
                JsonObject update_query = new JsonObject().put("username", "");
                // 3. send post request
                client.post(3000, "localhost", "/jwt/user_profil")
                        .putHeader("Authorization", "Bearer " + token).sendJson(update_query, res -> {

                    if (res.succeeded()){
                        // 4. check request response
                        HttpResponse<Buffer> answer = res.result();
                        context.assertEquals(400, answer.statusCode());

                        async.complete();

                    } else {
                        async.resolve(Future.failedFuture(res.cause()));
                    }
                });

            } else {
                async.resolve(Future.failedFuture(ans.cause()));
            }
        });
    }
}