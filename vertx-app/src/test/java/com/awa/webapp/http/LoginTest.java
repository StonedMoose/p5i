package com.awa.webapp.http;

import com.awa.webapp.MainVerticle;
import com.awa.webapp.utils.User;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.mongo.MongoAuth;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.security.KeyException;
import java.util.Base64;
import java.util.concurrent.CountDownLatch;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(VertxUnitRunner.class)
public class LoginTest {

    private static Vertx vertx;
    private static WebClient client;
    private static MongoClient mongoClient;
    private static MongoAuth authUsername;
    private static MongoAuth authMail;

    @BeforeClass
    public static void setUp(TestContext context) {
        // create vertx Instance
        vertx = Vertx.vertx();

        // deploy database verticle
        vertx.deployVerticle(MainVerticle.class.getName(), context.asyncAssertSuccess());

        // create mongo client and mong auth provider
        mongoClient = MongoClient.createShared(vertx, new JsonObject().put("connection_string", "mongodb://localhost:27017")
                .put("db_name", "awa"));

        authUsername = MongoAuth.create(mongoClient, new JsonObject());
        authMail = MongoAuth.create(mongoClient, new JsonObject());

        authUsername.setCollectionName("users");
        authUsername.setUsernameField("username");

        authMail.setCollectionName("users");
        authMail.setUsernameField("mail");

        // create WebClient
        client = WebClient.create(vertx, new WebClientOptions().setFollowRedirects(false));
    }

    @AfterClass
    public static void tearDown(TestContext context) throws InterruptedException{
        CountDownLatch latch = new CountDownLatch(2);

        mongoClient.removeDocuments("users", new JsonObject(), res -> {
            if (res.succeeded()) latch.countDown();
        });
        mongoClient.removeDocuments("streamers", new JsonObject(), res -> {
            if (res.succeeded()) latch.countDown();
        });

        latch.await();
        vertx.close(context.asyncAssertSuccess());
    }

    /*
       Test that a user can logged in with good credentials (username/password)
       1. insert a user in db
       2. check that /login works and respond with HTTP/200 + jwtToken
    */
    @Test
    public void loginHandlerTest_WithGoodCredentialsUsername(TestContext context){
        Async async = context.async();

        JsonObject user_data = new JsonObject().put("username", "loginUsername")
                .put("firstName", "test")
                .put("lastName", "user")
                .put("mail", "loginUsername@account.com")
                .put("birthdate", "1990-07-29")
                .put("password", "azerty");

        User user;
        try {
            user = new User(user_data, authMail);
        } catch (KeyException e){
            async.resolve(Future.failedFuture(""));
            return;
        }

        // activate user
        user.setActivated(true);

        // first we insert the user in db
        mongoClient.insert("users", JsonObject.mapFrom(user), res -> {
            if (res.succeeded()){
                String id = res.result();

                JsonObject post_data = new JsonObject().put("username", user.getUsername())
                        .put("password", "azerty")
                        .put("method", "username");

                client.post(3000, "localhost", "/login").sendJson(post_data, ans -> {

                    if (ans.succeeded()){
                        // we check status code
                        context.assertEquals(200, ans.result().statusCode());

                        // check that jwt token has good values
                        HttpResponse<Buffer> response = ans.result();

                        String token = response.bodyAsString();
                        JsonObject body = new JsonObject(new String(Base64.getDecoder().decode(token.split("\\.")[1])));

                        context.assertEquals(id, body.getString("_id"));
                        context.assertEquals(user.getUsername(), body.getString("username"));
                        context.assertEquals(user.getMail(), body.getString("mail"));
                        context.assertEquals(user.getPicture(), body.getString("picture"));
                        context.assertNull(user.getStreamerId());

                        async.complete();

                    } else {
                        async.resolve(Future.failedFuture(ans.cause()));
                    }
                });

            } else {
                async.resolve(Future.failedFuture(res.cause()));
            }
        });
    }

    /*
        Test that a user can logged in with good credentials (mail/password)
        1. insert a user in db
        2. check that /login works and respond with HTTP/200 + jwtToken
     */
    @Test
    public void loginHandlerTest_WithGoodCredentialsMail(TestContext context){
        Async async = context.async();

        JsonObject user_data = new JsonObject().put("username", "loginMail")
                .put("firstName", "test")
                .put("lastName", "user")
                .put("mail", "loginMail@account.com")
                .put("birthdate", "1990-07-29")
                .put("password", "azerty");

        User user;
        try {
            user = new User(user_data, authMail);
        } catch (KeyException e){
            async.resolve(Future.failedFuture(""));
            return;
        }

        // activate user
        user.setActivated(true);

        // first we insert the user in db
        mongoClient.insert("users", JsonObject.mapFrom(user), res -> {
            if (res.succeeded()){
                String id = res.result();

                JsonObject post_data = new JsonObject().put("username", user.getMail())
                        .put("password", "azerty")
                        .put("method", "mail");

                client.post(3000, "localhost", "/login").sendJson(post_data, ans -> {

                    if (ans.succeeded()){
                        // we check status code
                        context.assertEquals(200, ans.result().statusCode());

                        // check that jwt token has good values
                        HttpResponse<Buffer> response = ans.result();

                        String token = response.bodyAsString();
                        JsonObject body = new JsonObject(new String(Base64.getDecoder().decode(token.split("\\.")[1])));

                        context.assertEquals(id, body.getString("_id"));
                        context.assertEquals(user.getUsername(), body.getString("username"));
                        context.assertEquals(user.getMail(), body.getString("mail"));
                        context.assertEquals(user.getPicture(), body.getString("picture"));
                        context.assertNull(user.getStreamerId());

                        async.complete();

                    } else {
                        async.resolve(Future.failedFuture(ans.cause()));
                    }
                });

            } else {
                async.resolve(Future.failedFuture(res.cause()));
            }
        });
    }

    /*
        Test that a user can logged in if his account isn't activated yet
        1. insert a user in db
        2. check that /login works and respond with HTTP/200 + jwtToken
     */
    @Test
    public void loginHandlerTest_UserNotActivated(TestContext context){
        Async async = context.async();

        JsonObject user_data = new JsonObject().put("username", "notActivated")
                .put("firstName", "test")
                .put("lastName", "user")
                .put("mail", "notActivated@account.com")
                .put("birthdate", "1990-07-29")
                .put("password", "azerty");

        User user;
        try {
            user = new User(user_data, authMail);
        } catch (KeyException e){
            async.resolve(Future.failedFuture(""));
            return;
        }

        // first we insert the user in db
        mongoClient.insert("users", JsonObject.mapFrom(user), res -> {
            if (res.succeeded()){
                String id = res.result();

                JsonObject post_data = new JsonObject().put("username", user.getMail())
                        .put("password", "azerty")
                        .put("method", "mail");

                client.post(3000, "localhost", "/login").sendJson(post_data, ans -> {

                    if (ans.succeeded()){
                        // we check status code
                        context.assertEquals(400, ans.result().statusCode());
                        async.complete();

                    } else {
                        async.resolve(Future.failedFuture(ans.cause()));
                    }
                });

            } else {
                async.resolve(Future.failedFuture(res.cause()));
            }
        });
    }



    /*
        Test that a user can't logged in with wrong credentials (username/password)
        1. insert a user in db
        2. check that /login respond with HTTP/404 + good error msg
     */
    @Test
    public void loginHandlerTest_WithWrongCredentialsUsername(TestContext context){
        Async async = context.async();

        JsonObject user_data = new JsonObject().put("username", "wrongLoginUsername")
                .put("firstName", "test")
                .put("lastName", "user")
                .put("mail", "wrongLoginUsername@account.com")
                .put("birthdate", "1990-07-29")
                .put("password", "azerty");

        User user;
        try {
            user = new User(user_data, authMail);
        } catch (KeyException e){
            async.resolve(Future.failedFuture(""));
            return;
        }

        // activate user
        user.setActivated(true);

        // first we insert the user in db
        mongoClient.insert("users", JsonObject.mapFrom(user), res -> {
            if (res.succeeded()){
                String id = res.result();

                JsonObject post_data = new JsonObject().put("username", user.getUsername())
                        .put("password", "yolo")
                        .put("method", "username");

                client.post(3000, "localhost", "/login").sendJson(post_data, ans -> {

                    if (ans.succeeded()){
                        // we check status code
                        context.assertEquals(404, ans.result().statusCode());

                        // check error message
                        HttpResponse<Buffer> response = ans.result();
                        String msg = response.bodyAsString();
                        context.assertEquals("Nom d'utilisateur/mot de passe invalide", msg);

                        async.complete();

                    } else {
                        async.resolve(Future.failedFuture(ans.cause()));
                    }
                });

            } else {
                async.resolve(Future.failedFuture(res.cause()));
            }
        });
    }

    /*
        Test that a user can't logged in with wrong credentials (mail/password)
        1. insert a user in db
        2. check that /login respond with HTTP/400 + good error msg
     */
    @Test
    public void loginHandlerTest_WithWrongCredentialsMail(TestContext context){
        Async async = context.async();

        JsonObject user_data = new JsonObject().put("username", "wrongLoginMail")
                .put("firstName", "test")
                .put("lastName", "user")
                .put("mail", "wrongLoginMail@account.com")
                .put("birthdate", "1990-07-29")
                .put("password", "azerty");

        User user;
        try {
            user = new User(user_data, authMail);
        } catch (KeyException e){
            async.resolve(Future.failedFuture(""));
            return;
        }

        // activate user
        user.setActivated(true);

        // first we insert the user in db
        mongoClient.insert("users", JsonObject.mapFrom(user), res -> {
            if (res.succeeded()){
                String id = res.result();

                JsonObject post_data = new JsonObject().put("username", user.getMail())
                        .put("password", "yolo")
                        .put("method", "mail");

                client.post(3000, "localhost", "/login").sendJson(post_data, ans -> {

                    if (ans.succeeded()){
                        // we check status code
                        context.assertEquals(404, ans.result().statusCode());

                        // check error message
                        HttpResponse<Buffer> response = ans.result();
                        String msg = response.bodyAsString();
                        context.assertEquals("E-mail/mot de passe invalide", msg);

                        async.complete();

                    } else {
                        async.resolve(Future.failedFuture(ans.cause()));
                    }
                });

            } else {
                async.resolve(Future.failedFuture(res.cause()));
            }
        });
    }

    /*
        Test that a user can't logged when username don't exist in db
        1. check that /login respond with HTTP/400 + good error msg
     */
    @Test
    public void loginHandlerTest_WithWrongCredentialsUsernameDontExist(TestContext context){
        Async async = context.async();

        JsonObject post_data = new JsonObject().put("username", "yolo")
                .put("password", "azerty")
                .put("method", "username");

        client.post(3000, "localhost", "/login").sendJson(post_data, ans -> {

            if (ans.succeeded()){
                // we check status code
                context.assertEquals(404, ans.result().statusCode());

                // check error message
                HttpResponse<Buffer> response = ans.result();
                String msg = response.bodyAsString();
                context.assertEquals("Pas de compte trouvé avec ce nom d'utilisateur", msg);

                async.complete();

            } else {
                async.resolve(Future.failedFuture(ans.cause()));
            }
        });

    }

    /*
        Test that a user can't logged when mail don't exist in db
        1. check that /login respond with HTTP/400 + good error msg
     */
    @Test
    public void loginHandlerTest_WithWrongCredentialsMailDontExist(TestContext context){
        Async async = context.async();

        JsonObject post_data = new JsonObject().put("username", "yolo")
                .put("password", "azerty")
                .put("method", "mail");

        client.post(3000, "localhost", "/login").sendJson(post_data, ans -> {

            if (ans.succeeded()){
                // we check status code
                context.assertEquals(404, ans.result().statusCode());

                // check error message
                HttpResponse<Buffer> response = ans.result();
                String msg = response.bodyAsString();
                context.assertEquals("Pas de compte trouvé avec cet e-mail", msg);

                async.complete();

            } else {
                async.resolve(Future.failedFuture(ans.cause()));
            }
        });

    }
}
