package com.awa.webapp.http;

import com.awa.webapp.MainVerticle;
import com.awa.webapp.utils.User;
import io.vertx.core.Future;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.mongo.MongoAuth;
import io.vertx.ext.mongo.BulkOperation;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.security.KeyException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(VertxUnitRunner.class)
public class OnPublishTest {

    private static Vertx vertx;
    private static WebClient client;
    private static MongoClient mongoClient;
    private static MongoAuth authUsername;
    private static MongoAuth authMail;

    private static JsonObject streamer_data1, streamer_data2, streamer_data3;

    @BeforeClass
    public static void setUp(TestContext context) throws InterruptedException, KeyException {
        // create vertx Instance
        vertx = Vertx.vertx();

        // deploy database verticle
        vertx.deployVerticle(MainVerticle.class.getName(), context.asyncAssertSuccess());

        // create mongo client and mong auth provider
        mongoClient = MongoClient.createShared(vertx, new JsonObject().put("connection_string", "mongodb://localhost:27017")
                .put("db_name", "awa"));

        authUsername = MongoAuth.create(mongoClient, new JsonObject());
        authMail = MongoAuth.create(mongoClient, new JsonObject());

        authUsername.setCollectionName("users");
        authUsername.setUsernameField("username");

        authMail.setCollectionName("users");
        authMail.setUsernameField("mail");

        // create WebClient
        client = WebClient.create(vertx, new WebClientOptions().setFollowRedirects(false));

        // create latch object to make setUp method to wait for asynchronous insertion before processing tests
        CountDownLatch latch = new CountDownLatch(2);

        // insert a user to log with
        JsonObject user_data1 = new JsonObject().put("username", "user1")
                .put("firstName", "toto")
                .put("lastName", "tata")
                .put("mail", "user1@awa.com")
                .put("birthdate", "1990-07-29")
                .put("password", "azerty");

        User user1 = new User(user_data1, authMail);
        user1.setActivated(true);

        mongoClient.insert("users", JsonObject.mapFrom(user1).put("_id", "user1id"), res -> {
            if (res.failed()){
                context.async().resolve(Future.failedFuture(res.cause()));
                return;
            } else {
                latch.countDown();
            }
        });

        // insert streamers
        streamer_data1 = new JsonObject()
                .put("_id", "streamer1id")
                .put("username", "streamer1")
                .put("dispos", new JsonArray().add("Lundi").add("Mardi"))
                .put("genres", new JsonArray().add("Rock").add("Reggae"))
                .put("iban", "FR3030002006633200692730X25")
                .put("bic", "AZEIHKHH")
                .put("doPrivate", false)
                .put("telephone", "0683925521")
                .put("price", "50")
                .put("picture", "/dist/static/streamer1id.png")
                .put("followers", new JsonArray().add("userXid").add("user1id").add("userYid"))
                .put("streamKey", "TLZSHLVDJJIHSZQZRZQMRUHO")
                .put("streamKeyExpiration", System.currentTimeMillis() + 30 * 60 * 1000)
                .put("onAir", false);

        streamer_data2 = new JsonObject()
                .put("_id", "streamer2id")
                .put("username", "streamer2")
                .put("genres", new JsonArray().add("Rock").add("Reggae"))
                .put("iban", "FR3030002006630D00692730X24")
                .put("bic", "BZEIHKOD")
                .put("doPrivate", true)
                .put("telephone", "0618189716")
                .put("dispos", new JsonArray())
                .put("price", "0")
                .put("picture", "/dist/static/streamer2id.png")
                .put("followers", new JsonArray().add("userXid").add("user1id"))
                .put("streamKey", "IVAQCLSRHAKUXOMTIUUEGTFD")
                .put("streamKeyExpiration", System.currentTimeMillis())
                .put("onAir", false);

        streamer_data3 = new JsonObject()
                .put("_id", "streamer3id")
                .put("username", "streamer3")
                .put("genres", new JsonArray().add("Rock").add("Reggae"))
                .put("iban", "FR3030002006630D00692730X22")
                .put("bic", "BZEITKOD")
                .put("doPrivate", false)
                .put("telephone", "0618129716")
                .put("dispos", new JsonArray().add("Mercredi").add("Vendredi"))
                .put("price", "110")
                .put("picture", "/dist/static/streamer3id.png")
                .put("followers", new JsonArray().add("userXid").add("user1id").add("userYid"))
                .put("streamKey", "BVIBLHMMLXACKXZHIJMDKKVE")
                .put("streamKeyExpiration", System.currentTimeMillis() + 30 * 60 * 1000)
                .put("onAir", true);

        List<BulkOperation> streamer_operations = Arrays.asList(
                BulkOperation.createInsert(streamer_data1),
                BulkOperation.createInsert(streamer_data2),
                BulkOperation.createInsert(streamer_data3)
        );

        mongoClient.bulkWrite("streamers", streamer_operations, res -> {
            if (res.failed()) {
                context.async().resolve(Future.failedFuture(res.cause()));
                return;
            } else {
                latch.countDown();
            }
        });

        latch.await(5, TimeUnit.SECONDS);

    }

    @AfterClass
    public static void tearDown(TestContext context) throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(2);

        mongoClient.removeDocuments("users", new JsonObject(), res -> {
            if (res.succeeded()) latch.countDown();
        });
        mongoClient.removeDocuments("streamers", new JsonObject(), res -> {
            if (res.succeeded()) latch.countDown();
        });

        latch.await();
        vertx.close(context.asyncAssertSuccess());
    }


    /*
        Test that a streamer with the good key can publish a stream before his key expire (streamer1)
        1. send a post request emulating nginx post on publish
        2. check status code
        3. check that user is onAir in DB
     */
    @Test
    public void onPublishTest_goodRequest(TestContext context) {
        Async async = context.async();

        MultiMap form = MultiMap.caseInsensitiveMultiMap();
        form.set("aParam", "someValue");
        form.set("name", streamer_data1.getString("_id") + "_" + streamer_data1.getString("streamKey"));
        // 1. send a post request emulating nginx post on publish
        client.post(3002, "localhost", "/on_publish").sendForm(form, ar -> {
            if (ar.succeeded()) {
                HttpResponse<Buffer> response = ar.result();
                // 2. check status code
                context.assertEquals(200, response.statusCode());

                // 3. check that user is onAir in DB
                JsonObject query = new JsonObject().put("_id", streamer_data1.getString("_id"));
                mongoClient.findOne("streamers", query, new JsonObject(), res -> {

                    if (res.succeeded()){
                        context.assertTrue(res.result().getBoolean("onAir"));
                        async.complete();
                    } else {
                        async.resolve(Future.failedFuture(ar.cause()));
                    }
                });

            } else {
                async.resolve(Future.failedFuture(ar.cause()));
            }
        });
    }

    /*
        Test that a streamer with the good key can't publish a stream if his key has expired
        1. send a post request emulating nginx post on publish
        2. check status code
        3. check that user is still offAir on DB
     */
    @Test
    public void onPublishTest_ExpiredKey(TestContext context) {
        Async async = context.async();

        MultiMap form = MultiMap.caseInsensitiveMultiMap();
        form.set("aParam", "someValue");
        form.set("name", streamer_data2.getString("_id") + "_" + streamer_data2.getString("streamKey"));
        // 1. send a post request emulating nginx post on publish
        client.post(3002, "localhost", "/on_publish").sendForm(form, ar -> {
            if (ar.succeeded()) {
                HttpResponse<Buffer> response = ar.result();
                // 2. check status code
                context.assertEquals(400, response.statusCode());

                // 3. check that user is onAir in DB
                JsonObject query = new JsonObject().put("_id", streamer_data2.getString("_id"));
                mongoClient.findOne("streamers", query, new JsonObject(), res -> {

                    if (res.succeeded()){
                        context.assertFalse(res.result().getBoolean("onAir"));
                        async.complete();
                    } else {
                        async.resolve(Future.failedFuture(ar.cause()));
                    }
                });

            } else {
                async.resolve(Future.failedFuture(ar.cause()));
            }
        });
    }


    /*
        Test that a streamer with the good key can't publish a stream if his key has expired
        1. send a post request emulating nginx post on publish
        2. check status code
        3. check that user is still offAir on DB
     */
    @Test
    public void onPublishTest_AlreadyOnAir(TestContext context) {
        Async async = context.async();

        MultiMap form = MultiMap.caseInsensitiveMultiMap();
        form.set("aParam", "someValue");
        form.set("name", streamer_data3.getString("_id") + "_" + streamer_data3.getString("streamKey"));
        // 1. send a post request emulating nginx post on publish
        client.post(3002, "localhost", "/on_publish").sendForm(form, ar -> {
            if (ar.succeeded()) {
                HttpResponse<Buffer> response = ar.result();
                // 2. check status code
                context.assertEquals(400, response.statusCode());

                // 3. check that user is onAir in DB
                JsonObject query = new JsonObject().put("_id", streamer_data3.getString("_id"));
                mongoClient.findOne("streamers", query, new JsonObject(), res -> {

                    if (res.succeeded()){
                        context.assertTrue(res.result().getBoolean("onAir"));
                        async.complete();
                    } else {
                        async.resolve(Future.failedFuture(ar.cause()));
                    }
                });

            } else {
                async.resolve(Future.failedFuture(ar.cause()));
            }
        });
    }
}