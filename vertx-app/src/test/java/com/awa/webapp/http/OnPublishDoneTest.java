package com.awa.webapp.http;

import com.awa.webapp.MainVerticle;
import io.vertx.core.Future;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.mongo.MongoAuth;
import io.vertx.ext.mongo.BulkOperation;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(VertxUnitRunner.class)
public class OnPublishDoneTest {

    private static Vertx vertx;
    private static WebClient client;
    private static MongoClient mongoClient;
    private static MongoAuth authUsername;
    private static MongoAuth authMail;

    private static JsonObject streamer_data1;

    @BeforeClass
    public static void setUp(TestContext context) throws InterruptedException{
        // create vertx Instance
        vertx = Vertx.vertx();

        // deploy database verticle
        vertx.deployVerticle(MainVerticle.class.getName(), context.asyncAssertSuccess());

        // create mongo client and mong auth provider
        mongoClient = MongoClient.createShared(vertx, new JsonObject().put("connection_string", "mongodb://localhost:27017")
                .put("db_name", "awa"));

        authUsername = MongoAuth.create(mongoClient, new JsonObject());
        authMail = MongoAuth.create(mongoClient, new JsonObject());

        authUsername.setCollectionName("users");
        authUsername.setUsernameField("username");

        authMail.setCollectionName("users");
        authMail.setUsernameField("mail");

        // create WebClient
        client = WebClient.create(vertx, new WebClientOptions().setFollowRedirects(false));

        // create latch object to make setUp method to wait for asynchronous insertion before processing tests
        CountDownLatch latch = new CountDownLatch(1);

        // create associated streamer
        streamer_data1 = new JsonObject()
                .put("_id", "user1id")
                .put("username", "user1")
                .put("dispos", new JsonArray().add("Lundi").add("Mardi"))
                .put("genres", new JsonArray().add("Rock").add("Reggae"))
                .put("iban", "FR3030002006630000692730X25")
                .put("bic", "AZEIHKOH")
                .put("doPrivate", true)
                .put("telephone", "0683925519")
                .put("price", "50")
                .put("picture", "some_path");

        List<BulkOperation> streamer_operations = Arrays.asList(
                BulkOperation.createInsert(streamer_data1)
        );

        mongoClient.bulkWrite("streamers", streamer_operations, res -> {
            if (res.failed()) {
                context.async().resolve(Future.failedFuture(res.cause()));
                return;
            } else {
                latch.countDown();
            }
        });

        latch.await(5, TimeUnit.SECONDS);

    }

    @AfterClass
    public static void tearDown(TestContext context) throws InterruptedException{
        CountDownLatch latch = new CountDownLatch(2);

        mongoClient.removeDocuments("users", new JsonObject(), res -> {
            if (res.succeeded()) latch.countDown();
        });
        mongoClient.removeDocuments("streamers", new JsonObject(), res -> {
            if (res.succeeded()) latch.countDown();
        });

        latch.await();
        vertx.close(context.asyncAssertSuccess());
    }

    @Test
    public void onPublishDone_setStreamerOffAirTest(TestContext context) {

        System.out.println("## before async ##");
        Async async = context.async();

        MultiMap form = MultiMap.caseInsensitiveMultiMap();
        form.set("aParam", "someValue");
        form.set("name", streamer_data1.getString("_id") + "_" + streamer_data1.getString("streamKey"));
        // 1. send a post request emulating nginx post on publish
        client.post(3002, "localhost", "/on_publish_done").sendForm(form, ar -> {
            if (ar.succeeded()) {
                HttpResponse<Buffer> response = ar.result();
                // 2. check status code
                context.assertEquals(200, response.statusCode());
                // 3. check that user is onAir in DB
                JsonObject query = new JsonObject().put("_id", streamer_data1.getString("_id"));
                mongoClient.findOne("streamers", query, new JsonObject(), res -> {

                    if (res.succeeded()){
                        context.assertFalse(res.result().getBoolean("onAir"));
                        async.complete();
                    } else {
                        async.resolve(Future.failedFuture(ar.cause()));
                    }
                });

            } else {
                async.resolve(Future.failedFuture(ar.cause()));
            }
        });
    }


}