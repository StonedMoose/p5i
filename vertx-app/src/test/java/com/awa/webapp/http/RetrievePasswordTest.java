package com.awa.webapp.http;

import com.awa.webapp.MainVerticle;
import com.awa.webapp.utils.User;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.mongo.MongoAuth;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.security.KeyException;
import java.util.concurrent.CountDownLatch;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(VertxUnitRunner.class)
public class RetrievePasswordTest {

    private static Vertx vertx;
    private static WebClient client;
    private static MongoClient mongoClient;
    private static MongoAuth authUsername;
    private static MongoAuth authMail;

    @BeforeClass
    public static void setUp(TestContext context) {
        // create vertx Instance
        vertx = Vertx.vertx();

        // deploy database verticle
        vertx.deployVerticle(MainVerticle.class.getName(), context.asyncAssertSuccess());

        // create mongo client and mong auth provider
        mongoClient = MongoClient.createShared(vertx, new JsonObject().put("connection_string", "mongodb://localhost:27017")
                .put("db_name", "awa"));

        authUsername = MongoAuth.create(mongoClient, new JsonObject());
        authMail = MongoAuth.create(mongoClient, new JsonObject());

        authUsername.setCollectionName("users");
        authUsername.setUsernameField("username");

        authMail.setCollectionName("users");
        authMail.setUsernameField("mail");

        // create WebClient
        client = WebClient.create(vertx, new WebClientOptions().setFollowRedirects(false));
    }

    @AfterClass
    public static void tearDown(TestContext context) throws InterruptedException{
        CountDownLatch latch = new CountDownLatch(2);

        mongoClient.removeDocuments("users", new JsonObject(), res -> {
            if (res.succeeded()) latch.countDown();
        });
        mongoClient.removeDocuments("streamers", new JsonObject(), res -> {
            if (res.succeeded()) latch.countDown();
        });

        latch.await();
        vertx.close(context.asyncAssertSuccess());
    }

    /*
          Test that a user can retrieve his password with his username
          1. insert user in db to test on
          2. Post data for retrieving procedure
          3. check that activationKey has been changed
       */
    @Test
    public void retrievePasswordHandler_UsernameExist(TestContext context){
        Async async = context.async();

        JsonObject user_data = new JsonObject().put("username", "retrievePasswordUsernameExist")
                .put("firstName", "retrievePassword")
                .put("lastName", "UsernameExist")
                .put("mail", "retrievePasswordUsernameExist@gmail.com")
                .put("birthdate", "1990-07-29")
                .put("password", "azerty");

        User user;
        try {
            user = new User(user_data, authMail);
        } catch (KeyException e){
            async.resolve(Future.failedFuture(""));
            return;
        }

        // activate user and get old activationKey
        String oldKey = user.getActivationKey();
        user.setActivated(true);

        //  1. insert user in db to test on
        mongoClient.insert("users", JsonObject.mapFrom(user), res -> {
            if (res.succeeded()){
                JsonObject post_data = new JsonObject().put("username", user.getUsername());
                // 2. Post data for retrieving procedure
                client.post(3000, "localhost", "/retrieve_password").sendJson(post_data, ans -> {

                    if (ans.succeeded()){
                        // we check status code
                        context.assertEquals(200, ans.result().statusCode());

                        // 3. check that activationKey has been changed
                        mongoClient.findOne("users", new JsonObject().put("username", user.getUsername()), new JsonObject(), reply -> {
                            if (reply.succeeded()){
                                context.assertNotEquals(oldKey, reply.result().getString("activationKey"));
                                async.complete();
                            } else {
                                async.resolve(Future.failedFuture(reply.cause()));
                            }
                        });
                    } else {
                        async.resolve(Future.failedFuture(ans.cause()));
                    }
                });
            } else {
                async.resolve(Future.failedFuture(res.cause()));
            }
        });
    }

    /*
        Test that a user can retrieve his password with his mail
        1. insert user in db to test on
        2. Post data for retrieving procedure
        3. check that activationKey has been changed
     */
    @Test
    public void retrievePasswordHandler_MailExist(TestContext context){
        Async async = context.async();

        JsonObject user_data = new JsonObject().put("username", "retrievePasswordMailExist")
                .put("firstName", "retrievePassword")
                .put("lastName", "MailExist")
                .put("mail", "retrievePasswordMailExist@gmail.com")
                .put("birthdate", "1990-07-29")
                .put("password", "azerty");

        User user;
        try {
            user = new User(user_data, authMail);
        } catch (KeyException e){
            async.resolve(Future.failedFuture(""));
            return;
        }

        // activate user and get old activationKey
        String oldKey = user.getActivationKey();
        user.setActivated(true);

        //  1. insert user in db to test on
        mongoClient.insert("users", JsonObject.mapFrom(user), res -> {
            if (res.succeeded()){
                JsonObject post_data = new JsonObject().put("mail", user.getMail());
                // 2. Post data for retrieving procedure
                client.post(3000, "localhost", "/retrieve_password").sendJson(post_data, ans -> {

                    if (ans.succeeded()){
                        // we check status code
                        context.assertEquals(200, ans.result().statusCode());

                        // 3. check that activationKey has been changed
                        mongoClient.findOne("users", new JsonObject().put("username", user.getUsername()), new JsonObject(), reply -> {
                            if (reply.succeeded()){
                                context.assertNotEquals(oldKey, reply.result().getString("activationKey"));
                                async.complete();
                            } else {
                                async.resolve(Future.failedFuture(reply.cause()));
                            }
                        });
                    } else {
                        async.resolve(Future.failedFuture(ans.cause()));
                    }
                });
            } else {
                async.resolve(Future.failedFuture(res.cause()));
            }
        });
    }

    /*
        Test that a user can't retrieve his password with unknown username
        1. Post data for retrieving procedure with dumb username
        2. check status code HTTP/400 and error message
     */
    @Test
    public void retrievePasswordHandler_UsernameUnknown(TestContext context){
        Async async = context.async();

        //  Post data for retrieving procedure with dumb username
        JsonObject post_data = new JsonObject().put("username", "someDummyUsernameWhichDoesntExist");
        client.post(3000, "localhost", "/retrieve_password").sendJson(post_data, ans -> {

            if (ans.succeeded()){
                // 2. check status code HTTP/400 and error message
                context.assertEquals(400, ans.result().statusCode());

                HttpResponse<Buffer> response = ans.result();
                String msg = response.bodyAsString();
                context.assertEquals("Pas d'utilisateur connu avec cet identifiant", msg);

                async.complete();
            } else {
                async.resolve(Future.failedFuture(ans.cause()));
            }
        });
    }

    /*
        Test that a user can't retrieve his password with unknown mail
        1. Post data for retrieving procedure with dumb mail
        2. check status code HTTP/400 and error message
     */
    @Test
    public void retrievePasswordHandler_MailUnknown(TestContext context){
        Async async = context.async();

        //  Post data for retrieving procedure with dumb username
        JsonObject post_data = new JsonObject().put("username", "someDummyUsernameWhichDoesntExist@gmail.com");
        client.post(3000, "localhost", "/retrieve_password").sendJson(post_data, ans -> {

            if (ans.succeeded()){
                // 2. check status code HTTP/400 and error message
                context.assertEquals(400, ans.result().statusCode());

                HttpResponse<Buffer> response = ans.result();
                String msg = response.bodyAsString();
                context.assertEquals("Pas d'utilisateur connu avec cet identifiant", msg);

                async.complete();
            } else {
                async.resolve(Future.failedFuture(ans.cause()));
            }
        });
    }

    /*
        Test that a user can't retrieve his password with empty username
        1. Post data for retrieving procedure with dumb mail
        2. check status code HTTP/400 and error message
     */
    @Test
    public void retrievePasswordHandler_EmpyUsername(TestContext context){
        Async async = context.async();

        //  Post data for retrieving procedure with dumb username
        JsonObject post_data = new JsonObject().put("username", "");
        client.post(3000, "localhost", "/retrieve_password").sendJson(post_data, ans -> {

            if (ans.succeeded()){
                // 2. check status code HTTP/400 and error message
                context.assertEquals(400, ans.result().statusCode());

                HttpResponse<Buffer> response = ans.result();
                String msg = response.bodyAsString();
                context.assertEquals("Pas d'utilisateur connu avec cet identifiant", msg);

                async.complete();
            } else {
                async.resolve(Future.failedFuture(ans.cause()));
            }
        });
    }

    /*
        Test that a user can't retrieve his password wrong params
        1. Post data for retrieving procedure with dumb mail
        2. check status code HTTP/400 and error message
     */
    @Test
    public void retrievePasswordHandler_WrongParams(TestContext context){
        Async async = context.async();

        //  Post data for retrieving procedure with dumb username
        JsonObject post_data = new JsonObject().put("activated", true).put("method", "username");
        client.post(3000, "localhost", "/retrieve_password").sendJson(post_data, ans -> {

            if (ans.succeeded()){
                // 2. check status code HTTP/400 and error message
                context.assertEquals(400, ans.result().statusCode());

                HttpResponse<Buffer> response = ans.result();
                String msg = response.bodyAsString();
                context.assertNull(msg);

                async.complete();
            } else {
                async.resolve(Future.failedFuture(ans.cause()));
            }
        });
    }
}
