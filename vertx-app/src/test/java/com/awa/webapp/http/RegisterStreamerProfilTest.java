package com.awa.webapp.http;

import com.awa.webapp.MainVerticle;
import com.awa.webapp.utils.User;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.file.FileSystem;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.mongo.MongoAuth;
import io.vertx.ext.mongo.BulkOperation;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.security.KeyException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(VertxUnitRunner.class)
public class RegisterStreamerProfilTest {

    private static Vertx vertx;
    private static WebClient client;
    private static MongoClient mongoClient;
    private static FileSystem fs;
    private static MongoAuth authUsername;
    private static MongoAuth authMail;
    private static User user1, user2, user3, user4;

    private static JsonObject streamer_data1;

    private static String img_base64 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAQAAAAECAIAAAAmkwkpAAAACXBIWXMAAAsTAA" +
            "ALEwEAmpwYAAAAB3RJTUUH4gMEDhgQyqhYmQAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUHAAAALUlEQVQI103JoREAM" +
            "AwDsW8vBgYG2SX7zxZQUlGdmQFsJ7l8yvYbSZUEkNTdCzJaAZgMY+zMAAAAAElFTkSuQmCC";

    @BeforeClass
    public static void setUp(TestContext context) throws InterruptedException, KeyException {
        // create vertx Instance
        vertx = Vertx.vertx();
        fs = vertx.fileSystem();

        // deploy database verticle
        vertx.deployVerticle(MainVerticle.class.getName(), context.asyncAssertSuccess());

        // create mongo client and mong auth provider
        mongoClient = MongoClient.createShared(vertx, new JsonObject().put("connection_string", "mongodb://localhost:27017")
                .put("db_name", "awa"));

        authUsername = MongoAuth.create(mongoClient, new JsonObject());
        authMail = MongoAuth.create(mongoClient, new JsonObject());

        authUsername.setCollectionName("users");
        authUsername.setUsernameField("username");

        authMail.setCollectionName("users");
        authMail.setUsernameField("mail");

        // create WebClient
        client = WebClient.create(vertx, new WebClientOptions().setFollowRedirects(false));

        // insert users in DB
        JsonObject user_data1 = new JsonObject().put("username", "registerStreamerProfil1")
                .put("firstName", "toto")
                .put("lastName", "tata")
                .put("mail", "register@streamerprofil1.com")
                .put("birthdate", "1990-07-29")
                .put("password", "azerty");

        JsonObject user_data2 = new JsonObject().put("username", "registerStreamerProfil2")
                .put("firstName", "toto")
                .put("lastName", "tata")
                .put("mail", "register@streamerprofil2.com")
                .put("birthdate", "1990-07-29")
                .put("password", "azerty");

        JsonObject user_data3 = new JsonObject().put("username", "registerStreamerProfil3")
                .put("firstName", "toto")
                .put("lastName", "tata")
                .put("mail", "register@streamerprofil3.com")
                .put("birthdate", "1990-07-29")
                .put("password", "azerty");

        JsonObject user_data4 = new JsonObject().put("username", "registerStreamerProfil4")
                .put("firstName", "toto")
                .put("lastName", "tata")
                .put("mail", "register@streamerprofil4.com")
                .put("birthdate", "1990-07-29")
                .put("password", "azerty");

        user1 = new User(user_data1, authMail);
        user2 = new User(user_data2, authMail);
        user3 = new User(user_data3, authMail);
        user4 = new User(user_data4, authMail);

        user1.setStreamerId("registerstreamerprofil1");

        for (User u: Arrays.asList(user1, user2, user3, user4)){
            u.setActivated(true);
        }

        // create latch object to make setUp method to wait for asynchronous insertion before processing tests
        CountDownLatch latch = new CountDownLatch(2);


        List<BulkOperation> operations = Arrays.asList(
                BulkOperation.createInsert(JsonObject.mapFrom(user1).put("_id", "user1id")),
                BulkOperation.createInsert(JsonObject.mapFrom(user2).put("_id", "user2id")),
                BulkOperation.createInsert(JsonObject.mapFrom(user3).put("_id", "user3id")),
                BulkOperation.createInsert(JsonObject.mapFrom(user4).put("_id", "user4id")));

        mongoClient.bulkWrite("users", operations, res -> {
            if (res.failed()) {
                context.asyncAssertFailure();
            } else {
                latch.countDown();
            }
        });

        // create associated streamer
        streamer_data1 = new JsonObject()
                .put("_id", user1.getStreamerId())
                .put("username",user1.getUsername())
                .put("dispos", new JsonArray().add("Lundi").add("Mardi"))
                .put("genres", new JsonArray().add("Rock").add("Reggae"))
                .put("iban", "FR3030002006630000692730X25")
                .put("bic", "AZEIHKOH")
                .put("doPrivate", true)
                .put("telephone", "0683925519")
                .put("price", "50");

        mongoClient.insert("streamers", streamer_data1, res -> {
            if (res.failed()) {
                context.async().resolve(Future.failedFuture(res.cause()));
                return;
            } else {
                latch.countDown();
            }
        });

        latch.await(5, TimeUnit.SECONDS);

    }

    @AfterClass
    public static void tearDown(TestContext context) throws InterruptedException{
        CountDownLatch latch = new CountDownLatch(2);

        mongoClient.removeDocuments("users", new JsonObject(), res -> {
            if (res.succeeded()) latch.countDown();
        });
        mongoClient.removeDocuments("streamers", new JsonObject(), res -> {
            if (res.succeeded()) latch.countDown();
        });

        fs.deleteBlocking("src/frontend/dist/static/user2id_stream.png");
        fs.deleteBlocking("src/frontend/dist/static/user3id_stream.png");
        fs.deleteBlocking("src/frontend/dist/static/user4id_stream.png");
        latch.await();

        vertx.close(context.asyncAssertSuccess());
    }


    /*
        Test that a user can register as a streamer with all params
        1. log as user2
        2. get jwt token
        3. send post request
        4. check that parameters returned are right
        5. check that changes have been commit int db
     */
    @Test
    public void registerStreamerProfilHandler_FullRegister(TestContext context) {
        Async async = context.async();

        JsonObject post_data = new JsonObject().put("username", "registerStreamerProfil2")
                .put("password", "azerty")
                .put("method", "username");

        //  1. log as user2
        client.post(3000, "localhost", "/login").sendJson(post_data, ans -> {

            if (ans.succeeded()){
                HttpResponse<Buffer> response = ans.result();

                if (response.statusCode() != 200){
                    async.resolve(Future.failedFuture("Can't log in"));
                }

                // 2. get jwt token
                String token = response.bodyAsString();

                JsonObject data = new JsonObject()
                        .put("dispos", new JsonArray().add("Lundi").add("Mardi"))
                        .put("genres", new JsonArray().add("Rock").add("Reggae"))
                        .put("iban", "FR3030002006630000692730X24")
                        .put("bic", "AZEIHSOH")
                        .put("doPrivate", true)
                        .put("telephone", "0683925518")
                        .put("price", "50")
                        .put("picture", img_base64);

                // 3. send post request
                client.put(3000, "localhost", "/jwt/streamer_profil")
                        .putHeader("Authorization", "Bearer " + token).sendJson(data, res -> {

                    if (res.succeeded()){
                        // 4. check that parameters returned are right
                        HttpResponse<Buffer> answer = res.result();

                        context.assertEquals(200, answer.statusCode());
                        JsonObject json_answer = answer.bodyAsJsonObject();

                        for (String s: Arrays.asList("doPrivate", "genres", "iban", "bic", "telephone", "dispos",
                                "price")){
                            context.assertEquals(data.getValue(s), json_answer.getValue(s));
                        }
                        context.assertEquals("/dist/static/user2id_stream.png", json_answer.getString("picture"));

                        // 5. check that changes have been commit in db
                        mongoClient.findOne("streamers",
                                new JsonObject().put("username", user2.getUsername()),
                                new JsonObject(),
                                reply -> {

                            if (reply.succeeded()){
                                for (String s: Arrays.asList("doPrivate", "genres", "iban", "bic", "telephone", "dispos",
                                        "price")){
                                    context.assertEquals(data.getValue(s), reply.result().getValue(s));
                                }
                                context.assertEquals(user2.getUsername(), reply.result().getString("username"));
                                context.assertEquals("/dist/static/user2id_stream.png", reply.result().getString("picture"));
                                context.assertFalse(reply.result().getBoolean("onAir"));
                                context.assertNotNull(reply.result().getString("streamKey"));

                                mongoClient.findOne("users", new JsonObject().put("username", user2.getUsername()),new
                                        JsonObject(), resp -> {
                                    if (resp.succeeded()){
                                        context.assertEquals(reply.result().getString("_id"), resp.result().getString
                                                ("streamerId"));
                                        async.complete();
                                    } else {
                                        async.resolve(Future.failedFuture(resp.cause()));
                                    }
                                });

                            } else {
                                async.resolve(Future.failedFuture(reply.cause()));
                            }
                        });
                    } else {
                        async.resolve(Future.failedFuture(res.cause()));
                    }
                });
            } else {
                async.resolve(Future.failedFuture(ans.cause()));
            }
        });
    }

    /*
        Test that a user can register as streamer without dispos and price when doPrivate == false
        1. log as user3
        2. get jwt token
        3. send post request
        4. check that parameters returned are right
        5. check that changes have been commit int db
     */
    @Test
    public void registerStreamerProfilHandler_NotFullRegister(TestContext context) {
        Async async = context.async();

        JsonObject post_data = new JsonObject().put("username", "registerStreamerProfil3")
                .put("password", "azerty")
                .put("method", "username");

        //  1. log as user2
        client.post(3000, "localhost", "/login").sendJson(post_data, ans -> {

            if (ans.succeeded()){
                HttpResponse<Buffer> response = ans.result();

                if (response.statusCode() != 200){
                    async.resolve(Future.failedFuture("Can't log in"));
                }

                // 2. get jwt token
                String token = response.bodyAsString();

                JsonObject data = new JsonObject()
                        .put("genres", new JsonArray().add("Rock").add("Reggae"))
                        .put("iban", "FR3030002006630000692730X23")
                        .put("bic", "AZEIHSOQ")
                        .put("doPrivate", false)
                        .put("telephone", "0683925517")
                        .put("picture", img_base64);

                // 3. send post request
                client.put(3000, "localhost", "/jwt/streamer_profil")
                        .putHeader("Authorization", "Bearer " + token).sendJson(data, res -> {

                    if (res.succeeded()){
                        // 4. check that parameters returned are right
                        HttpResponse<Buffer> answer = res.result();

                        context.assertEquals(200, answer.statusCode());
                        JsonObject json_answer = answer.bodyAsJsonObject();

                        for (String s: Arrays.asList("doPrivate", "genres", "iban", "bic", "telephone")){
                            context.assertEquals(data.getValue(s), json_answer.getValue(s));
                        }
                        context.assertEquals(new JsonArray(), json_answer.getJsonArray("dispos"));
                        context.assertEquals("0", json_answer.getString("price"));
                        context.assertEquals("/dist/static/user3id_stream.png", json_answer.getString("picture"));

                        // 5. check that changes have been commit in db
                        mongoClient.findOne("streamers",
                            new JsonObject().put("username", user3.getUsername()),
                            new JsonObject(),
                            reply -> {

                                if (reply.succeeded()){
                                    for (String s: Arrays.asList("doPrivate", "genres", "iban", "bic", "telephone")){
                                        context.assertEquals(data.getValue(s), reply.result().getValue(s));
                                    }
                                    context.assertEquals(new JsonArray(), reply.result().getJsonArray("dispos"));
                                    context.assertEquals("0", reply.result().getString("price"));
                                    context.assertEquals("/dist/static/user3id_stream.png", reply.result().getString("picture"));
                                    context.assertEquals(user3.getUsername(), reply.result().getString("username"));
                                    context.assertFalse(reply.result().getBoolean("onAir"));
                                    context.assertNotNull(reply.result().getString("streamKey"));

                                    mongoClient.findOne("users", new JsonObject().put("username", user3.getUsername()),new
                                            JsonObject(), resp -> {
                                        if (resp.succeeded()){
                                            context.assertEquals(reply.result().getString("_id"), resp.result().getString
                                                    ("streamerId"));
                                            async.complete();
                                        } else {
                                            async.resolve(Future.failedFuture(resp.cause()));
                                        }
                                    });

                                } else {
                                    async.resolve(Future.failedFuture(reply.cause()));
                                }
                            });
                    } else {
                        async.resolve(Future.failedFuture(res.cause()));
                    }
                });
            } else {
                async.resolve(Future.failedFuture(ans.cause()));
            }
        });
    }

    /*
        Test that a user can't register two times
        1. log as user1
        2. get jwt token
        3. send post request
        4. check response status code
     */
    @Test
    public void registerStreamerProfilHandler_AlreadyStreamer(TestContext context) {
        Async async = context.async();

        JsonObject post_data = new JsonObject().put("username", "registerStreamerProfil1")
                .put("password", "azerty")
                .put("method", "username");

        //  1. log as user2
        client.post(3000, "localhost", "/login").sendJson(post_data, ans -> {

            if (ans.succeeded()){
                HttpResponse<Buffer> response = ans.result();

                if (response.statusCode() != 200){
                    async.resolve(Future.failedFuture("Can't log in"));
                }

                // 2. get jwt token
                String token = response.bodyAsString();

                JsonObject data = new JsonObject()
                        .put("genres", new JsonArray().add("Rock").add("Reggae"))
                        .put("iban", "FR3030002006630000692730X23")
                        .put("bic", "AZEIHSOQ")
                        .put("doPrivate", false)
                        .put("telephone", "0683925517")
                        .put("picture", img_base64);

                // 3. send post request
                client.put(3000, "localhost", "/jwt/streamer_profil")
                        .putHeader("Authorization", "Bearer " + token).sendJson(data, res -> {

                    if (res.succeeded()){
                        // 4. check response status code
                        HttpResponse<Buffer> answer = res.result();

                        context.assertEquals(400, answer.statusCode());
                        async.complete();

                    } else {
                        async.resolve(Future.failedFuture(res.cause()));
                    }
                });
            } else {
                async.resolve(Future.failedFuture(ans.cause()));
            }
        });
    }

    /*
        Test that a user can't register with existing telephone
        1. log as user4
        2. get jwt token
        3. send post request
        4. check response status code and msg
     */
    @Test
    public void registerStreamerProfilHandler_ExistingTelephone(TestContext context) {
        Async async = context.async();

        JsonObject post_data = new JsonObject().put("username", "registerStreamerProfil4")
                .put("password", "azerty")
                .put("method", "username");

        //  1. log as user2
        client.post(3000, "localhost", "/login").sendJson(post_data, ans -> {

            if (ans.succeeded()){
                HttpResponse<Buffer> response = ans.result();

                if (response.statusCode() != 200){
                    async.resolve(Future.failedFuture("Can't log in"));
                }

                // 2. get jwt token
                String token = response.bodyAsString();

                JsonObject data = new JsonObject()
                        .put("genres", new JsonArray().add("Rock").add("Reggae"))
                        .put("iban", "FR3030002006630000692730X21")
                        .put("bic", "AZEIHXOQ")
                        .put("doPrivate", false)
                        .put("telephone", "0683925519")
                        .put("picture", img_base64);

                // 3. send post request
                client.put(3000, "localhost", "/jwt/streamer_profil")
                        .putHeader("Authorization", "Bearer " + token).sendJson(data, res -> {

                    if (res.succeeded()){
                        // 4. check response status code
                        HttpResponse<Buffer> answer = res.result();

                        context.assertEquals(400, answer.statusCode());
                        context.assertEquals("Téléphone déjà utilisé", answer.bodyAsString());
                        async.complete();

                    } else {
                        async.resolve(Future.failedFuture(res.cause()));
                    }
                });
            } else {
                async.resolve(Future.failedFuture(ans.cause()));
            }
        });
    }

    /*
        Test that a user can't register with existing iban
        1. log as user4
        2. get jwt token
        3. send post request
        4. check response status code and msg
     */
    @Test
    public void registerStreamerProfilHandler_ExistingIban(TestContext context) {
        Async async = context.async();

        JsonObject post_data = new JsonObject().put("username", "registerStreamerProfil4")
                .put("password", "azerty")
                .put("method", "username");

        //  1. log as user2
        client.post(3000, "localhost", "/login").sendJson(post_data, ans -> {

            if (ans.succeeded()){
                HttpResponse<Buffer> response = ans.result();

                if (response.statusCode() != 200){
                    async.resolve(Future.failedFuture("Can't log in"));
                }

                // 2. get jwt token
                String token = response.bodyAsString();

                JsonObject data = new JsonObject()
                        .put("genres", new JsonArray().add("Rock").add("Reggae"))
                        .put("iban", "FR3030002006630000692730X25")
                        .put("bic", "AZEIHXOQ")
                        .put("doPrivate", false)
                        .put("telephone", "0683925511")
                        .put("picture", img_base64);

                // 3. send post request
                client.put(3000, "localhost", "/jwt/streamer_profil")
                        .putHeader("Authorization", "Bearer " + token).sendJson(data, res -> {

                    if (res.succeeded()){
                        // 4. check response status code
                        HttpResponse<Buffer> answer = res.result();

                        context.assertEquals(400, answer.statusCode());
                        context.assertEquals("IBAN déjà utilisé", answer.bodyAsString());
                        async.complete();

                    } else {
                        async.resolve(Future.failedFuture(res.cause()));
                    }
                });
            } else {
                async.resolve(Future.failedFuture(ans.cause()));
            }
        });
    }

    /*
        Test that a user can't register with existing bic
        1. log as user4
        2. get jwt token
        3. send post request
        4. check response status code and msg
     */
    @Test
    public void registerStreamerProfilHandler_ExistingBic(TestContext context) {
        Async async = context.async();

        JsonObject post_data = new JsonObject().put("username", "registerStreamerProfil4")
                .put("password", "azerty")
                .put("method", "username");

        //  1. log as user2
        client.post(3000, "localhost", "/login").sendJson(post_data, ans -> {

            if (ans.succeeded()){
                HttpResponse<Buffer> response = ans.result();

                if (response.statusCode() != 200){
                    async.resolve(Future.failedFuture("Can't log in"));
                }

                // 2. get jwt token
                String token = response.bodyAsString();

                JsonObject data = new JsonObject()
                        .put("genres", new JsonArray().add("Rock").add("Reggae"))
                        .put("iban", "FR3030002006630000692730X33")
                        .put("bic", "AZEIHKOH")
                        .put("doPrivate", false)
                        .put("telephone", "0683925511")
                        .put("picture", img_base64);

                // 3. send post request
                client.put(3000, "localhost", "/jwt/streamer_profil")
                        .putHeader("Authorization", "Bearer " + token).sendJson(data, res -> {

                    if (res.succeeded()){
                        // 4. check response status code
                        HttpResponse<Buffer> answer = res.result();

                        context.assertEquals(400, answer.statusCode());
                        context.assertEquals("BIC déjà utilisé", answer.bodyAsString());
                        async.complete();

                    } else {
                        async.resolve(Future.failedFuture(res.cause()));
                    }
                });
            } else {
                async.resolve(Future.failedFuture(ans.cause()));
            }
        });
    }

    /*
        Test that a user can't register with picture bigger than 4.5*1024*1024
        1. log as user4
        2. get jwt token
        3. send post request
        4. check response status code and msg
     */
    @Test
    public void registerStreamerProfilHandler_PictureTooBig(TestContext context) {
        Async async = context.async();

        JsonObject post_data = new JsonObject().put("username", "registerStreamerProfil4")
                .put("password", "azerty")
                .put("method", "username");

        //  1. log as user2
        client.post(3000, "localhost", "/login").sendJson(post_data, ans -> {

            if (ans.succeeded()){
                HttpResponse<Buffer> response = ans.result();

                if (response.statusCode() != 200){
                    async.resolve(Future.failedFuture("Can't log in"));
                }

                // 2. get jwt token
                String token = response.bodyAsString();

                StringBuilder img = new StringBuilder("data:image/png;base64,");
                for (int i = 0; i < 4.5*1024*1024; i++){
                    img.append("A");
                }

                JsonObject data = new JsonObject()
                        .put("genres", new JsonArray().add("Rock").add("Reggae"))
                        .put("iban", "FR3030002006630000692730X33")
                        .put("bic", "AZEIHKOH")
                        .put("doPrivate", false)
                        .put("telephone", "0683925511")
                        .put("picture",img.toString());

                // 3. send post request
                client.put(3000, "localhost", "/jwt/streamer_profil")
                        .putHeader("Authorization", "Bearer " + token).sendJson(data, res -> {

                    if (res.succeeded()){
                        // 4. check response status code
                        HttpResponse<Buffer> answer = res.result();

                        context.assertEquals(400, answer.statusCode());
                        context.assertEquals("Erreur ! Fichier image trop volumineux.", answer.bodyAsString());
                        async.complete();

                    } else {
                        async.resolve(Future.failedFuture(res.cause()));
                    }
                });
            } else {
                async.resolve(Future.failedFuture(ans.cause()));
            }
        });
    }

    /*
        Test that a user can't register with bio bigger 15000000
        1. log as user4
        2. get jwt token
        3. send post request
        4. check response status code and msg
     */
    @Test
    public void registerStreamerProfilHandler_BioTooBig(TestContext context) {
        Async async = context.async();

        JsonObject post_data = new JsonObject().put("username", "registerStreamerProfil4")
                .put("password", "azerty")
                .put("method", "username");

        //  1. log as user2
        client.post(3000, "localhost", "/login").sendJson(post_data, ans -> {

            if (ans.succeeded()){
                HttpResponse<Buffer> response = ans.result();

                if (response.statusCode() != 200){
                    async.resolve(Future.failedFuture("Can't log in"));
                }

                // 2. get jwt token
                String token = response.bodyAsString();

                StringBuilder bio = new StringBuilder("data:image/png;base64,");
                for (int i = 0; i < 15000001; i++){
                    bio.append("A");
                }

                JsonObject data = new JsonObject()
                        .put("genres", new JsonArray().add("Rock").add("Reggae"))
                        .put("iban", "FR3030002006630000692730X33")
                        .put("bic", "AZEIHKOH")
                        .put("doPrivate", false)
                        .put("telephone", "0683925511")
                        .put("picture",img_base64)
                        .put("bio", bio.toString());

                // 3. send post request
                client.put(3000, "localhost", "/jwt/streamer_profil")
                        .putHeader("Authorization", "Bearer " + token).sendJson(data, res -> {

                    if (res.succeeded()){
                        // 4. check response status code
                        HttpResponse<Buffer> answer = res.result();

                        context.assertEquals(400, answer.statusCode());
                        context.assertEquals("Erreur ! Description trop volumineuse !", answer.bodyAsString());
                        async.complete();

                    } else {
                        async.resolve(Future.failedFuture(res.cause()));
                    }
                });
            } else {
                async.resolve(Future.failedFuture(ans.cause()));
            }
        });
    }

    /*
        Test that a register can't be done without genres
        1. log as user4
        2. get jwt token
        3. send post request
        4. check response status code
     */
    @Test
    public void registerStreamerProfilHandler_WithoutGenres(TestContext context) {
        Async async = context.async();

        JsonObject post_data = new JsonObject().put("username", "registerStreamerProfil4")
                .put("password", "azerty")
                .put("method", "username");

        //  1. log as user2
        client.post(3000, "localhost", "/login").sendJson(post_data, ans -> {

            if (ans.succeeded()){
                HttpResponse<Buffer> response = ans.result();

                if (response.statusCode() != 200){
                    async.resolve(Future.failedFuture("Can't log in"));
                }

                // 2. get jwt token
                String token = response.bodyAsString();

                JsonObject data = new JsonObject()
                        .put("iban", "FR3030002006630000692730X33")
                        .put("bic", "AZEIHKOH")
                        .put("doPrivate", false)
                        .put("telephone", "0683925511")
                        .put("picture",img_base64);

                // 3. send post request
                client.put(3000, "localhost", "/jwt/streamer_profil")
                        .putHeader("Authorization", "Bearer " + token).sendJson(data, res -> {

                    if (res.succeeded()){
                        // 4. check response status code
                        HttpResponse<Buffer> answer = res.result();

                        context.assertEquals(400, answer.statusCode());
                        async.complete();

                    } else {
                        async.resolve(Future.failedFuture(res.cause()));
                    }
                });
            } else {
                async.resolve(Future.failedFuture(ans.cause()));
            }
        });
    }

    /*
        Test that a register can't be done without picture
        1. log as user4
        2. get jwt token
        3. send post request
        4. check response status code
     */
    @Test
    public void registerStreamerProfilHandler_WithoutPicture(TestContext context) {
        Async async = context.async();

        JsonObject post_data = new JsonObject().put("username", "registerStreamerProfil4")
                .put("password", "azerty")
                .put("method", "username");

        //  1. log as user2
        client.post(3000, "localhost", "/login").sendJson(post_data, ans -> {

            if (ans.succeeded()){
                HttpResponse<Buffer> response = ans.result();

                if (response.statusCode() != 200){
                    async.resolve(Future.failedFuture("Can't log in"));
                }

                // 2. get jwt token
                String token = response.bodyAsString();

                JsonObject data = new JsonObject()
                        .put("genres", new JsonArray().add("Rock").add("Reggae"))
                        .put("iban", "FR3030002006630000692730X33")
                        .put("bic", "AZEIHKOH")
                        .put("doPrivate", false)
                        .put("telephone", "0683925511");

                // 3. send post request
                client.put(3000, "localhost", "/jwt/streamer_profil")
                        .putHeader("Authorization", "Bearer " + token).sendJson(data, res -> {

                    if (res.succeeded()){
                        // 4. check response status code
                        HttpResponse<Buffer> answer = res.result();

                        context.assertEquals(400, answer.statusCode());
                        async.complete();

                    } else {
                        async.resolve(Future.failedFuture(res.cause()));
                    }
                });
            } else {
                async.resolve(Future.failedFuture(ans.cause()));
            }
        });
    }

    /*
        Test that a register can't be done without telephone
        1. log as user4
        2. get jwt token
        3. send post request
        4. check response status code
     */
    @Test
    public void registerStreamerProfilHandler_WithoutTelephone(TestContext context) {
        Async async = context.async();

        JsonObject post_data = new JsonObject().put("username", "registerStreamerProfil4")
                .put("password", "azerty")
                .put("method", "username");

        //  1. log as user2
        client.post(3000, "localhost", "/login").sendJson(post_data, ans -> {

            if (ans.succeeded()){
                HttpResponse<Buffer> response = ans.result();

                if (response.statusCode() != 200){
                    async.resolve(Future.failedFuture("Can't log in"));
                }

                // 2. get jwt token
                String token = response.bodyAsString();

                JsonObject data = new JsonObject()
                        .put("genres", new JsonArray().add("Rock").add("Reggae"))
                        .put("iban", "FR3030002006630000692730X33")
                        .put("bic", "AZEIHKOH")
                        .put("doPrivate", false)
                        .put("picture",img_base64);

                // 3. send post request
                client.put(3000, "localhost", "/jwt/streamer_profil")
                        .putHeader("Authorization", "Bearer " + token).sendJson(data, res -> {

                    if (res.succeeded()){
                        // 4. check response status code
                        HttpResponse<Buffer> answer = res.result();

                        context.assertEquals(400, answer.statusCode());
                        async.complete();

                    } else {
                        async.resolve(Future.failedFuture(res.cause()));
                    }
                });
            } else {
                async.resolve(Future.failedFuture(ans.cause()));
            }
        });
    }

    /*
        Test that a register can't be done without bic
        1. log as user4
        2. get jwt token
        3. send post request
        4. check response status code
     */
    @Test
    public void registerStreamerProfilHandler_WithoutBic(TestContext context) {
        Async async = context.async();

        JsonObject post_data = new JsonObject().put("username", "registerStreamerProfil4")
                .put("password", "azerty")
                .put("method", "username");

        //  1. log as user2
        client.post(3000, "localhost", "/login").sendJson(post_data, ans -> {

            if (ans.succeeded()){
                HttpResponse<Buffer> response = ans.result();

                if (response.statusCode() != 200){
                    async.resolve(Future.failedFuture("Can't log in"));
                }

                // 2. get jwt token
                String token = response.bodyAsString();

                JsonObject data = new JsonObject()
                        .put("genres", new JsonArray().add("Rock").add("Reggae"))
                        .put("iban", "FR3030002006630000692730X33")
                        .put("telephone", "0681764284")
                        .put("doPrivate", false)
                        .put("picture",img_base64);

                // 3. send post request
                client.put(3000, "localhost", "/jwt/streamer_profil")
                        .putHeader("Authorization", "Bearer " + token).sendJson(data, res -> {

                    if (res.succeeded()){
                        // 4. check response status code
                        HttpResponse<Buffer> answer = res.result();

                        context.assertEquals(400, answer.statusCode());
                        async.complete();

                    } else {
                        async.resolve(Future.failedFuture(res.cause()));
                    }
                });
            } else {
                async.resolve(Future.failedFuture(ans.cause()));
            }
        });
    }

    /*
        Test that a register can't be done without iban
        1. log as user4
        2. get jwt token
        3. send post request
        4. check response status code
     */
    @Test
    public void registerStreamerProfilHandler_WithoutIban(TestContext context) {
        Async async = context.async();

        JsonObject post_data = new JsonObject().put("username", "registerStreamerProfil4")
                .put("password", "azerty")
                .put("method", "username");

        //  1. log as user2
        client.post(3000, "localhost", "/login").sendJson(post_data, ans -> {

            if (ans.succeeded()){
                HttpResponse<Buffer> response = ans.result();

                if (response.statusCode() != 200){
                    async.resolve(Future.failedFuture("Can't log in"));
                }

                // 2. get jwt token
                String token = response.bodyAsString();

                JsonObject data = new JsonObject()
                        .put("genres", new JsonArray().add("Rock").add("Reggae"))
                        .put("bic", "LKOUHKIU")
                        .put("telephone", "0681764284")
                        .put("doPrivate", false)
                        .put("picture",img_base64);

                // 3. send post request
                client.put(3000, "localhost", "/jwt/streamer_profil")
                        .putHeader("Authorization", "Bearer " + token).sendJson(data, res -> {

                    if (res.succeeded()){
                        // 4. check response status code
                        HttpResponse<Buffer> answer = res.result();

                        context.assertEquals(400, answer.statusCode());
                        async.complete();

                    } else {
                        async.resolve(Future.failedFuture(res.cause()));
                    }
                });
            } else {
                async.resolve(Future.failedFuture(ans.cause()));
            }
        });
    }
}