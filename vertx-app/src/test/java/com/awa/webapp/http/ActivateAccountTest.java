package com.awa.webapp.http;

import com.awa.webapp.MainVerticle;
import com.awa.webapp.utils.User;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.mongo.MongoAuth;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.io.IOException;
import java.security.KeyException;
import java.util.concurrent.CountDownLatch;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(VertxUnitRunner.class)
public class ActivateAccountTest {

    private static Vertx vertx;
    private static WebClient client;
    private static MongoClient mongoClient;
    private static MongoAuth authUsername;
    private static MongoAuth authMail;

    @BeforeClass
    public static void setUp(TestContext context) throws IOException {
        // create vertx Instance
        vertx = Vertx.vertx();

        // deploy database verticle
        vertx.deployVerticle(MainVerticle.class.getName(), context.asyncAssertSuccess());

        // create mongo client and mong auth provider
        mongoClient = MongoClient.createShared(vertx, new JsonObject().put("connection_string", "mongodb://localhost:27017")
                .put("db_name", "awa"));

        authUsername = MongoAuth.create(mongoClient, new JsonObject());
        authMail = MongoAuth.create(mongoClient, new JsonObject());

        authUsername.setCollectionName("users");
        authUsername.setUsernameField("username");

        authMail.setCollectionName("users");
        authMail.setUsernameField("mail");

        // create WebClient
        client = WebClient.create(vertx, new WebClientOptions().setFollowRedirects(false));
    }

    @AfterClass
    public static void tearDown(TestContext context) throws InterruptedException{
        CountDownLatch latch = new CountDownLatch(2);

        mongoClient.removeDocuments("users", new JsonObject(), res -> {
            if (res.succeeded()) latch.countDown();
        });
        mongoClient.removeDocuments("streamers", new JsonObject(), res -> {
            if (res.succeeded()) latch.countDown();
        });

        latch.await();
        vertx.close(context.asyncAssertSuccess());
    }


    /*
        Test that a user can activate his account after registration
        1. insert a user in db
        2. send data to activate account
        3. check that redirection is done
        4. check that user account is activated and activationKey changed
     */
    @Test
    public void activateAccountHandler_GoodActivationParams(TestContext context){
        Async async = context.async();

        JsonObject user_data = new JsonObject().put("username", "activateAccount")
                .put("firstName", "test")
                .put("lastName", "user")
                .put("mail", "activate@account.com")
                .put("birthdate", "1990-07-29")
                .put("password", "azerty");

        User user;
        try {
            user = new User(user_data, authMail);
        } catch (KeyException e){
            async.resolve(Future.failedFuture("KeyException when creating user"));
            return;
        }

        // first we insert the user in db
        mongoClient.insert("users", JsonObject.mapFrom(user), res -> {
            if (res.succeeded()){
                // then we send request to activate account
                client.get(3000, "localhost",
                        "/activate_account?username=activateAccount&activationKey=" + user.getActivationKey()).send(ans -> {

                    if (ans.succeeded()){
                        // we check that redirection is done
                        context.assertEquals(302, ans.result().statusCode());
                        context.assertTrue(ans.result().headers().contains("location"));
                        context.assertEquals("/#/login/home/0", ans.result().getHeader("location"));

                        mongoClient.findOne("users", new JsonObject().put("username", "activateAccount"), new JsonObject(), ar -> {
                            if (ar.succeeded()){
                                // finally we check that user is activated and activationKey changed
                                context.assertNotEquals(ar.result().getString("activationKey"), user.getActivationKey());
                                context.assertTrue(ar.result().getBoolean("activated"));
                                async.complete();
                            } else {
                                async.resolve(Future.failedFuture(ar.cause()));
                            }
                        });

                    } else {
                        async.resolve(Future.failedFuture(ans.cause()));
                    }
                });

            } else {
                async.resolve(Future.failedFuture(res.cause()));
            }
        });
    }

    /*
        Test that a user whose account is already activated is redirected to home
        1. retrieve user informations
        2. send data to activate account
        3. check that redirection is done
        4. check that activationKey has not been changed
    */
    @Test
    public void activateAccountHandler_UserAlreadyActivated(TestContext context){
        Async async = context.async();

        // first we retrieve user from db
        mongoClient.findOne("users", new JsonObject().put("username", "activateAccount"), new JsonObject(), res -> {
            if (res.succeeded()){
                String key = res.result().getString("activationKey");

                // then we send request to activate account
                client.get(3000, "localhost", "/activate_account?username=activateAccount&activationKey=" + key)
                        .send(ans -> {

                            if (ans.succeeded()){
                                // we check that redirection is done
                                context.assertEquals(302, ans.result().statusCode());
                                context.assertTrue(ans.result().headers().contains("location"));
                                context.assertEquals("/#/?activationAlreadyDone=true", ans.result().getHeader("location"));

                                mongoClient.findOne("users", new JsonObject().put("username", "activateAccount"), new JsonObject(), ar -> {
                                    if (ar.succeeded()){
                                        // finally we check that activation key has not been changed
                                        context.assertEquals(ar.result().getString("activationKey"), key);
                                        async.complete();
                                    } else {
                                        async.resolve(Future.failedFuture(ar.cause()));
                                    }
                                });

                            } else {
                                async.resolve(Future.failedFuture(ans.cause()));
                            }
                        });

            } else {
                async.resolve(Future.failedFuture(res.cause()));
            }
        });
    }

    /*
        Test that an activation link with wrong key cause a HTTP/302 (we assume that user is already activated)
        1. send data to activate account
        2. check status code and redirection
    */
    @Test
    public void activateAccountHandler_WrongKey(TestContext context){
        Async async = context.async();

        // 1. send data to activate account
        client.get(3000, "localhost",
                "/activate_account?username=activateAccount&activationKey=yololo").send(ans -> {

            if (ans.succeeded()){
                // 2. check status code
                context.assertEquals(302, ans.result().statusCode());
                context.assertTrue(ans.result().headers().contains("location"));
                context.assertEquals("/#/?activationAlreadyDone=true", ans.result().getHeader("location"));
                async.complete();
            } else {
                async.resolve(Future.failedFuture(ans.cause()));
            }
        });
    }

    /*
        Test that an activation link with wrong username cause a HTTP/302 (we assume that user is already activated)
        1. send data to activate account
        2. check status code
        3. check that nothing has changed in db
    */
    @Test
    public void activateAccountHandler_WrongUsername(TestContext context){
        Async async = context.async();

        JsonObject user_data = new JsonObject().put("username", "activateAccount3")
                .put("firstName", "test")
                .put("lastName", "user")
                .put("mail", "activate3@account.com")
                .put("birthdate", "1990-07-29")
                .put("password", "azerty");

        User user;
        try {
            user = new User(user_data, authMail);
        } catch (KeyException e){
            async.resolve(Future.failedFuture(""));
            return;
        }

        // first we insert the user in db
        mongoClient.insert("users", JsonObject.mapFrom(user), res -> {
            if (res.succeeded()){
                // then we send request to activate account
                client.get(3000, "localhost",
                        "/activate_account?username=wrongUsername&activationKey=" + user.getActivationKey()).send(ans -> {

                    if (ans.succeeded()){
                        // we check status code
                        context.assertEquals(302, ans.result().statusCode());
                        context.assertTrue(ans.result().headers().contains("location"));
                        context.assertEquals("/#/?activationAlreadyDone=true", ans.result().getHeader("location"));

                        mongoClient.findOne("users", new JsonObject().put("username", "activateAccount3"), new JsonObject(), ar -> {
                            if (ar.succeeded()){
                                // finally we check that user hasn't been modified in db
                                context.assertEquals(JsonObject.mapFrom(new User(ar.result())), JsonObject.mapFrom(user));
                                async.complete();
                            } else {
                                async.resolve(Future.failedFuture(ar.cause()));
                            }
                        });

                    } else {
                        async.resolve(Future.failedFuture(ans.cause()));
                    }
                });

            } else {
                async.resolve(Future.failedFuture(res.cause()));
            }
        });
    }

    /*
        Test that an incorrect activation link cause a HTTP/400
        1. send data to activate account
        2. check status code
    */
    @Test
    public void activateAccountHandler_WrongLink1(TestContext context){
        Async async = context.async();

        client.get(3000, "localhost",
                "/activate_account?username=activateAccount&activationKey=").send(ans -> {

            if (ans.succeeded()){
                // we check status code
                context.assertEquals(400, ans.result().statusCode());
                async.complete();

            } else {
                async.resolve(Future.failedFuture(ans.cause()));
            }
        });

    }

    /*
        Test that an incorrect activation link cause a HTTP/400
        1. send data to activate account
        2. check status code
    */
    @Test
    public void activateAccountHandler_WrongLink2(TestContext context){
        Async async = context.async();

        client.get(3000, "localhost",
                "/activate_account?username=activateAccount&activation_key=LKLLKJLJL").send(ans -> {

            if (ans.succeeded()){
                // we check status code
                context.assertEquals(400, ans.result().statusCode());
                async.complete();

            } else {
                async.resolve(Future.failedFuture(ans.cause()));
            }
        });

    }

    /*
        Test that an incorrect activation link cause a HTTP/400
        1. send data to activate account
        2. check status code
    */
    @Test
    public void activateAccountHandler_WrongLink3(TestContext context){
        Async async = context.async();

        client.get(3000, "localhost",
                "/activate_account?activation_key=LKLLKJLJL").send(ans -> {

            if (ans.succeeded()){
                // we check status code
                context.assertEquals(400, ans.result().statusCode());
                async.complete();

            } else {
                async.resolve(Future.failedFuture(ans.cause()));
            }
        });

    }

}
