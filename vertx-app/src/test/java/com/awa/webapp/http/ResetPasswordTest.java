package com.awa.webapp.http;

import com.awa.webapp.MainVerticle;
import com.awa.webapp.utils.User;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.mongo.MongoAuth;
import io.vertx.ext.auth.mongo.impl.DefaultHashStrategy;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.security.KeyException;
import java.util.concurrent.CountDownLatch;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(VertxUnitRunner.class)
public class ResetPasswordTest {

    private static Vertx vertx;
    private static WebClient client;
    private static MongoClient mongoClient;
    private static MongoAuth authUsername;
    private static MongoAuth authMail;

    @BeforeClass
    public static void setUp(TestContext context) {
        // create vertx Instance
        vertx = Vertx.vertx();

        // deploy database verticle
        vertx.deployVerticle(MainVerticle.class.getName(), context.asyncAssertSuccess());

        // create mongo client and mong auth provider
        mongoClient = MongoClient.createShared(vertx, new JsonObject().put("connection_string", "mongodb://localhost:27017")
                .put("db_name", "awa"));

        authUsername = MongoAuth.create(mongoClient, new JsonObject());
        authMail = MongoAuth.create(mongoClient, new JsonObject());

        authUsername.setCollectionName("users");
        authUsername.setUsernameField("username");

        authMail.setCollectionName("users");
        authMail.setUsernameField("mail");

        // create WebClient
        client = WebClient.create(vertx, new WebClientOptions().setFollowRedirects(false));
    }

    @AfterClass
    public static void tearDown(TestContext context) throws InterruptedException{
        CountDownLatch latch = new CountDownLatch(2);

        mongoClient.removeDocuments("users", new JsonObject(), res -> {
            if (res.succeeded()) latch.countDown();
        });
        mongoClient.removeDocuments("streamers", new JsonObject(), res -> {
            if (res.succeeded()) latch.countDown();
        });

        latch.await();
        vertx.close(context.asyncAssertSuccess());
    }


    /*
        Test that a user can reset his password with the good 64bits key
        1. insert a user in db
        2. send data to reset password
        3. check that activatioKey password, salt has changed
        4. check that we can authenticate with new credentials
     */
    @Test
    public void resetPasswordHandler_GoodResetParams(TestContext context){
        Async async = context.async();

        JsonObject user_data = new JsonObject().put("username", "resetPassword")
                .put("firstName", "reset")
                .put("lastName", "Password")
                .put("mail", "reset@password.com")
                .put("birthdate", "1990-07-29")
                .put("password", "azerty");

        User user;
        try {
            user = new User(user_data, authMail);
        } catch (KeyException e){
            async.resolve(Future.failedFuture("KeyException when creating user"));
            return;
        }

        String clear_password = "somepassword";

        // 1. insert a user in db
        mongoClient.insert("users", JsonObject.mapFrom(user), res -> {
            if (res.succeeded()){

                JsonObject data = new JsonObject().put("username", user.getUsername())
                        .put("password", clear_password)
                        .put("activationKey", user.getActivationKey());

                // 2. send data to reset password
                client.post(3000, "localhost",
                        "/reset_password").sendJson(data, ans
                        -> {

                    if (ans.succeeded()){
                        context.assertEquals(200, ans.result().statusCode());

                        mongoClient.findOne("users", new JsonObject().put("username", user.getUsername()), new JsonObject(), ar -> {
                            if (ar.succeeded()){
                                // 3. check that activatioKey password, salt has changed
                                context.assertNotEquals(ar.result().getString("activationKey"), user.getActivationKey());
                                context.assertNotEquals(ar.result().getString("password"), user.getPassword());
                                context.assertNotEquals(ar.result().getString("salt"), user.getSalt());

                                // 4. check that we can authenticate with new credentials
                                authUsername.authenticate(new JsonObject().put("username", user.getUsername())
                                        .put("password", clear_password), reply -> {
                                    if (reply.succeeded()){
                                        async.complete();
                                    } else {
                                        async.resolve(Future.failedFuture("Can't authenticate after password reset"));
                                    }
                                });

                            } else {
                                async.resolve(Future.failedFuture(ar.cause()));
                            }
                        });

                    } else {
                        async.resolve(Future.failedFuture(ans.cause()));
                    }
                });

            } else {
                async.resolve(Future.failedFuture(res.cause()));
            }
        });
    }

    /*
        Test that a user can't reset his password with the wrong 64bits key
        1. insert user in db
        2. send data to reset password with wrong key
        3. check status code
        4. check that key password and salt have not changed
        5. check that we can authenticate with old credentials
    */
    @Test
    public void resetPasswordAccountHandler_WrongKey(TestContext context){
        Async async = context.async();

        JsonObject user_data = new JsonObject().put("username", "resetPasswordWrongKey")
                .put("firstName", "reset")
                .put("lastName", "Password")
                .put("mail", "resetpassword@wrongkey.com")
                .put("birthdate", "1990-07-29")
                .put("password", "azerty");

        User user;
        try {
            user = new User(user_data, authMail);
        } catch (KeyException e){
            async.resolve(Future.failedFuture("KeyException when creating user"));
            return;
        }

        String clear_password = "somepassword";

        // 1. insert user in db
        mongoClient.insert("users", JsonObject.mapFrom(user), res -> {
            if (res.succeeded()){

                JsonObject data = new JsonObject().put("username", user.getUsername())
                        .put("password", clear_password)
                        .put("activationKey", DefaultHashStrategy.generateSalt());

                // 2. send data to reset password with wrong key
                client.post(3000, "localhost", "/reset_password").sendJson(data, ans -> {

                    if (ans.succeeded()){
                        // 3. check status code
                        context.assertEquals(400, ans.result().statusCode());

                        mongoClient.findOne("users", new JsonObject().put("username", user.getUsername()), new JsonObject(), ar -> {
                            if (ar.succeeded()){
                                // 4. check that key password and salt have not changed
                                context.assertEquals(ar.result().getString("activationKey"), user.getActivationKey());
                                context.assertEquals(ar.result().getString("password"), user.getPassword());
                                context.assertEquals(ar.result().getString("salt"), user.getSalt());

                                // 5. check that we can authenticate with old credentials
                                authUsername.authenticate(new JsonObject().put("username", user.getUsername())
                                        .put("password", user_data.getString("password")), reply -> {
                                    if (reply.succeeded()){
                                        async.complete();
                                    } else {
                                        async.resolve(Future.failedFuture("Can't authenticate with old credentials"));
                                    }
                                });

                            } else {
                                async.resolve(Future.failedFuture(ar.cause()));
                            }
                        });

                    } else {
                        async.resolve(Future.failedFuture(ans.cause()));
                    }
                });

            } else {
                async.resolve(Future.failedFuture(res.cause()));
            }
        });
    }

    /*
        Test that user can't reset password with missing params
        1. send data to reset password with missing params
        2. check status code
    */
    @Test
    public void resetPasswordAccountHandler_MissingParams(TestContext context){
        Async async = context.async();

        JsonObject data = new JsonObject().put("username", "someUser")
                .put("activationKey", DefaultHashStrategy.generateSalt());

        // 2. send data to reset password with missing params
        client.post(3000, "localhost", "/reset_password").sendJson(data, ans -> {

            if (ans.succeeded()){
                // 2. check status code
                context.assertEquals(400, ans.result().statusCode());
                async.complete();

            } else {
                async.resolve(Future.failedFuture(ans.cause()));
            }
        });
    }

    /*
        Test that user can't reset password with missing params
        1. send data to reset password with wrong params
        2. check status code
    */
    @Test
    public void resetPasswordAccountHandler_WrongParams(TestContext context){
        Async async = context.async();

        JsonObject data = new JsonObject().put("username", "someUser")
                .put("wrongKey", "someString")
                .put("activationKey", DefaultHashStrategy.generateSalt());

        // 2. send data to reset password with wrong params
        client.post(3000, "localhost", "/reset_password").sendJson(data, ans -> {

            if (ans.succeeded()){
                // 2. check status code
                context.assertEquals(400, ans.result().statusCode());
                async.complete();

            } else {
                async.resolve(Future.failedFuture(ans.cause()));
            }
        });
    }

    /*
        Test that user can't reset password with empty params
        1. send data to reset password with wrong params
        2. check status code
    */
    @Test
    public void resetPasswordAccountHandler_EmpyParam(TestContext context){
        Async async = context.async();

        JsonObject data = new JsonObject().put("username", "someUser")
                .put("password", "")
                .put("activationKey", DefaultHashStrategy.generateSalt());

        // 2. send data to reset password with wrong params
        client.post(3000, "localhost", "/reset_password").sendJson(data, ans -> {

            if (ans.succeeded()){
                // 2. check status code
                context.assertEquals(400, ans.result().statusCode());
                async.complete();

            } else {
                async.resolve(Future.failedFuture(ans.cause()));
            }
        });
    }

}

