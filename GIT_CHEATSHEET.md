GIT CHEATSHEET :clipboard:
=====================


[What's git ?](https://www.atlassian.com/git/tutorials/what-is-version-control)

A git repository (**repo** for short) is the entire group of code that is being tracked by git. It's the folder that contains all the code for your project.

[Setting up your repo](https://www.atlassian.com/git/tutorials/setting-up-a-repository)

## Local vs Remote

A **local** repo, which is stored on your computer's hard drive.
A **remote** repo (`origin`) is a copy of your local repo, stored on the Internet.

## Common local commands

Command | Description
--- | ---
`git help -a` | List available git commands
`git init` | Enables git in working directory
`git status` | Show changes in working directory
`git log` | Show all commits
`git diff` | Show changes to tracked files
`git add [file]` | Stage changes to [file]
`git add .` | Stage all changes in working directory
`git commit -m '[message]'` | Commit staged changes with [message]
`git branch` | Show all local branches
`git branch [branch]` | Create branch [branch]
`git checkout [branch]` | Switch from current branch to [branch]
`git merge [branch]` | Pull changes from [branch] and combine with current working branch
`git branch -d [branch]` | Delete local [branch]
`rm -rf .git` | Remove git from working directory

## Common remote commands
Command | Description
--- | ---
`git clone [URL]` | Clone (copy) remote repo from [URL] to working directory (local)
`git push origin [branch]` | Publish local commits on change to origin (first time, link the branch to remote repo)
`git push` | Publish local commits to origin (first time)
`git pull` | Update local repo from remote repo
`git push [remote] [branch]` | Publish local commits to [remote]
`git pull [remote] [branch]` | Update local repo with changes from [remote]
`git branch -r` | Show all remote branches
`git push origin --delete [branch]` | Delete remote [branch]

## COMMIT/PUSH changes routine

[Saving changes](https://www.atlassian.com/git/tutorials/saving-changes)

1. Check files status `git status`
2. Add files you wish to track `git add [files]`
3. Commit changes locally `git commit -a -m "msg de commit"`
4. Push changes to origin `git push`

## Revert changes
[Undoing changes](https://www.atlassian.com/git/tutorials/undoing-changes)

Command | Description
--- | ---
`git checkout <commit>` | Update all files in the working directory to match the specified commit. You can use either a commit hash or a tag as the <commit> argument. This will put you in a detached HEAD state.
`git revert <commit>` | Generate a new commit that undoes all of the changes introduced in *commit*, then apply it to the current branch.
`git revert HEAD~3..HEAD` | Revert the last three commits

![Short commit refs](https://wac-cdn.atlassian.com/dam/jcr:62440330-6153-4f43-9554-b94c7b205e62/01.svg?cdnVersion=hh)

[Rewriting history](https://www.atlassian.com/git/tutorials/rewriting-history)

## MERGE routine

1. Checkout to main branch `git checkout <branch to merge on>`
2. Merge working branch `git merge <working branch>`
3. Solve the [conflicts](https://wincent.com/wiki/Understanding_Git_conflict_markers). (your IDE can help you doing it)
4. Commit and push changes to origin `git commit -a -m "conflict solved" && git push`
