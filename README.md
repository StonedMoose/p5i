LINKS :link:
======
* Markdown Cheatsheet  [ :point_right: here](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
* Markdown icon code [:point_right: here](https://www.webpagefx.com/tools/emoji-cheat-sheet/)
* GIT Cheatsheet [:point_right: here](GIT_CHEATSHEET.md)

AUTHORS
=========
* <A HREF="mailto:v.andriot301@gmail.com">:e-mail:</A> Victor ANDRIOT


* <A HREF="mailto:san@antonio.net">:e-mail:</A> Julien LHERMITE


* <A HREF="mailto:vinson.edouard@gmail.com">:e-mail:</A> Edouard Vinson


* <A HREF="mailto:Slaoui.khalil@gmail.com">:e-mail:</A> Khalil Slaoui
