db.createCollection("users", {
    validator: {
        $jsonSchema: {
            bsonType: "object",
            properties: {
                username:      { pattern: "^[a-zA-Z0-9_-]+$"},
                firstName:     { bsonType: "string" },
                lastName:      { bsonType: "string" },
                mail:          { bsonType: "string" },
                password:      { pattern: "[A-Z0-9]{128}" },
                salt:          { pattern: "[A-Z0-9]{64}" },
                birthdate:     { pattern: "^[0-9]{4}-[0-9]{2}-[0-9]{2}$" },
                activationKey: { pattern: "[A-Z0-9]{64}" },
                activated:     { bsonType: "bool" },
            }
        }
    }
});

db.users.createIndex({username: 1}, {unique: true});
db.users.createIndex({mail: 1}, {unique: true});

db.createCollection("streamers", {
    validator: {
        $jsonSchema: {
            bsonType: "object",
            properties: {
                username:            { pattern: "^[a-zA-Z0-9_-]+$" },
                dispos:              { bsonType: "array", items: { enum: [ 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi',
                                                                           'Samedi', 'Dimanche']}},
                genres:              { bsonType: "array", items: { enum: ["Rock", "Pop", "Reggae", "Rap", "RnB", "Classique", "Electro",
                                                                            "Trans", "Jazz", "Salsa", "Blues"]}},
                bio:                 { bsonType: "string"},
                iban:                { bsonType: "string"},
                bic:                 { pattern: "^[A-Za-z]{6}([A-Za-z0-9]{2}|[A-Za-z0-9]{5})$" },
                doPrivate:           { bsonType: "bool"},
                telephone:           { pattern: "^[+]?[0-9]{8,18}$"},
                price:               { pattern: "^[0-9]+\.?[0-9]{0,2}$"},
                onAir:               { bsonType: "bool"},
                streamKey:           { pattern: "[A-Z0-9]{24}" }
                // streamKeyExpiration:   { bsonType: "long" }
            }
        }
    }
});

db.streamers.createIndex({telephone: 1}, {unique: true});
db.streamers.createIndex({iban: 1}, {unique: true});
db.streamers.createIndex({bic: 1}, {unique: true});
